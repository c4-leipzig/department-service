package org.c4.microservice.framework.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.bson.types.ObjectId;
import org.c4.departmentservice.Application;
import org.c4.microservice.framework.domain.user.UserRepository;
import org.c4.microservice.framework.domain.user.UserType;
import org.c4.microservice.framework.domain.user.UserTypeResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import reactor.test.StepVerifier;

import java.io.IOException;

/**
 * Testet den Zugriff auf Userressourcen vom User Service.
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest(classes = { Application.class })
class UserRepositoryImplTest
{
    private MockWebServer mockWebServer;

    @Autowired
    private ObjectMapper   objectMapper;
    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setUp() throws IOException
    {
        mockWebServer = new MockWebServer();
        mockWebServer.start(65002);
    }

    @AfterEach
    void tearDown() throws IOException
    {
        mockWebServer.shutdown();
    }

    @Test
    void getTypeForUser_success() throws Exception
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200)
                .setBody(objectMapper.writeValueAsString(UserTypeResult.of(UserType.MEMBER.toString())))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        StepVerifier.create(userRepository.getTypeForUser(new ObjectId().toHexString()))
                .expectNext(UserType.MEMBER)
                .verifyComplete();
    }

    @Test
    void getTypeForUser_notFound()
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(404));

        StepVerifier.create(userRepository.getTypeForUser(new ObjectId().toHexString()))
                .expectError(CouldNotRetrieveException.class)
                .verify();
    }

    @Test
    void getTypeForUser_error()
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(500));

        StepVerifier.create(userRepository.getTypeForUser(new ObjectId().toHexString()))
                .expectError(CouldNotRetrieveException.class)
                .verify();
    }

    @Test
    void existsById_success() throws Exception
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200)
                .setBody(objectMapper.writeValueAsString(UserTypeResult.of(UserType.MEMBER.toString())))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        StepVerifier.create(userRepository.existsById(new ObjectId().toHexString()))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    void existsById_error()
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(500));

        StepVerifier.create(userRepository.existsById(new ObjectId().toHexString()))
                .expectError(CouldNotRetrieveException.class)
                .verify();
    }

}
