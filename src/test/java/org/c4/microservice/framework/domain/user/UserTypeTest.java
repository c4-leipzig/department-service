package org.c4.microservice.framework.domain.user;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class UserTypeTest
{
    @Test
    void exists()
    {
        String existing = UserType.MEMBER.toString();
        assertThat(UserType.exists(existing), is(equalTo(true)));

        String nonExisting = new ObjectId().toHexString();
        assertThat(UserType.exists(nonExisting), is(equalTo(false)));
    }

    @Test
    void allExist()
    {
        assertThat(UserType.allExist(List.of(UserType.MEMBER.toString(), UserType.HONORARY_MEMBER.toString())),
                is(equalTo(true)));

        assertThat(UserType.allExist(List.of(UserType.MEMBER.toString(), new ObjectId().toHexString())), is(equalTo(false)));

        assertThat(UserType.allExist(List.of(new ObjectId().toHexString(), new ObjectId().toHexString())), is(equalTo(false)));
    }
}
