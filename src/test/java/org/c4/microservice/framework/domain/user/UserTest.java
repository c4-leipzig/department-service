package org.c4.microservice.framework.domain.user;

import org.bson.types.ObjectId;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import reactor.test.StepVerifier;

import java.util.Collection;
import java.util.Collections;

import static org.c4.microservice.framework.domain.user.Role.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class UserTest
{
    @Test
    void createFromAuthentication()
    {
        User user = new User(new Authentication()
        {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities()
            {
                return Collections.singletonList(new SimpleGrantedAuthority("DEPARTMENT_ADMIN"));
            }

            @Override
            public Object getCredentials()
            {
                return null;
            }

            @Override
            public Object getDetails()
            {
                return null;
            }

            @Override
            public Object getPrincipal()
            {
                return null;
            }

            @Override
            public boolean isAuthenticated()
            {
                return false;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException
            {

            }

            @Override
            public String getName()
            {
                return new ObjectId().toHexString();
            }
        });

        assertThat(user.isDepartmentAdmin(), is(equalTo(true)));
        assertThat(user.isAnyMember(), is(equalTo(false)));
    }

    @Test
    void isDepartmentAdmin()
    {
        User departmentAdmin = new User(new ObjectId().toHexString(),
                Collections.singletonList(DEPARTMENT_ADMIN));

        assertThat(departmentAdmin.isDepartmentAdmin(), is(equalTo(true)));

        User otherUser = new User(new ObjectId().toHexString(), Collections.singletonList(CANDIDATE));

        assertThat(otherUser.isDepartmentAdmin(), is(equalTo(false)));
    }

    @Test
    void isAnyMember()
    {
        User candidate = new User(new ObjectId().toHexString(), Collections.singletonList(CANDIDATE));
        User member = new User(new ObjectId().toHexString(), Collections.singletonList(MEMBER));
        User honorary = new User(new ObjectId().toHexString(), Collections.singletonList(HONORARY_MEMBER));
        User other = new User(new ObjectId().toHexString(), Collections.singletonList(DEPARTMENT_ADMIN));

        assertThat(candidate.isAnyMember(), is(equalTo(true)));
        assertThat(member.isAnyMember(), is(equalTo(true)));
        assertThat(honorary.isAnyMember(), is(equalTo(true)));
        assertThat(other.isAnyMember(), is(equalTo(false)));
    }

    @Test
    void validateIsDepartmentAdmin_success()
    {
        StepVerifier.create(TestHelper.departmentAdmin().validateIsDepartmentAdmin()).verifyComplete();
    }

    @Test
    void validateIsDepartmentAdmin_failure()
    {
        StepVerifier.create(TestHelper.member().validateIsDepartmentAdmin())
                .expectError(NotAuthorizedException.class)
                .verify();
    }

    @Test
    void validateIsAnyMember()
    {
        StepVerifier.create(TestHelper.member().validateIsAnyMember()).verifyComplete();

        StepVerifier.create(TestHelper.departmentAdmin().validateIsAnyMember())
                .expectError(NotAuthorizedException.class)
                .verify();
    }
}
