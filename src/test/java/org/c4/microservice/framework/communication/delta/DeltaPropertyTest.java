package org.c4.microservice.framework.communication.delta;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * <br/>
 * Copyright: Copyright (c) 09.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DeltaPropertyTest
{
    @Test
    void of()
    {
        DeltaProperty<String> deltaProperty = DeltaProperty.of("anyValue");
        assertThat(deltaProperty.isDoNotChange(), Matchers.is(Matchers.equalTo(false)));
        assertThat(deltaProperty.getChangeValue(), Matchers.is(Matchers.equalTo("anyValue")));

        DeltaProperty<String> nullDelta = DeltaProperty.of(null);
        assertThat(nullDelta.isDoNotChange(), Matchers.is(Matchers.equalTo(false)));
        assertNull(nullDelta.getChangeValue());
    }

    @Test
    void ofOldAndNew()
    {
        DeltaProperty<String> delta = DeltaProperty.of("old", "new");
        assertThat(delta.isDoNotChange(), Matchers.is(Matchers.equalTo(false)));
        assertThat(delta.getChangeValue(), Matchers.is(Matchers.equalTo("new")));

        DeltaProperty<String> nullDelta = DeltaProperty.of("old", null);
        assertThat(nullDelta.isDoNotChange(), Matchers.is(Matchers.equalTo(false)));
        assertNull(nullDelta.getChangeValue());

        DeltaProperty<String> noChange = DeltaProperty.of("old", "old");
        assertThat(noChange.isDoNotChange(), Matchers.is(Matchers.equalTo(true)));
    }

    @Test
    void apply()
    {
        assertThat(DeltaProperty.of("old", "new").apply("oldValue"), is(equalTo("new")));
        assertThat(DeltaProperty.of("old", "old").apply("oldValue"),
                Matchers.is(Matchers.equalTo("oldValue")));

    }
}
