package org.c4.microservice.framework.communication.delta;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class ListDeltaPropertyTest
{
    @Test
    void of()
    {
        ListDeltaProperty<String> delta = ListDeltaProperty.of(Collections.singletonList("asdf"));
        assertThat(delta.isDoNotChange(), is(equalTo(false)));
        assertThat(delta.getChangeList().size(), is(equalTo(1)));
        assertThat(delta.getChangeList(), contains("asdf"));
    }

    @Test
    void of_null()
    {
        ListDeltaProperty<String> delta = ListDeltaProperty.of(null);
        assertThat(delta.isDoNotChange(), is(equalTo(true)));
    }


    @Test
    void ofNoDuplicatesNoOrder_identical()
    {
        ListDeltaProperty<String> delta = ListDeltaProperty.ofNoDuplicatesNoOrder(List.of("a", "b"),
                List.of("b", "a"));
        assertThat(delta.isDoNotChange(), is(equalTo(true)));
    }

    @Test
    void ofNoDuplicatesNoOrder_fullyDiverse()
    {
        ListDeltaProperty<String> delta = ListDeltaProperty.ofNoDuplicatesNoOrder(List.of("a", "b"),
                List.of("c", "d"));
        assertThat(delta.isDoNotChange(), is(equalTo(false)));
        assertThat(delta.getChangeList(), contains("c", "d"));
    }

    @Test
    void ofNoDuplicatesNoOrder_partiallyDiverse()
    {
        ListDeltaProperty<String> delta = ListDeltaProperty.ofNoDuplicatesNoOrder(List.of("a", "b"),
                List.of("a", "c"));
        assertThat(delta.isDoNotChange(), is(equalTo(false)));
        assertThat(delta.getChangeList(), contains("a", "c"));
    }

    @Test
    void apply_isDoNotChange()
    {
        ListDeltaProperty<Object> nullDelta = ListDeltaProperty.of(null);
        assertThat(nullDelta.apply(List.of("a", "b")), is(equalTo(List.of("a", "b"))));
    }

    @Test
    void void_isChanged()
    {
        ListDeltaProperty<String> delta = ListDeltaProperty.of(List.of("a"));
        assertThat(delta.apply(List.of("b")), is(equalTo(List.of("a"))));
    }
}
