package org.c4.microservice.framework.configuration;

import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.microservice.framework.initialization.InitializationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Deaktiviert den {@link InitializationRunner} für Tests.
 * <br/>
 * Copyright: Copyright (c) 22.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class TestInitializationConfig
{
    @Bean
    @Profile("test")
    InitializationRunner initializationRunner(DomainEventRepository domainEventRepository,
            DomainEventHandlingContext initialDomainEventHandlingContextImpl)
    {
        return new InitializationRunner(domainEventRepository,
                initialDomainEventHandlingContextImpl);
    }
}
