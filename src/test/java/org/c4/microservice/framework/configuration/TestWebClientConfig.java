package org.c4.microservice.framework.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Überschreibt den WebClient für Tests.
 * <br/>
 * Copyright: Copyright (c) 22.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
@Profile("test")
@Log4j2
public class TestWebClientConfig
{
    @Bean
    WebClient webClient()
    {
        return WebClient.builder().filter(((request, next) -> {
            log.debug("[{}] {}", request::method, request::url);
            return next.exchange(request);
        })).build();
    }
}
