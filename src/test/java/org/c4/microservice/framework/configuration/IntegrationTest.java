package org.c4.microservice.framework.configuration;

import org.c4.departmentservice.Application;
import org.c4.microservice.framework.domainevent.DomainEventSink;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Abstrakte Baseklasse für Integrationstests, die den Kafka überbrückt, damit Events getestet werden können.
 * <br/>
 * Copyright: Copyright (c) 19.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest(classes = Application.class)
public abstract class IntegrationTest
{
    @MockBean
    protected EventPublisher  eventPublisher;
    @Autowired
    protected DomainEventSink domainEventSink;

    @BeforeEach
    void setUpEventRoundTrip()
    {
        doAnswer(invocation -> {
            DomainEvent event = invocation.getArgument(0);
            event.setVersion(event.getVersion() == null ? 0L : event.getVersion() + 1);

            domainEventSink.consumeDomainEvent(event);
            return null;
        }).when(eventPublisher).send(any());
    }
}
