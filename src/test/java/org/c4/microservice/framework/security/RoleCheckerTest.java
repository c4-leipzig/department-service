package org.c4.microservice.framework.security;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 11.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class RoleCheckerTest
{
    private Authentication   authentication   = mock(Authentication.class);
    private GrantedAuthority grantedAuthority = mock(GrantedAuthority.class);

    @BeforeEach
    void setUp()
    {
        doReturn(Collections.singletonList(grantedAuthority)).when(authentication).getAuthorities();
    }

    /**
     * Rolle wird korrekt gematcht.
     */
    @Test
    void authenticationHasRole_successMatch()
    {
        doReturn("ROLE_SUCCESS").when(grantedAuthority).getAuthority();

        boolean result = RoleChecker.authenticationHasRole(authentication, "SUCCESS");

        assertThat(result, Is.is(equalTo(true)));
    }

    /**
     * Rolle wird korrekt nicht gefunden.
     */
    @Test
    void authenticationHasRole_successNoMatch()
    {
        doReturn("ROLE_SUCCESS").when(grantedAuthority).getAuthority();

        boolean result = RoleChecker.authenticationHasRole(authentication, "FAILURE");

        assertThat(result, Is.is(equalTo(false)));
    }

    /**
     * Die zu suchende Rolle darf nicht mit dem Spring-Prefix ROLE_ beginnen
     * (da dieser automatisch erstellt wird).
     */
    @Test
    void authenticationHasRole_RolePrefixMustNotBeSet()
    {
        doReturn("ROLE_SUCCESS").when(grantedAuthority).getAuthority();

        boolean result = RoleChecker.authenticationHasRole(authentication, "ROLE_SUCCESS");

        assertThat(result, Is.is(equalTo(false)));
    }
}
