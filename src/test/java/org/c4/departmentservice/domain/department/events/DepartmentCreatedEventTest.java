package org.c4.departmentservice.domain.department.events;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 03.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCreatedEventTest
{
    private final DomainEventHandlingContext domainEventHandlingContext = mock(
            DomainEventHandlingContext.class);

    @Test
    void apply()
    {
        doReturn(Mono.empty()).when(domainEventHandlingContext).saveDepartment(any());

        Department department = TestHelper.department();
        DepartmentCreatedEvent departmentCreatedEvent = new DepartmentCreatedEvent(department,
                TestHelper.departmentAdmin());
        StepVerifier.create(departmentCreatedEvent.apply(domainEventHandlingContext))
                .expectComplete()
                .verify();

        verify(domainEventHandlingContext, times(1)).saveDepartment(eq(department));
    }

    @Test
    void aggregatePresent()
    {
        doReturn(Mono.just(true)).when(domainEventHandlingContext).departmentExists(any());
        DepartmentCreatedEvent departmentCreatedEvent = TestHelper.departmentCreatedEvent();
        StepVerifier.create(departmentCreatedEvent.aggregatePresent(domainEventHandlingContext))
                .expectNext(true)
                .verifyComplete();
    }

}
