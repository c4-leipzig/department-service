package org.c4.departmentservice.domain.department;

import org.bson.types.ObjectId;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.domain.user.UserType;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentTest
{
    @Test
    void canBeAssignedToType()
    {
        Department department = Department.create(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), Collections.singletonList(UserType.MEMBER));

        assertThat(department.canBeAssignedToType(UserType.MEMBER), is(equalTo(true)));
        assertThat(department.canBeAssignedToType(UserType.HONORARY_MEMBER), is(equalTo(false)));
    }

    @Test
    void generateChange()
    {
        String name = new ObjectId().toHexString();
        Department department = TestHelper.department(name);
        UpdateDepartmentCommand command = TestHelper.updateDepartmentCommand(name);

        DepartmentChange departmentChange = department.generateChange(command);
        assertThat(departmentChange.getName().isDoNotChange(), is(equalTo(true)));
        assertThat(departmentChange.getDescription().getChangeValue(), is(equalTo(command.getDescription())));
        assertThat(departmentChange.getDepartmentCategoryId().getChangeValue(),
                is(equalTo(command.getDepartmentCategoryId())));
        assertThat(departmentChange.getPossibleUserTypes().isDoNotChange(), is(equalTo(true)));
    }

    @Test
    void handleChange()
    {
        String name = new ObjectId().toHexString();
        Department department = TestHelper.department(name);
        UpdateDepartmentCommand command = TestHelper.updateDepartmentCommand(name);

        DepartmentChange departmentChange = department.generateChange(command);
        department.handleChange(departmentChange);

        DepartmentChange noChange = department.generateChange(command);
        assertThat(noChange.getName().isDoNotChange(), is(equalTo(true)));
        assertThat(noChange.getDescription().isDoNotChange(), is(equalTo(true)));
        assertThat(noChange.getDepartmentCategoryId().isDoNotChange(), is(equalTo(true)));
        assertThat(noChange.getPossibleUserTypes().isDoNotChange(), is(equalTo(true)));
    }

}
