package org.c4.departmentservice.domain.department.events;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 03.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentEventHandlingContextTest
{
    private final DepartmentRepository           departmentRepository = mock(DepartmentRepository.class);
    private final DepartmentEventHandlingContext handlingContext      = new DepartmentEventHandlingContext(
            departmentRepository);

    @Test
    void departmentExists()
    {
        doReturn(Mono.just(true)).when(departmentRepository).existsById(any());
        StepVerifier.create(handlingContext.departmentExists(TestHelper.department()))
                .expectNext(true)
                .verifyComplete();

        doReturn(Mono.just(false)).when(departmentRepository).existsById(any());
        StepVerifier.create(handlingContext.departmentExists(TestHelper.department()))
                .expectNext(false)
                .verifyComplete();
    }

    @Test
    void saveDepartment()
    {
        Department department = TestHelper.department();
        doReturn(Mono.just(department)).when(departmentRepository).save(any());

        StepVerifier.create(handlingContext.saveDepartment(department)).expectComplete().verify();
    }
}
