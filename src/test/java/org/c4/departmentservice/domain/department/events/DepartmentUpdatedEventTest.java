package org.c4.departmentservice.domain.department.events;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentUpdatedEventTest
{
    private final DomainEventHandlingContext handlingContext = mock(DomainEventHandlingContext.class);

    @Test
    void apply()
    {
        doReturn(Mono.empty()).when(handlingContext).saveDepartment(any());

        UpdateDepartmentCommand updateCommand = TestHelper.updateDepartmentCommand();
        Department department = TestHelper.department();

        DepartmentUpdatedEvent updatedEvent = new DepartmentUpdatedEvent(new ObjectId().toHexString(), 10L,
                TestHelper.member(), department.generateChange(updateCommand));

        StepVerifier.create(updatedEvent.apply(handlingContext, department)).verifyComplete();
        verify(handlingContext, times(1)).saveDepartment(department);
        assertThat(department.getVersion(), is(equalTo(10L)));
        assertThat(TestHelper.isEmptyDepartmentChange(department.generateChange(updateCommand)),
                is(equalTo(true)));
    }

    @Test
    void getAggregate()
    {
        Department department = TestHelper.department();
        doReturn(Mono.just(department)).when(handlingContext).getDepartment(any());

        StepVerifier.create(TestHelper.departmentUpdatedEvent().getAggregate(handlingContext))
                .expectNext(department)
                .verifyComplete();
    }

}
