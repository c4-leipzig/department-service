package org.c4.departmentservice.domain.department.validation;

import org.bson.types.ObjectId;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.department.commands.CreateDepartmentCommand;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.microservice.framework.domain.user.UserType;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.util.Collections;
import java.util.List;

import static org.c4.departmentservice.ErrorKey.*;

/**
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCommandValidatorTest
{
    @Test
    void validate_success()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command)).verifyComplete();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand)).verifyComplete();
    }

    @Test
    void validate_nameEmpty()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(null, new ObjectId().toHexString(),
                new ObjectId().toHexString(), Collections.singletonList(UserType.MEMBER.toString()));

        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(null,
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, NAME_EMPTY))
                .verify();

        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, NAME_EMPTY))
                .verify();
    }

    @Test
    void validate_nameLengthInvalid()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(TestHelper.stringOfLength(101),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(TestHelper.stringOfLength(101),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, NAME_LENGTH_INVALID))
                .verify();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, NAME_LENGTH_INVALID))
                .verify();
    }

    @Test
    void validate_descriptionLengthInvalid()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(new ObjectId().toHexString(),
                TestHelper.stringOfLength(4001), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(new ObjectId().toHexString(),
                TestHelper.stringOfLength(4001), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, DESCRIPTION_LENGTH_INVALID))
                .verify();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, DESCRIPTION_LENGTH_INVALID))
                .verify();
    }

    @Test
    void validate_categoryEmpty()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), "", Collections.singletonList(UserType.MEMBER.toString()));
        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), "", Collections.singletonList(UserType.MEMBER.toString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, DEPARTMENT_CATEGORY_REQUIRED))
                .verify();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, DEPARTMENT_CATEGORY_REQUIRED))
                .verify();
    }

    @Test
    void validate_userTypeRequired()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(), Collections.emptyList());
        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(), Collections.emptyList());

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, USER_TYPE_REQUIRED))
                .verify();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, USER_TYPE_REQUIRED))
                .verify();
    }

    @Test
    void validate_userTypeInvalid()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(new ObjectId().toHexString()));
        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(new ObjectId().toHexString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, USER_TYPE_INVALID))
                .verify();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, USER_TYPE_INVALID))
                .verify();
    }

    @Test
    void validate_userTypeContainsDuplicates()
    {
        CreateDepartmentCommand command = new CreateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                List.of(UserType.MEMBER.toString(), UserType.HONORARY_MEMBER.toString(),
                        UserType.MEMBER.toString()));
        UpdateDepartmentCommand updateCommand = new UpdateDepartmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString(),
                List.of(UserType.MEMBER.toString(), UserType.HONORARY_MEMBER.toString(),
                        UserType.MEMBER.toString()));

        StepVerifier.create(DepartmentCommandValidator.validate(command))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, USER_TYPE_DUPLICATES))
                .verify();
        StepVerifier.create(DepartmentCommandValidator.validate(updateCommand))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, USER_TYPE_DUPLICATES))
                .verify();
    }
}
