package org.c4.departmentservice.domain.department.validation;

import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * Testet die Validierung von {@link Department}-Commands durch den {@link DepartmentValidator}.
 * <br/>
 * Copyright: Copyright (c) 09.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentValidatorTest
{
    private final DepartmentRepository         departmentRepository         = mock(
            DepartmentRepository.class);
    private final DepartmentCategoryRepository departmentCategoryRepository = mock(
            DepartmentCategoryRepository.class);
    private final DepartmentValidator          departmentValidator          = new DepartmentValidator(
            departmentRepository, departmentCategoryRepository);

    @Test
    void create_success()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByName(any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());

        StepVerifier.create(departmentValidator.validate(TestHelper.createDepartmentCommand(),
                TestHelper.departmentAdmin())).verifyComplete();
    }

    @Test
    void create_notAuthorized()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByName(any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());

        StepVerifier.create(
                departmentValidator.validate(TestHelper.createDepartmentCommand(), TestHelper.member()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }

    @Test
    void create_commandInvalid()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByName(any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());

        StepVerifier.create(departmentValidator.validate(TestHelper.invalidCreateDepartmentCommand(),
                TestHelper.departmentAdmin())).expectError(InvalidOperationException.class).verify();
    }

    @Test
    void create_nameTaken()
    {
        doReturn(Mono.just(true)).when(departmentRepository).existsByName(any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());

        StepVerifier.create(departmentValidator.validate(TestHelper.createDepartmentCommand(),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.NAME_TAKEN))
                .verify();
    }

    @Test
    void create_departmentCategoryNotFound()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByName(any());
        doReturn(Mono.just(false)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate(TestHelper.createDepartmentCommand(),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_CATEGORY_NOT_FOUND))
                .verify();
    }

    @Test
    void update_success()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.updateDepartmentCommand(),
                TestHelper.departmentAdmin())).expectNextCount(1L).verifyComplete();
    }

    @Test
    void update_nameTakenBySelf()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsByName(any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.updateDepartmentCommand(),
                TestHelper.departmentAdmin())).expectNextCount(1L).verifyComplete();
    }

    @Test
    void update_notAuthorized()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.updateDepartmentCommand(),
                TestHelper.member())).expectError(NotAuthorizedException.class).verify();
    }

    @Test
    void update_commandInvalid()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.invalidUpdateDepartmentCommand(),
                TestHelper.departmentAdmin())).expectError(InvalidOperationException.class).verify();
    }

    @Test
    void update_nameTaken()
    {
        doReturn(Mono.just(true)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.updateDepartmentCommand(),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.NAME_TAKEN))
                .verify();
    }

    @Test
    void update_departmentCategoryNotFound()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(false)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.updateDepartmentCommand(),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_CATEGORY_NOT_FOUND))
                .verify();
    }

    @Test
    void update_departmentNotFound()
    {
        doReturn(Mono.just(false)).when(departmentRepository).existsByNameExcludingSelf(any(), any());
        doReturn(Mono.just(true)).when(departmentCategoryRepository).existsById(any());
        doReturn(Mono.empty()).when(departmentRepository).getById(any());

        StepVerifier.create(departmentValidator.validate("anyId", TestHelper.updateDepartmentCommand(),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_NOT_FOUND))
                .verify();
    }

}
