package org.c4.departmentservice.domain.departmentcategory.events;

import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCategoryCreatedEventTest
{
    private final DomainEventHandlingContext domainEventHandlingContext = mock(
            DomainEventHandlingContext.class);

    @Test
    void apply_invokesContext()
    {
        DepartmentCategoryCreatedEvent createdEvent = createdEvent();
        createdEvent.apply(domainEventHandlingContext);

        verify(domainEventHandlingContext, times(1)).saveDepartmentCategory(any());
    }

    @Test
    void aggregatePresent_invokesContext()
    {
        DepartmentCategoryCreatedEvent createdEvent = createdEvent();
        createdEvent.aggregatePresent(domainEventHandlingContext);

        verify(domainEventHandlingContext, times(1)).departmentCategoryExists(any());
    }

    private DepartmentCategoryCreatedEvent createdEvent()
    {
        return new DepartmentCategoryCreatedEvent(TestHelper.departmentCategory(),
                TestHelper.departmentAdmin());
    }
}
