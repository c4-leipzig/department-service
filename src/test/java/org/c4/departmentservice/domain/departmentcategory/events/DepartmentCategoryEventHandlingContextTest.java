package org.c4.departmentservice.domain.departmentcategory.events;

import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 25.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCategoryEventHandlingContextTest
{
    private final DepartmentCategoryRepository           repository = mock(
            DepartmentCategoryRepository.class);
    private final DepartmentCategoryEventHandlingContext context    = new DepartmentCategoryEventHandlingContext(
            repository);

    @Test
    void saveDepartmentCategory_invokesRepository()
    {
        doReturn(Mono.empty()).when(repository).save(any());

        DepartmentCategory departmentCategory = TestHelper.departmentCategory();
        context.saveDepartmentCategory(departmentCategory);

        verify(repository, times(1)).save(departmentCategory);
    }

    @Test
    void departmentCategoryExists_invokesRepository()
    {
        doReturn(Mono.just(true)).when(repository).existsById(any());
        context.departmentCategoryExists(TestHelper.departmentCategory());

        verify(repository, times(1)).existsById(any());
    }
}
