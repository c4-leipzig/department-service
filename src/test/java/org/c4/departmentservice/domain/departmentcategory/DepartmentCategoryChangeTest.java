package org.c4.departmentservice.domain.departmentcategory;

import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Testet, dass das {@link DepartmentCategoryChange} ein {@link DepartmentCategory} korrekt updated.
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCategoryChangeTest
{
    @Test
    void apply()
    {
        DepartmentCategory departmentCategory = TestHelper.departmentCategory();
        DepartmentCategoryChange change = new DepartmentCategoryChange("newName");

        departmentCategory = change.apply(departmentCategory);
        assertThat(departmentCategory.getName(), is(equalTo("newName")));
    }

    @Test
    void isEmpty()
    {
        DepartmentCategoryChange change = new DepartmentCategoryChange("anyNewName");
        assertThat(change.isEmpty(), is(equalTo(false)));

        DepartmentCategoryChange emptyChange = new DepartmentCategoryChange();
        assertThat(emptyChange.isEmpty(), is(equalTo(true)));
    }
}
