package org.c4.departmentservice.domain.departmentcategory;

import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCategoryTest
{
    @Test
    void create()
    {
        DepartmentCategory departmentCategory = DepartmentCategory.create("anyName");
        assertThat(departmentCategory.getName(), is(equalTo("anyName")));
        assertThat(departmentCategory.getId() == null, is(equalTo(false)));
        assertThat(departmentCategory.getId().isBlank(), is(equalTo(false)));
    }

    @Test
    void updateName()
    {
        DepartmentCategory category = TestHelper.departmentCategory();
        category.updateName("newName");

        assertThat(category.getName(), is(equalTo("newName")));
    }
}
