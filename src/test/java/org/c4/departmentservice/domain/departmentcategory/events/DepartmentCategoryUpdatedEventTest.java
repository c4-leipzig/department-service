package org.c4.departmentservice.domain.departmentcategory.events;

import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCategoryUpdatedEventTest
{
    private final DomainEventHandlingContext domainEventHandlingContext = mock(
            DomainEventHandlingContext.class);

    @Test
    void apply_invokesContext()
    {
        updatedEvent().apply(domainEventHandlingContext, TestHelper.departmentCategory());

        verify(domainEventHandlingContext, times(1)).saveDepartmentCategory(any());
    }

    @Test
    void getAggregateWrapper_invokesContext()
    {
        updatedEvent().getAggregate(domainEventHandlingContext);

        verify(domainEventHandlingContext, times(1)).getDepartmentCategory(any());
    }

    private DepartmentCategoryUpdatedEvent updatedEvent()
    {
        return new DepartmentCategoryUpdatedEvent("anyId", 1L, TestHelper.departmentAdmin(),
                TestHelper.departmentCategoryChange());
    }
}
