package org.c4.departmentservice.domain.departmentassignment.events;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentAssignmentEventHandlingContextTest
{
    private final DepartmentAssignmentRepository           repository      = mock(
            DepartmentAssignmentRepository.class);
    private final DepartmentAssignmentEventHandlingContext handlingContext = new DepartmentAssignmentEventHandlingContext(
            repository);

    @Test
    void departmentAssignmentExists()
    {
        doReturn(Mono.just(false)).when(repository).existsById(any());
        StepVerifier.create(handlingContext.departmentAssignmentExists(TestHelper.departmentAssignment()))
                .expectNext(false)
                .verifyComplete();

        doReturn(Mono.just(true)).when(repository).existsById(any());
        StepVerifier.create(handlingContext.departmentAssignmentExists(TestHelper.departmentAssignment()))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    void saveDepartmentAssignment()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        doReturn(Mono.just(departmentAssignment)).when(repository).save(any());

        StepVerifier.create(handlingContext.saveDepartmentAssignment(departmentAssignment)).verifyComplete();
    }

    @Test
    void getDepartmentAssignmentWrapper()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        doReturn(Mono.just(departmentAssignment)).when(repository).getById(any());

        StepVerifier.create(handlingContext.getById(new ObjectId().toHexString()))
                .expectNextCount(1L)
                .verifyComplete();
    }

}
