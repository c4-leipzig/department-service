package org.c4.departmentservice.domain.departmentassignment.validation;

import org.bson.types.ObjectId;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

/**
 * Testet die statische Validierung eingehender {@link CreateDepartmentAssignmentCommand}s.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentAssignmentCommandValidatorTest
{
    private final DepartmentAssignmentCommandValidator commandValidator = new DepartmentAssignmentCommandValidator(
            4000);

    @Test
    void validate_success()
    {
        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                new ObjectId().toHexString(), new ObjectId().toHexString(), new ObjectId().toHexString());

        StepVerifier.create(commandValidator.validate(command)).verifyComplete();
    }

    @Test
    void validate_descriptionLengthInvalid()
    {
        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                TestHelper.stringOfLength(4001), new ObjectId().toHexString(), new ObjectId().toHexString());

        StepVerifier.create(commandValidator.validate(command))
                .expectErrorSatisfies(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DESCRIPTION_LENGTH_INVALID))
                .verify();
    }

    @Test
    void validate_departmentIdRequired()
    {
        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                new ObjectId().toHexString(), "", new ObjectId().toHexString());

        StepVerifier.create(commandValidator.validate(command))
                .expectErrorSatisfies(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_ID_REQUIRED))
                .verify();
    }

    @Test
    void validate_userIdRequired()
    {
        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                new ObjectId().toHexString(), new ObjectId().toHexString(), "");

        StepVerifier.create(commandValidator.validate(command))
                .expectErrorSatisfies(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.USER_ID_REQUIRED))
                .verify();
    }
}
