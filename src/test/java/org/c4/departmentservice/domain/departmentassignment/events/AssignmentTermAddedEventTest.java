package org.c4.departmentservice.domain.departmentassignment.events;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class AssignmentTermAddedEventTest
{
    private final DomainEventHandlingContext handlingContext = mock(DomainEventHandlingContext.class);

    @Test
    void apply()
    {
        doReturn(Mono.empty()).when(handlingContext).saveDepartmentAssignment(any());

        AssignmentTerm assignmentTerm = TestHelper.assignmentTerm();
        AssignmentTermAddedEvent addedEvent = new AssignmentTermAddedEvent(new ObjectId().toHexString(), 1L,
                TestHelper.departmentAdmin(), assignmentTerm);

        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        StepVerifier.create(addedEvent.apply(handlingContext, departmentAssignment)).verifyComplete();

        verify(handlingContext, times(1)).saveDepartmentAssignment(departmentAssignment);
        assertThat(departmentAssignment.getTerms(), contains(assignmentTerm));
        assertThat(departmentAssignment.getTerms().size(), is(equalTo(1)));
    }

    @Test
    void getAggregateWrapper()
    {
        doReturn(Mono.just(TestHelper.departmentAssignment())).when(handlingContext)
                .getDepartmentAssignment(any());

        AssignmentTermAddedEvent addedEvent = new AssignmentTermAddedEvent(new ObjectId().toHexString(), 1L,
                TestHelper.departmentAdmin(), TestHelper.assignmentTerm());

        StepVerifier.create(addedEvent.getAggregate(handlingContext))
                .expectNextCount(1L)
                .verifyComplete();

        verify(handlingContext, times(1)).getDepartmentAssignment(addedEvent.getAggregateId());
    }
}
