package org.c4.departmentservice.domain.departmentassignment.events;

import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentAssignmentCreatedEventTest
{
    private final DomainEventHandlingContext handlingContext = mock(DomainEventHandlingContext.class);

    @Test
    void apply()
    {
        doReturn(Mono.empty()).when(handlingContext).saveDepartmentAssignment(any());

        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        DepartmentAssignmentCreatedEvent createdEvent = new DepartmentAssignmentCreatedEvent(
                departmentAssignment, TestHelper.departmentAdmin());

        StepVerifier.create(createdEvent.apply(handlingContext)).verifyComplete();

        verify(handlingContext, times(1)).saveDepartmentAssignment(departmentAssignment);
        assertThat(departmentAssignment.getVersion(), is(equalTo(createdEvent.getVersion())));
    }

    @Test
    void aggregatePresent()
    {
        DepartmentAssignmentCreatedEvent createdEvent = TestHelper.departmentAssignmentCreatedEvent();
        doReturn(Mono.just(true)).when(handlingContext).departmentAssignmentExists(any());

        StepVerifier.create(createdEvent.aggregatePresent(handlingContext)).expectNext(true).verifyComplete();

        verify(handlingContext, times(1)).departmentAssignmentExists(any());
    }
}
