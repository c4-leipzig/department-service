package org.c4.departmentservice.adapter.rest.departmentassignment;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.department.GetAssignmentTermsForDepartmentUseCase;
import org.c4.departmentservice.usecases.departmentassignment.AddAssignmentTermUseCase;
import org.c4.departmentservice.usecases.departmentassignment.CreateDepartmentAssignmentUseCase;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentAssignmentRestControllerTest
{
    private final CreateDepartmentAssignmentUseCase      createUseCase                          = mock(
            CreateDepartmentAssignmentUseCase.class);
    private final AddAssignmentTermUseCase               addAssignmentTermUseCase               = mock(
            AddAssignmentTermUseCase.class);
    private final GetAssignmentTermsForDepartmentUseCase getAssignmentTermsForDepartmentUseCase = mock(
            GetAssignmentTermsForDepartmentUseCase.class);
    private final DepartmentAssignmentRestController     restController                         = new DepartmentAssignmentRestController(
            createUseCase, addAssignmentTermUseCase, getAssignmentTermsForDepartmentUseCase);

    private final Authentication authentication = mock(Authentication.class);

    @Test
    void createDepartmentAssignment()
    {
        doReturn(Mono.just(IdDto.random())).when(createUseCase).createDepartmentAssignment(any(), any());

        StepVerifier.create(restController.createDepartmentAssignment(new CreateDepartmentAssignmentRestDto(),
                authentication)).expectNextCount(1L).verifyComplete();

        verify(createUseCase, times(1)).createDepartmentAssignment(any(), any());
    }

    @Test
    void addAssignmentTerm()
    {
        doReturn(Mono.empty()).when(addAssignmentTermUseCase).addAssignmentTerm(any(), any());

        StepVerifier.create(restController.addAssignmentTerm(IdDto.random(),
                new AddAssignmentTermRestDto(new ObjectId().toHexString(), LocalDate.MIN, LocalDate.MAX),
                authentication)).verifyComplete();
    }

    @Test
    void getAssignmentTermsForDepartment()
    {
        doReturn(Flux.just(TestHelper.departmentAssignmentTermDto())).when(
                getAssignmentTermsForDepartmentUseCase).getTermsForDepartment(any(), any(), any());

        StepVerifier.create(
                restController.getAssignmentTermsForDepartment(IdDto.random(), SortOrder.ASC, authentication))
                .expectNextCount(1L)
                .verifyComplete();
    }

}
