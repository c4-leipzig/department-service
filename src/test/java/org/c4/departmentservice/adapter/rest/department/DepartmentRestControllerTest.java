package org.c4.departmentservice.adapter.rest.department;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentWithActiveAssignmentDto;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.department.*;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 03.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentRestControllerTest
{
    private final CreateDepartmentUseCase                      createDepartmentUseCase           = mock(
            CreateDepartmentUseCase.class);
    private final UpdateDepartmentUseCase                      updateDepartmentUseCase           = mock(
            UpdateDepartmentUseCase.class);
    private final GetAllDepartmentsUseCase                     getAllDepartmentsUseCase          = mock(
            GetAllDepartmentsUseCase.class);
    private final GetDepartmentByIdUseCase                     getDepartmentByIdUseCase          = mock(
            GetDepartmentByIdUseCase.class);
    private final GetAllDepartmentsWithActiveAssignmentUseCase getAllWithActiveAssignmentUseCase = mock(
            GetAllDepartmentsWithActiveAssignmentUseCase.class);

    private final DepartmentRestController restController = new DepartmentRestController(
            createDepartmentUseCase, updateDepartmentUseCase, getAllDepartmentsUseCase,
            getAllWithActiveAssignmentUseCase, getDepartmentByIdUseCase);

    private final Authentication authentication = mock(Authentication.class);

    @Test
    void createDepartment()
    {
        doReturn(Mono.just(IdDto.random())).when(createDepartmentUseCase).createDepartment(any(), any());

        StepVerifier.create(restController.createDepartment(new DepartmentRestDto(), authentication))
                .expectNextCount(1L)
                .verifyComplete();

        verify(createDepartmentUseCase, times(1)).createDepartment(any(), any());
    }

    @Test
    void updateDepartment()
    {
        doReturn(Mono.empty()).when(updateDepartmentUseCase).updateDepartment(any(), any(), any());

        StepVerifier.create(
                restController.updateDepartment(IdDto.random(), new DepartmentRestDto(), authentication))
                .verifyComplete();
        verify(updateDepartmentUseCase, times(1)).updateDepartment(any(), any(), any());
    }

    @Test
    void getAllDepartments()
    {
        Department department = TestHelper.department();
        Department department2 = TestHelper.department();
        doReturn(Flux.just(department, department2)).when(getAllDepartmentsUseCase).getAllDepartments(any());

        StepVerifier.create(restController.getAllDepartments(authentication))
                .expectNext(department)
                .expectNext(department2)
                .verifyComplete();
    }

    @Test
    void getDepartmentById()
    {
        Department department = TestHelper.department();

        doReturn(Mono.just(department)).when(getDepartmentByIdUseCase).getDepartmentById(any(), any());

        StepVerifier.create(restController.getDepartment(department.getIdDto(), authentication))
                .expectNext(department)
                .verifyComplete();
    }

    @Test
    void getAllDepartmentsWithActiveAssignment()
    {
        Department department = TestHelper.department();
        Department department2 = TestHelper.department();
        doReturn(Flux.just(
                new DepartmentWithActiveAssignmentDto(department, TestHelper.departmentAssignmentTermDto()),
                new DepartmentWithActiveAssignmentDto(department2, null))).when(
                getAllWithActiveAssignmentUseCase).getAllDepartmentsWithActiveAssignment(any());

        StepVerifier.create(restController.getAllDepartmentsWithActiveAssignment(authentication))
                .expectNextMatches(dto -> dto.getDepartment().equals(department) && dto.getAssignmentTerm() != null)
                .expectNextMatches(dto -> dto.getDepartment().equals(department2) && dto.getAssignmentTerm() == null)
                .verifyComplete();
    }

}
