package org.c4.departmentservice.adapter.rest.departmentcategory;

import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.departmentcategory.CreateDepartmentCategoryUseCase;
import org.c4.departmentservice.usecases.departmentcategory.GetAllDepartmentCategoriesUseCase;
import org.c4.departmentservice.usecases.departmentcategory.UpdateDepartmentCategoryUseCase;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DepartmentCategoryRestControllerTest
{

    private final CreateDepartmentCategoryUseCase   createDepartmentCategoryUseCase   = mock(
            CreateDepartmentCategoryUseCase.class);
    private final GetAllDepartmentCategoriesUseCase getAllDepartmentCategoriesUseCase = mock(
            GetAllDepartmentCategoriesUseCase.class);
    private final UpdateDepartmentCategoryUseCase   updateDepartmentCategoryUseCase   = mock(
            UpdateDepartmentCategoryUseCase.class);

    private final Authentication authentication = mock(Authentication.class);

    private final DepartmentCategoryRestController restController = new DepartmentCategoryRestController(
            createDepartmentCategoryUseCase, updateDepartmentCategoryUseCase,
            getAllDepartmentCategoriesUseCase);

    /**
     * DepartmentCategory wird korrekt im UseCase erzeugt.
     */
    @Test
    void createDepartmentCategory()
    {
        IdDto randomId = IdDto.random();
        doReturn(Mono.just(randomId)).when(createDepartmentCategoryUseCase)
                .createDepartmentCategory(any(), any());

        Mono<IdDto> result = restController.createDepartmentCategory(
                new CreateDepartmentCategoryRestDto("anyName"), authentication);

        StepVerifier.create(result).expectNext(randomId).expectComplete().verify();
    }

    /**
     * Update wird korrekt im UseCase verarbeitet.
     */
    @Test
    void updateDepartmentCategory()
    {
        doReturn(Mono.empty()).when(updateDepartmentCategoryUseCase)
                .updateDepartmentCategory(any(), any(), any());

        Mono<Void> result = restController.updateDepartmentCategory(IdDto.random(),
                new UpdateDepartmentCategoryRestDto("anyName"), authentication);

        StepVerifier.create(result).expectComplete().verify();
    }

    @Test
    void getAllDepartmentCategories()
    {
        doReturn(Flux.just(TestHelper.departmentCategory(), TestHelper.departmentCategory())).when(
                getAllDepartmentCategoriesUseCase).getAllDepartmentCategories(any());

        StepVerifier.create(restController.getAllDepartmentCategories(authentication))
                .expectNextCount(2)
                .expectComplete()
                .verify();
    }
}
