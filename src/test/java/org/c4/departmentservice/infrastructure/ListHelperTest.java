package org.c4.departmentservice.infrastructure;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class ListHelperTest
{
    @Test
    void containsDuplicates_doesContain()
    {
        List<String> stringList = List.of("a", "b", "a", "c");
        assertThat(ListHelper.containsDuplicates(stringList), is(equalTo(true)));
    }

    @Test
    void containsDuplicates_doesNotContain()
    {
        List<String> stringList = List.of("a", "b", "c", "d");
        assertThat(ListHelper.containsDuplicates(stringList), is(equalTo(false)));
    }

    @Test
    void containsDuplicates_null()
    {
        assertThat(ListHelper.containsDuplicates(null), is(equalTo(false)));
    }


}
