package org.c4.departmentservice.infrastructure;

import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class StringHelperTest
{

    @Test
    void nullOrEmpty_null()
    {
        assertThat(StringHelper.nullOrEmpty(null), is(equalTo(true)));
    }

    @Test
    void nullOrEmpty_empty()
    {
        assertThat(StringHelper.nullOrEmpty(""), is(equalTo(true)));
    }

    @Test
    void nullOrEmpty_nonBlank()
    {
        assertThat(StringHelper.nullOrEmpty("anyString"), is(equalTo(false)));
    }

    @Test
    void exceedsMaxLength_null()
    {
        assertThat(StringHelper.exceedsMaxLength(null, 10), is(equalTo(false)));
    }

    @Test
    void exceedsMaxLength_shorterThanMaxLength()
    {
        assertThat(StringHelper.exceedsMaxLength(TestHelper.stringOfLength(10), 11), is(equalTo(false)));
    }

    @Test
    void exceedsMaxLength_exactlyMaxLength()
    {
        assertThat(StringHelper.exceedsMaxLength(TestHelper.stringOfLength(10), 10), is(equalTo(false)));
    }

    @Test
    void exceedsMaxLength_greaterThanMaxLength()
    {
        assertThat(StringHelper.exceedsMaxLength(TestHelper.stringOfLength(10), 9), is(equalTo(true)));
    }

}
