package org.c4.departmentservice.infrastructure.repositories.departmentcategory;

import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class MongoDepartmentCategoryRepositoryImplTest
{
    private final DepartmentCategoryDataRepository dataRepository = mock(
            DepartmentCategoryDataRepository.class);
    private final DepartmentCategoryRepository     repository     = new MongoDepartmentCategoryRepositoryImpl(
            dataRepository);

    @Test
    void save()
    {
        DepartmentCategory departmentCategory = TestHelper.departmentCategory();
        doReturn(Mono.just(departmentCategory)).when(dataRepository).save(any());

        StepVerifier.create(repository.save(departmentCategory))
                .expectNext(departmentCategory)
                .expectComplete()
                .verify();
    }

    @Test
    void getById()
    {
        DepartmentCategory departmentCategory = TestHelper.departmentCategory();
        doReturn(Mono.just(departmentCategory)).when(dataRepository).findById(departmentCategory.getId());

        StepVerifier.create(repository.getById(departmentCategory.getId()))
                .expectNext(departmentCategory)
                .expectComplete()
                .verify();
    }

    @Test
    void existsByName()
    {
        doReturn(Mono.just(true)).when(dataRepository).existsByName("existingName");
        doReturn(Mono.just(false)).when(dataRepository).existsByName("nonExistingName");

        StepVerifier.create(repository.existsByName("existingName"))
                .expectNext(true)
                .expectComplete()
                .verify();

        StepVerifier.create(repository.existsByName("nonExistingName"))
                .expectNext(false)
                .expectComplete()
                .verify();
    }

    @Test
    void existsById()
    {
        doReturn(Mono.just(true)).when(dataRepository).existsById("existingId");
        doReturn(Mono.just(false)).when(dataRepository).existsById("nonExistingId");

        StepVerifier.create(repository.existsById("existingId")).expectNext(true).expectComplete().verify();

        StepVerifier.create(repository.existsById("nonExistingId"))
                .expectNext(false)
                .expectComplete()
                .verify();
    }

    @Test
    void getAll()
    {
        doReturn(Flux.just(TestHelper.departmentCategory())).when(dataRepository).findAll();

        StepVerifier.create(repository.getAll()).expectNextCount(1).expectComplete().verify();
    }
}
