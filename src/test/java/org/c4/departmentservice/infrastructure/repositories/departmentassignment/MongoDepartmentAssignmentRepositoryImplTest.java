package org.c4.departmentservice.infrastructure.repositories.departmentassignment;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class MongoDepartmentAssignmentRepositoryImplTest extends IntegrationTest
{
    @Autowired
    private DepartmentAssignmentDataRepository dataRepository;
    @Autowired
    private DepartmentAssignmentRepository     repository;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void save()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();

        StepVerifier.create(repository.save(departmentAssignment))
                .expectNextMatches(da -> da.equalsRegardingDepartmentAndUser(departmentAssignment))
                .verifyComplete();

        StepVerifier.create(dataRepository.findById(departmentAssignment.getId()))
                .expectNextCount(1L)
                .verifyComplete();
    }

    @Test
    void existsById()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(repository.existsById(new ObjectId().toHexString()))
                .expectNext(false)
                .verifyComplete();

        StepVerifier.create(repository.existsById(departmentAssignment.getId()))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    void existsByDepartmentAndUser()
    {
        String departmentId = new ObjectId().toHexString();
        String userId = new ObjectId().toHexString();

        DepartmentAssignment departmentAssignment = DepartmentAssignment.create(new ObjectId().toHexString(),
                departmentId, userId);
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(repository.existsByDepartmentAndUser(departmentId, userId))
                .expectNext(true)
                .verifyComplete();

        StepVerifier.create(repository.existsByDepartmentAndUser(new ObjectId().toHexString(),
                new ObjectId().toHexString())).expectNext(false).verifyComplete();
    }

    @Test
    void getWrapperById()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(repository.getById(departmentAssignment.getId()))
                .expectNextMatches(wrapper -> wrapper.getId().equals(departmentAssignment.getId()))
                .verifyComplete();
    }

    /**
     * DB   ---------|----------|------>
     * Neu  --------------|--------|--->
     */
    @Test
    void existsOverlappingTermForDepartment_startDbBeforeStartEndDbBetweenStartAndEnd()
    {
        DepartmentAssignment departmentAssignmentDB = TestHelper.departmentAssignment();
        departmentAssignmentDB.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(departmentAssignmentDB).block();

        AssignmentTerm assignmentTerm = new AssignmentTerm(LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 2, 15), new ObjectId().toHexString());

        StepVerifier.create(
                repository.existsOverlappingTermForDepartment(departmentAssignmentDB.getDepartmentId(),
                        assignmentTerm)).expectNext(true).verifyComplete();
    }

    /**
     * DB   ---------|----------|------>
     * Neu  --------------|--------|--->
     * Unterschiedliche Departments, daher darf das gespeicherte Intervall nicht mitgewertet werden.
     */
    @Test
    void existsOverlappingTermForDepartment_startDbBeforeStartEndDbBetweenStartAndEnd_differentDepartment()
    {
        DepartmentAssignment departmentAssignmentDB = TestHelper.departmentAssignment();
        departmentAssignmentDB.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(departmentAssignmentDB).block();

        AssignmentTerm assignmentTerm = new AssignmentTerm(LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 2, 15), new ObjectId().toHexString());

        StepVerifier.create(
                repository.existsOverlappingTermForDepartment(new ObjectId().toHexString(), assignmentTerm))
                .expectNext(false)
                .verifyComplete();
    }

    /**
     * DB   --------------|--------|--->
     * Neu  ---------|----------|------>
     */
    @Test
    void existsOverlappingTermForDepartment_startDbBetweenStartAndEndEndDbAfterEnd()
    {
        DepartmentAssignment departmentAssignmentDB = TestHelper.departmentAssignment();
        departmentAssignmentDB.addTerm(new AssignmentTerm(LocalDate.of(2020, 2, 1), LocalDate.of(2020, 3, 1),
                new ObjectId().toHexString()));
        dataRepository.save(departmentAssignmentDB).block();

        AssignmentTerm assignmentTerm = new AssignmentTerm(LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 2, 15), new ObjectId().toHexString());

        StepVerifier.create(
                repository.existsOverlappingTermForDepartment(departmentAssignmentDB.getDepartmentId(),
                        assignmentTerm)).expectNext(true).verifyComplete();
    }

    /**
     * DB   ---------|---------------|->
     * Neu  --------------|--------|--->
     */
    @Test
    void existsOverlappingTermForDepartment_startDbBeforeStartEndDbAfterEnd()
    {
        DepartmentAssignment departmentAssignmentDB = TestHelper.departmentAssignment();
        departmentAssignmentDB.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(departmentAssignmentDB).block();

        AssignmentTerm assignmentTerm = new AssignmentTerm(LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 1, 16), new ObjectId().toHexString());

        StepVerifier.create(
                repository.existsOverlappingTermForDepartment(departmentAssignmentDB.getDepartmentId(),
                        assignmentTerm)).expectNext(true).verifyComplete();
    }

    /**
     * DB   -----------------|--|------>
     * Neu  --------------|--------|--->
     */
    @Test
    void existsOverlappingTermForDepartment_startDbAndEndDbBetweenStartAndEnd()
    {
        DepartmentAssignment departmentAssignmentDB = TestHelper.departmentAssignment();
        departmentAssignmentDB.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 16), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(departmentAssignmentDB).block();

        AssignmentTerm assignmentTerm = new AssignmentTerm(LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 2, 15), new ObjectId().toHexString());

        StepVerifier.create(
                repository.existsOverlappingTermForDepartment(departmentAssignmentDB.getDepartmentId(),
                        assignmentTerm)).expectNext(true).verifyComplete();
    }

    /**
     * DB   ---------|----------|------>
     * Neu  --------------|--------|--->
     */
    @Test
    void existsOverlappingTermForDepartment_noOverlapping()
    {
        DepartmentAssignment departmentAssignmentDB = TestHelper.departmentAssignment();
        departmentAssignmentDB.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(departmentAssignmentDB).block();

        AssignmentTerm assignmentTerm = new AssignmentTerm(LocalDate.of(2020, 2, 15),
                LocalDate.of(2020, 3, 15), new ObjectId().toHexString());

        StepVerifier.create(
                repository.existsOverlappingTermForDepartment(departmentAssignmentDB.getDepartmentId(),
                        assignmentTerm)).expectNext(false).verifyComplete();
    }

    @Test
    void termsForDepartment_correctlyFilteredByDepartment()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(List.of(TestHelper.assignmentTerm()));

        DepartmentAssignment departmentAssignment2 = TestHelper.departmentAssignment();
        departmentAssignment2.addTerm(TestHelper.assignmentTerm());
        dataRepository.save(departmentAssignment).block();
        dataRepository.save(departmentAssignment2).block();

        StepVerifier.create(repository.getAllAssignmentTermsForDepartmentSortedByDate(
                departmentAssignment.getDepartmentId(), SortOrder.ASC)).expectNextMatches(termDto -> {
            assertThat(termDto.getDepartmentAssignmentId(), is(equalTo(departmentAssignment.getId())));
            assertThat(termDto.getDepartmentId(), is(equalTo(departmentAssignment.getDepartmentId())));
            return true;
        }).verifyComplete();
    }

    @Test
    void termsForDepartment_correctlySorted()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(repository.getAllAssignmentTermsForDepartmentSortedByDate(
                departmentAssignment.getDepartmentId(), SortOrder.ASC))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term1"))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term2"))
                .verifyComplete();

        StepVerifier.create(repository.getAllAssignmentTermsForDepartmentSortedByDate(
                departmentAssignment.getDepartmentId(), SortOrder.DESC))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term2"))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term1"))
                .verifyComplete();
    }

    @Test
    void activeTermForDepartment()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.now(), LocalDate.now().plusDays(1), "term2")));
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(
                repository.getActiveAssignmentTermForDepartment(departmentAssignment.getDepartmentId()))
                .expectNextMatches(depDto -> depDto.getAssignmentTerm().getDescription().equals("term2"))
                .verifyComplete();
    }

    @Test
    void activeTermForDepartment_nonePresent()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(
                repository.getActiveAssignmentTermForDepartment(departmentAssignment.getDepartmentId()))
                .verifyComplete();
    }

    @Test
    void activeTermForDepartment_departmentCorrectlyFiltered()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.now(), LocalDate.now().plusDays(1), "term2")));
        dataRepository.save(departmentAssignment).block();
        DepartmentAssignment departmentAssignment2 = TestHelper.departmentAssignment();
        dataRepository.save(departmentAssignment2).block();

        StepVerifier.create(
                repository.getActiveAssignmentTermForDepartment(departmentAssignment2.getDepartmentId()))
                .verifyComplete();
    }

}
