package org.c4.departmentservice.infrastructure.repositories.department;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

/**
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class MongoDepartmentRepositoryImplTest extends IntegrationTest
{
    @Autowired
    private DepartmentDataRepository dataRepository;
    @Autowired
    private DepartmentRepository     departmentRepository;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void existsById()
    {
        Department department = TestHelper.department();
        dataRepository.save(department).block();

        StepVerifier.create(departmentRepository.existsById(department.getId()))
                .expectNext(true)
                .verifyComplete();

        StepVerifier.create(departmentRepository.existsById("anyOtherId")).expectNext(false).verifyComplete();
    }

    @Test
    void existsByName()
    {
        String name = new ObjectId().toHexString();
        dataRepository.save(TestHelper.department(name)).block();

        StepVerifier.create(departmentRepository.existsByName(name)).expectNext(true).verifyComplete();

        StepVerifier.create(departmentRepository.existsByName("nonExistingName"))
                .expectNext(false)
                .verifyComplete();
    }

    @Test
    void existsByNameExcludingSelf()
    {
        String name = new ObjectId().toHexString();
        Department department = TestHelper.department(name);
        dataRepository.save(department).block();

        StepVerifier.create(departmentRepository.existsByNameExcludingSelf(department.getId(), name))
                .expectNext(false)
                .verifyComplete();

        StepVerifier.create(
                departmentRepository.existsByNameExcludingSelf(new ObjectId().toHexString(), name))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    void save()
    {
        Department department = TestHelper.department();

        StepVerifier.create(departmentRepository.save(department)).expectNextCount(1L).verifyComplete();

        StepVerifier.create(dataRepository.findAll())
                .expectNextMatches(d -> d.getId().equals(department.getId()))
                .verifyComplete();
    }

    @Test
    void getAll()
    {
        Department department = TestHelper.department();
        Department department2 = TestHelper.department();
        dataRepository.save(department).block();
        dataRepository.save(department2).block();

        StepVerifier.create(departmentRepository.getAll())
                .expectNextMatches(d -> d.getId().equals(department.getId()))
                .expectNextMatches(d -> d.getId().equals(department2.getId()))
                .verifyComplete();
    }

    @Test
    void getById()
    {
        Department department = TestHelper.department();
        dataRepository.save(department).block();

        StepVerifier.create(departmentRepository.getById(department.getId()))
                .expectNextCount(1L)
                .verifyComplete();

        StepVerifier.create(departmentRepository.getById(new ObjectId().toHexString())).verifyComplete();
    }
}
