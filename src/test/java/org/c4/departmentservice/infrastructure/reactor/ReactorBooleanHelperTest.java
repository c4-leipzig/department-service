package org.c4.departmentservice.infrastructure.reactor;

import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Testet die Funktionalität des {@link ReactorBooleanHelper}s.
 * <br/>
 * Copyright: Copyright (c) 21.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class ReactorBooleanHelperTest
{
    @Test
    void throwIfFalse()
    {
        assertThrows(InvalidOperationException.class,
                () -> ReactorBooleanHelper.throwIfFalse(false, "anyString"));

        assertDoesNotThrow(() -> ReactorBooleanHelper.throwIfFalse(true, "anyError"));
    }

    @Test
    void throwIfFalseList()
    {
        assertThrows(InvalidOperationException.class,
                () -> ReactorBooleanHelper.throwIfFalse(false, Collections.singletonList("anyString")));

        assertDoesNotThrow(
                () -> ReactorBooleanHelper.throwIfFalse(true, Collections.singletonList("anyError")));
    }

    @Test
    void throwIfTrue()
    {
        assertThrows(InvalidOperationException.class,
                () -> ReactorBooleanHelper.throwIfTrue(true, "anyString"));

        assertDoesNotThrow(() -> ReactorBooleanHelper.throwIfTrue(false, "anyError"));
    }

    @Test
    void throwIfTrueList()
    {
        assertThrows(InvalidOperationException.class,
                () -> ReactorBooleanHelper.throwIfTrue(true, Collections.singletonList("anyString")));

        assertDoesNotThrow(
                () -> ReactorBooleanHelper.throwIfTrue(false, Collections.singletonList("anyError")));
    }
}
