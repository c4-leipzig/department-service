package org.c4.departmentservice.infrastructure.converter;

import org.c4.departmentservice.domain.exceptions.InvalidParameterException;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.c4.departmentservice.domain.general.sort.SortOrder.ASC;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * <br/>
 * Copyright: Copyright (c) 14.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class SortOrderConverterTest
{
    @Test
    void convert_success()
    {
        SortOrder result = new SortOrderConverter().convert(ASC.toString());
        assertThat(result, Matchers.is(Matchers.equalTo(ASC)));
    }

    @Test
    void convert_couldNotConvert()
    {
        assertThrows(InvalidParameterException.class, () -> new SortOrderConverter().convert("ascc"));
    }
}
