package org.c4.departmentservice.infrastructure;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class LocalDateHelperTest
{
    @Test
    void inOrder_startNull()
    {
        assertThat(LocalDateHelper.inOrder(null, LocalDate.MIN), is(equalTo(true)));

    }

    @Test
    void inOrder_endNull()
    {
        assertThat(LocalDateHelper.inOrder(LocalDate.MAX, null), is(equalTo(true)));
    }

    @Test
    void inOrder_startBeforeEnd()
    {
        assertThat(LocalDateHelper.inOrder(LocalDate.of(2020, 8, 6), LocalDate.of(2020, 8, 7)),
                is(equalTo(true)));
    }

    @Test
    void inOrder_startEqualsEnd()
    {
        assertThat(LocalDateHelper.inOrder(LocalDate.of(2020, 8, 7), LocalDate.of(2020, 8, 7)),
                is(equalTo(false)));
    }

    @Test
    void inOrder_startAfterEnd()
    {
        assertThat(LocalDateHelper.inOrder(LocalDate.of(2020, 8, 8), LocalDate.of(2020, 8, 7)),
                is(equalTo(false)));
    }
}
