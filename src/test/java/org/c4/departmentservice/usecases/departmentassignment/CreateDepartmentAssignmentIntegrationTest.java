package org.c4.departmentservice.usecases.departmentassignment;

import org.bson.types.ObjectId;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.infrastructure.repositories.departmentassignment.DepartmentAssignmentDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.c4.microservice.framework.domain.user.UserRepository;
import org.c4.microservice.framework.domain.user.UserType;
import org.c4.microservice.framework.infrastructure.CouldNotRetrieveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * IntegrationTest für den {@link CreateDepartmentAssignmentUseCase}.
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class CreateDepartmentAssignmentIntegrationTest extends IntegrationTest
{
    @Autowired
    private CreateDepartmentAssignmentUseCase  createUseCase;
    @Autowired
    private DepartmentAssignmentDataRepository dataRepository;
    @MockBean
    private DepartmentRepository               departmentRepository;
    @MockBean
    private UserRepository                     userRepository;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void createDepartmentAssignment_success()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        CreateDepartmentAssignmentCommand command = TestHelper.createDepartmentAssignmentCommand();
        StepVerifier.create(createUseCase.createDepartmentAssignment(command, TestHelper.departmentAdmin()))
                .expectNextCount(1L)
                .verifyComplete();

        StepVerifier.create(dataRepository.findAll())
                .expectNextMatches(da -> da.equalsRegardingDepartmentAndUser(command.toDepartmentAssignment()))
                .verifyComplete();
    }

    @Test
    void createDepartmentAssignment_notAuthorized()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        CreateDepartmentAssignmentCommand command = TestHelper.createDepartmentAssignmentCommand();
        StepVerifier.create(createUseCase.createDepartmentAssignment(command, TestHelper.member()))
                .expectError(NotAuthorizedException.class)
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_descriptionLengthInvalid()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                TestHelper.stringOfLength(4001), new ObjectId().toHexString(), new ObjectId().toHexString());
        StepVerifier.create(createUseCase.createDepartmentAssignment(command, TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DESCRIPTION_LENGTH_INVALID))
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_departmentIdRequired()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                new ObjectId().toHexString(), "", new ObjectId().toHexString());
        StepVerifier.create(createUseCase.createDepartmentAssignment(command, TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_ID_REQUIRED))
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_userIdRequired()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        CreateDepartmentAssignmentCommand command = new CreateDepartmentAssignmentCommand(
                new ObjectId().toHexString(), new ObjectId().toHexString(), "");
        StepVerifier.create(createUseCase.createDepartmentAssignment(command, TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.USER_ID_REQUIRED))
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_assignmentExistsForDepartmentAndUser()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        CreateDepartmentAssignmentCommand command = TestHelper.createDepartmentAssignmentCommand();
        DepartmentAssignment departmentAssignment = command.toDepartmentAssignment();
        departmentAssignment.updateVersion(1L);
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(createUseCase.createDepartmentAssignment(command, TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e,
                        ErrorKey.DEPARTMENT_ASSIGNMENT_ALREADY_EXISTS))
                .verify();

        StepVerifier.create(dataRepository.findAll()).expectNextCount(1L).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_departmentNotFound()
    {
        doReturn(Mono.just(UserType.MEMBER)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.empty()).when(departmentRepository).getById(any());

        StepVerifier.create(
                createUseCase.createDepartmentAssignment(TestHelper.createDepartmentAssignmentCommand(),
                        TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_NOT_FOUND))
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_userNotFound()
    {
        doReturn(Mono.error(new CouldNotRetrieveException())).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(
                createUseCase.createDepartmentAssignment(TestHelper.createDepartmentAssignmentCommand(),
                        TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.USER_NOT_FOUND))
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

    @Test
    void createDepartmentAssignment_userTypeInvalid()
    {
        doReturn(Mono.just(UserType.GUEST)).when(userRepository).getTypeForUser(any());
        doReturn(Mono.just(TestHelper.department())).when(departmentRepository).getById(any());

        StepVerifier.create(
                createUseCase.createDepartmentAssignment(TestHelper.createDepartmentAssignmentCommand(),
                        TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.USER_TYPE_INVALID))
                .verify();

        StepVerifier.create(dataRepository.findAll()).verifyComplete();
    }

}
