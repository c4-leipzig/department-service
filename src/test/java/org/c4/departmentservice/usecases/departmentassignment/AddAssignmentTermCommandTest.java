package org.c4.departmentservice.usecases.departmentassignment;

import org.bson.types.ObjectId;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.AddAssignmentTermCommand;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.time.LocalDate;

/**
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class AddAssignmentTermCommandTest
{
    @Test
    void validate_success()
    {
        AddAssignmentTermCommand command = new AddAssignmentTermCommand(new ObjectId().toHexString(),
                LocalDate.of(2020, 8, 7), LocalDate.of(2020, 8, 8), new ObjectId().toHexString());

        StepVerifier.create(command.validate()).verifyComplete();
    }

    @Test
    void validate_descriptionLengthInvalid()
    {
        AddAssignmentTermCommand command = new AddAssignmentTermCommand(new ObjectId().toHexString(),
                LocalDate.of(2020, 8, 7), LocalDate.of(2020, 8, 8), TestHelper.stringOfLength(4001));

        StepVerifier.create(command.validate())
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DESCRIPTION_LENGTH_INVALID))
                .verify();
    }

    @Test
    void validate_startNull()
    {
        AddAssignmentTermCommand command = new AddAssignmentTermCommand(new ObjectId().toHexString(), null,
                LocalDate.of(2020, 8, 8), new ObjectId().toHexString());

        StepVerifier.create(command.validate())
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.START_DATE_REQUIRED))
                .verify();
    }

    @Test
    void validate_endNull()
    {
        AddAssignmentTermCommand command = new AddAssignmentTermCommand(new ObjectId().toHexString(),
                LocalDate.of(2020, 8, 8), null, new ObjectId().toHexString());

        StepVerifier.create(command.validate())
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.END_DATE_REQUIRED))
                .verify();
    }

    @Test
    void validate_startAfterEnd()
    {
        AddAssignmentTermCommand command = new AddAssignmentTermCommand(new ObjectId().toHexString(),
                LocalDate.of(2020, 8, 9), LocalDate.of(2020, 8, 8), new ObjectId().toHexString());

        StepVerifier.create(command.validate())
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.START_AFTER_END))
                .verify();
    }

}
