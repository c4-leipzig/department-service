package org.c4.departmentservice.usecases.departmentassignment;

import org.bson.types.ObjectId;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.events.AssignmentTermAddedEvent;
import org.c4.departmentservice.infrastructure.repositories.departmentassignment.DepartmentAssignmentDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.AddAssignmentTermCommand;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

/**
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class AddAssignmentTermIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentAssignmentDataRepository dataRepository;
    @Autowired
    private AddAssignmentTermUseCase           addUseCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void addAssignmentTerm_success() throws Exception
    {
        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 8, 7),
                        LocalDate.of(2020, 8, 8), new ObjectId().toHexString()),
                TestHelper.departmentAdmin())).verifyComplete();

        verify(eventPublisher, times(1)).send(any(AssignmentTermAddedEvent.class));
        Thread.sleep(100);

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 2L && da.getTerms().size() == 1)
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_success_overlappingTermExistsForOtherDepartment() throws Exception
    {
        DepartmentAssignment otherAssignment = TestHelper.departmentAssignment();
        otherAssignment.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(otherAssignment).block();

        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 1, 7),
                        LocalDate.of(2020, 1, 8), new ObjectId().toHexString()),
                TestHelper.departmentAdmin())).verifyComplete();

        verify(eventPublisher, times(1)).send(any(AssignmentTermAddedEvent.class));
        Thread.sleep(100);

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 2L && da.getTerms().size() == 1)
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_notAuthorized()
    {
        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 1, 7),
                        LocalDate.of(2020, 1, 8), new ObjectId().toHexString()), TestHelper.member()))
                .expectError(NotAuthorizedException.class)
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 1L && da.getTerms().isEmpty())
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_descriptionLengthInvalid()
    {
        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 1, 7),
                        LocalDate.of(2020, 1, 8), TestHelper.stringOfLength(4001)),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DESCRIPTION_LENGTH_INVALID))
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 1L && da.getTerms().isEmpty())
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_startNull()
    {
        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), null, LocalDate.of(2020, 1, 8),
                        new ObjectId().toHexString()), TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.START_DATE_REQUIRED))
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 1L && da.getTerms().isEmpty())
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_endNull()
    {
        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 1, 7), null, ""),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.END_DATE_REQUIRED))
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 1L && da.getTerms().isEmpty())
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_startAfterEnd()
    {
        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 1, 9),
                        LocalDate.of(2020, 1, 8), ""), TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e, ErrorKey.START_AFTER_END))
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 1L && da.getTerms().isEmpty())
                .verifyComplete();
    }

    @Test
    void addAssignmentTerm_departmentAssignmentNotFound()
    {
        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(new ObjectId().toHexString(), LocalDate.of(2020, 1, 7),
                        LocalDate.of(2020, 1, 8), ""), TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e,
                        ErrorKey.DEPARTMENT_ASSIGNMENT_NOT_FOUND))
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));
    }

    @Test
    void addAssignmentTerm_overlappingTermExists()
    {

        DepartmentAssignment assignment = TestHelper.departmentAssignment();
        dataRepository.save(assignment).block();

        DepartmentAssignment otherAssignment = DepartmentAssignment.create("", assignment.getDepartmentId(),
                new ObjectId().toHexString());
        otherAssignment.addTerm(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1),
                new ObjectId().toHexString()));
        dataRepository.save(otherAssignment).block();

        StepVerifier.create(addUseCase.addAssignmentTerm(
                new AddAssignmentTermCommand(assignment.getId(), LocalDate.of(2020, 1, 7),
                        LocalDate.of(2020, 1, 8), new ObjectId().toHexString()),
                TestHelper.departmentAdmin()))
                .expectErrorMatches(e -> TestHelper.invalidOperationContaining(e,
                        ErrorKey.OVERLAPPING_ASSIGNMENT_TERM_EXISTS))
                .verify();

        verify(eventPublisher, never()).send(any(AssignmentTermAddedEvent.class));

        StepVerifier.create(dataRepository.findById(assignment.getId()))
                .expectNextMatches(da -> da.getVersion() == 1L && da.getTerms().isEmpty())
                .verifyComplete();
    }

}
