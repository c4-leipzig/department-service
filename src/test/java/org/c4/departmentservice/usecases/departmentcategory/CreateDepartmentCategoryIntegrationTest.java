package org.c4.departmentservice.usecases.departmentcategory;

import org.bson.types.ObjectId;
import org.c4.departmentservice.infrastructure.repositories.departmentcategory.DepartmentCategoryDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 * IntegrationTest für den {@link CreateDepartmentCategoryUseCase}.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class CreateDepartmentCategoryIntegrationTest extends IntegrationTest
{
    private static final String CATEGORY_NAME = new ObjectId().toHexString();

    @Autowired
    private DepartmentCategoryDataRepository dataRepository;
    @Autowired
    private CreateDepartmentCategoryUseCase  createDepartmentCategoryUseCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().subscribe();
    }

    /**
     * Der UseCase beendet erfolgreich. Die Category wird korrekt persistiert.
     */
    @Test
    void createDepartmentCategory_success()
    {
        Mono<IdDto> departmentCategoryMono = createDepartmentCategoryUseCase.createDepartmentCategory(
                CATEGORY_NAME, TestHelper.departmentAdmin());

        StepVerifier.create(departmentCategoryMono).expectNextCount(1).expectComplete().verify();

        StepVerifier.create(dataRepository.findAll())
                .expectNextMatches(departmentCategory -> departmentCategory.getVersion() == 0
                        && departmentCategory.getName().equals(CATEGORY_NAME))
                .expectComplete()
                .verify();
    }

    @Test
    void createDepartmentCategory_notAuthorized()
    {
        StepVerifier.create(
                createDepartmentCategoryUseCase.createDepartmentCategory(CATEGORY_NAME, TestHelper.member()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }
}
