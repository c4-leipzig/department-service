package org.c4.departmentservice.usecases.departmentcategory;

import org.c4.departmentservice.infrastructure.repositories.departmentcategory.DepartmentCategoryDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

/**
 * IntegrationTest für den {@link GetAllDepartmentCategoriesUseCase}.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class GetAllDepartmentCategoriesIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentCategoryDataRepository  dataRepository;
    @Autowired
    private GetAllDepartmentCategoriesUseCase useCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void getAllForDepartmentAdmin()
    {
        dataRepository.save(TestHelper.departmentCategory()).block();
        dataRepository.save(TestHelper.departmentCategory()).block();

        StepVerifier.create(useCase.getAllDepartmentCategories(TestHelper.departmentAdmin()))
                .expectNextCount(2)
                .expectComplete()
                .verify();
    }

    @Test
    void getAllForMember()
    {
        dataRepository.save(TestHelper.departmentCategory()).block();
        dataRepository.save(TestHelper.departmentCategory()).block();

        StepVerifier.create(useCase.getAllDepartmentCategories(TestHelper.member()))
                .expectNextCount(2)
                .expectComplete()
                .verify();
    }

    @Test
    void getAll_notAuthorized()
    {
        StepVerifier.create(useCase.getAllDepartmentCategories(TestHelper.noRoleUser()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }
}
