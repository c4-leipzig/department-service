package org.c4.departmentservice.usecases.departmentcategory;

import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.repositories.departmentcategory.DepartmentCategoryDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.c4.departmentservice.ErrorKey.DEPARTMENT_CATEGORY_NOT_FOUND;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class UpdateDepartmentCategoryIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentCategoryDataRepository dataRepository;
    @Autowired
    private UpdateDepartmentCategoryUseCase  useCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void updateDepartmentCategory_nonAdminRejected()
    {
        Mono<Void> result = useCase.updateDepartmentCategory("anyId", "anyName", TestHelper.member());

        StepVerifier.create(result).expectError(NotAuthorizedException.class).verify();
    }

    @Test
    void updateDepartmentCategory_nameTaken()
    {
        dataRepository.save(TestHelper.departmentCategory("nameTaken")).block();

        Mono<Void> result = useCase.updateDepartmentCategory("anyId", "nameTaken",
                TestHelper.departmentAdmin());

        StepVerifier.create(result).expectErrorSatisfies(ex -> {
            assertThat(ex, instanceOf(InvalidOperationException.class));
            assertThat(((InvalidOperationException) ex).getErrors(), contains(ErrorKey.NAME_TAKEN));
        }).verify();
    }

    @Test
    void updateDepartmentCategory_notFound()
    {
        Mono<Void> result = useCase.updateDepartmentCategory("notFoundId", "anyName",
                TestHelper.departmentAdmin());

        StepVerifier.create(result).expectErrorSatisfies(ex -> {
            assertThat(ex, instanceOf(InvalidOperationException.class));
            assertThat(((InvalidOperationException) ex).getErrors(), contains(DEPARTMENT_CATEGORY_NOT_FOUND));
        }).verify();
    }

    @Test
    void updateDepartmentCategory_success() throws InterruptedException
    {
        DepartmentCategory initial = TestHelper.departmentCategory("initialName");
        dataRepository.save(initial).block();

        Mono<Void> result = useCase.updateDepartmentCategory(initial.getId(), "updatedName",
                TestHelper.departmentAdmin());

        StepVerifier.create(result).expectComplete().verify();

        Thread.sleep(200);

        StepVerifier.create(dataRepository.findById(initial.getId()))
                .expectNextMatches(
                        departmentCategory -> departmentCategory.getVersion() == initial.getVersion() + 1
                                && departmentCategory.getName().equals("updatedName"))
                .expectComplete()
                .verify();
    }
}
