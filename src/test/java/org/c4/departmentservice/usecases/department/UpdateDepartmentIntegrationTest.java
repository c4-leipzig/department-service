package org.c4.departmentservice.usecases.department;

import org.bson.types.ObjectId;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.domain.department.events.DepartmentUpdatedEvent;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.repositories.department.DepartmentDataRepository;
import org.c4.departmentservice.infrastructure.repositories.departmentcategory.DepartmentCategoryDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;

import static org.mockito.Mockito.*;

/**
 * {@link IntegrationTest} für den {@link UpdateDepartmentUseCase}.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class UpdateDepartmentIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentDataRepository         dataRepository;
    @MockBean
    private DepartmentCategoryDataRepository departmentCategoryDataRepository;
    @Autowired
    private UpdateDepartmentUseCase          updateUseCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
        doReturn(Mono.just(true)).when(departmentCategoryDataRepository).existsById(anyString());
    }

    @Test
    void updateDepartment_success() throws Exception
    {
        UpdateDepartmentCommand updateDepartmentCommand = TestHelper.updateDepartmentCommand();
        Department department = TestHelper.department();
        department.updateVersion(1L);
        dataRepository.save(department).block();

        StepVerifier.create(updateUseCase.updateDepartment(department.getId(), updateDepartmentCommand,
                TestHelper.departmentAdmin())).verifyComplete();

        Thread.sleep(200);
        StepVerifier.create(dataRepository.findById(department.getId()))
                .expectNextMatches(d -> d.getVersion() == 2L && TestHelper.isEmptyDepartmentChange(
                        d.generateChange(updateDepartmentCommand)))
                .verifyComplete();
        verify(eventPublisher, times(1)).send(any(DepartmentUpdatedEvent.class));
    }

    @Test
    void updateDepartment_staticValidationError()
    {
        UpdateDepartmentCommand updateDepartmentCommand = new UpdateDepartmentCommand(
                new ObjectId().toHexString(), new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.emptyList());
        Department department = TestHelper.department();
        department.updateVersion(1L);
        dataRepository.save(department).block();

        StepVerifier.create(updateUseCase.updateDepartment(department.getId(), updateDepartmentCommand,
                TestHelper.departmentAdmin())).expectError(InvalidOperationException.class).verify();
        verify(eventPublisher, never()).send(any());
    }

    @Test
    void updateDepartment_validationError()
    {
        UpdateDepartmentCommand updateDepartmentCommand = TestHelper.updateDepartmentCommand();

        StepVerifier.create(
                updateUseCase.updateDepartment(new ObjectId().toHexString(), updateDepartmentCommand,
                        TestHelper.departmentAdmin()))
                .expectErrorMatches(
                        e -> TestHelper.invalidOperationContaining(e, ErrorKey.DEPARTMENT_NOT_FOUND))
                .verify();
    }

    @Test
    void updateDepartment_notAuthorized()
    {
        StepVerifier.create(updateUseCase.updateDepartment(new ObjectId().toHexString(),
                TestHelper.updateDepartmentCommand(), TestHelper.member()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }

}
