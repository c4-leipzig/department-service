package org.c4.departmentservice.usecases.department;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.infrastructure.repositories.department.DepartmentDataRepository;
import org.c4.departmentservice.infrastructure.repositories.departmentassignment.DepartmentAssignmentDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.List;

/**
 * <br/>
 * Copyright: Copyright (c) 19.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class GetAllDepartmentsWithActiveAssignmentIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentDataRepository                     departmentRepository;
    @Autowired
    private DepartmentAssignmentDataRepository           departmentAssignmentRepository;
    @Autowired
    private GetAllDepartmentsWithActiveAssignmentUseCase getActiveAssignmentUseCase;

    @BeforeEach
    void setUp()
    {
        departmentRepository.deleteAll().block();
        departmentAssignmentRepository.deleteAll().block();
    }

    @Test
    void getAllDepartmentsWithActiveAssignment_noActivePresentReturnsNull()
    {
        Department department = TestHelper.department();
        departmentRepository.save(department).block();
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment(department.getId());
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        departmentAssignmentRepository.save(departmentAssignment).block();

        StepVerifier.create(
                getActiveAssignmentUseCase.getAllDepartmentsWithActiveAssignment(TestHelper.member()))
                .expectNextMatches(dto -> dto.getDepartment().getId().equals(department.getId())
                        && dto.getAssignmentTerm() == null)
                .verifyComplete();
    }

    @Test
    void getAllDepartmentsWithActiveAssignment_activeIsReturned()
    {
        Department department = TestHelper.department();
        departmentRepository.save(department).block();
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment(department.getId());
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.now(), LocalDate.now().plusDays(1), "term2")));
        departmentAssignmentRepository.save(departmentAssignment).block();

        StepVerifier.create(
                getActiveAssignmentUseCase.getAllDepartmentsWithActiveAssignment(TestHelper.member()))
                .expectNextMatches(dto -> dto.getDepartment().getId().equals(department.getId())
                        && dto.getAssignmentTerm().getAssignmentTerm().getDescription().equals("term2"))
                .verifyComplete();
    }

    @Test
    void getAllDepartmentsWithActiveAssignment_multipleDepartments()
    {
        Department department = TestHelper.department();
        departmentRepository.save(department).block();
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment(department.getId());
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.now(), LocalDate.now().plusDays(1), "term2")));
        departmentAssignmentRepository.save(departmentAssignment).block();

        Department department2 = TestHelper.department();
        departmentRepository.save(department2).block();
        DepartmentAssignment departmentAssignment2 = TestHelper.departmentAssignment(department2.getId());
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        departmentAssignmentRepository.save(departmentAssignment2).block();

        StepVerifier.create(
                getActiveAssignmentUseCase.getAllDepartmentsWithActiveAssignment(TestHelper.member()))
                .expectNextMatches(dto -> dto.getDepartment().getId().equals(department.getId())
                        && dto.getAssignmentTerm() != null)
                .expectNextMatches(dto -> dto.getDepartment().getId().equals(department2.getId())
                        && dto.getAssignmentTerm() == null)
                .verifyComplete();
    }

    @Test
    void getAllDepartmentsWithActiveAssignment_notAuthorized()
    {
        Department department = TestHelper.department();
        departmentRepository.save(department).block();
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment(department.getId());
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        departmentAssignmentRepository.save(departmentAssignment).block();

        StepVerifier.create(
                getActiveAssignmentUseCase.getAllDepartmentsWithActiveAssignment(TestHelper.noRoleUser()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }

}
