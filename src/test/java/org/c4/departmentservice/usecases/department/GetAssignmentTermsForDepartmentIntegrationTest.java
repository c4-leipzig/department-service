package org.c4.departmentservice.usecases.department;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.c4.departmentservice.infrastructure.repositories.departmentassignment.DepartmentAssignmentDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * <br/>
 * Copyright: Copyright (c) 14.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class GetAssignmentTermsForDepartmentIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentAssignmentDataRepository     dataRepository;
    @Autowired
    private GetAssignmentTermsForDepartmentUseCase getUseCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void getTermsForDepartment_notAuthorized()
    {
        StepVerifier.create(getUseCase.getTermsForDepartment(new ObjectId().toHexString(), SortOrder.ASC,
                TestHelper.noRoleUser())).expectError(NotAuthorizedException.class).verify();
    }

    @Test
    void getTermsForDepartment_sortedAsc()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(
                getUseCase.getTermsForDepartment(departmentAssignment.getDepartmentId(), SortOrder.ASC,
                        TestHelper.member()))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term1"))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term2"))
                .verifyComplete();
    }

    @Test
    void getTermsForDepartment_sortedDesc()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(
                List.of(new AssignmentTerm(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 2), "term1"),
                        new AssignmentTerm(LocalDate.of(2020, 1, 3), LocalDate.of(2020, 1, 4), "term2")));
        dataRepository.save(departmentAssignment).block();

        StepVerifier.create(
                getUseCase.getTermsForDepartment(departmentAssignment.getDepartmentId(), SortOrder.DESC,
                        TestHelper.member()))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term2"))
                .expectNextMatches(termDto -> termDto.getAssignmentTerm().getDescription().equals("term1"))
                .verifyComplete();
    }

    @Test
    void getTermsForDepartment_departmentFilteredCorrectly()
    {
        DepartmentAssignment departmentAssignment = TestHelper.departmentAssignment();
        departmentAssignment.addTerms(List.of(TestHelper.assignmentTerm()));

        DepartmentAssignment departmentAssignment2 = TestHelper.departmentAssignment();
        departmentAssignment2.addTerm(TestHelper.assignmentTerm());
        dataRepository.save(departmentAssignment).block();
        dataRepository.save(departmentAssignment2).block();

        StepVerifier.create(
                getUseCase.getTermsForDepartment(departmentAssignment.getDepartmentId(), SortOrder.ASC,
                        TestHelper.member())).expectNextMatches(termDto -> {
            assertThat(termDto.getDepartmentAssignmentId(), is(equalTo(departmentAssignment.getId())));
            assertThat(termDto.getDepartmentId(), is(equalTo(departmentAssignment.getDepartmentId())));
            return true;
        }).verifyComplete();
    }

}
