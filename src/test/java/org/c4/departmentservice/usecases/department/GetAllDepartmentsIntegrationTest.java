package org.c4.departmentservice.usecases.department;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.infrastructure.repositories.department.DepartmentDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

/**
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class GetAllDepartmentsIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentDataRepository dataRepository;
    @Autowired
    private GetAllDepartmentsUseCase getAllUseCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void getAllDepartments_success()
    {
        Department department = TestHelper.department();
        Department department2 = TestHelper.department();
        dataRepository.save(department).block();
        dataRepository.save(department2).block();

        StepVerifier.create(getAllUseCase.getAllDepartments(TestHelper.departmentAdmin()))
                .expectNextMatches(d -> d.getId().equals(department.getId()))
                .expectNextMatches(d -> d.getId().equals(department2.getId()))
                .verifyComplete();
    }

    @Test
    void getAllDepartments_notAuthorized()
    {
        dataRepository.save(TestHelper.department()).block();
        dataRepository.save(TestHelper.department()).block();

        StepVerifier.create(getAllUseCase.getAllDepartments(TestHelper.noRoleUser()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }
}
