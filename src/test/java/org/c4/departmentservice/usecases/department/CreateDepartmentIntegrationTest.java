package org.c4.departmentservice.usecases.department;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.repositories.department.DepartmentDataRepository;
import org.c4.departmentservice.infrastructure.repositories.departmentcategory.DepartmentCategoryDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.*;

/**
 * {@link IntegrationTest} für den {@link CreateDepartmentUseCase}.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class CreateDepartmentIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentDataRepository         departmentDataRepository;
    @MockBean
    private DepartmentCategoryDataRepository departmentCategoryDataRepository;

    @Autowired
    private CreateDepartmentUseCase createDepartmentUseCase;

    @BeforeEach
    void setUp()
    {
        departmentDataRepository.deleteAll().block();
        doReturn(Mono.just(true)).when(departmentCategoryDataRepository).existsById(anyString());
    }

    @Test
    void createDepartment_success()
    {
        Mono<Department> result = createDepartmentUseCase.createDepartment(TestHelper.createDepartmentCommand(),
                TestHelper.departmentAdmin()).map(IdDto::getId).flatMap(departmentDataRepository::findById);

        StepVerifier.create(result)
                .expectNextMatches(departmentWrapper -> departmentWrapper.getVersion() == 0L)
                .verifyComplete();
    }

    @Test
    void createDepartment_validationError()
    {
        Mono<IdDto> result = createDepartmentUseCase.createDepartment(TestHelper.invalidCreateDepartmentCommand(),
                TestHelper.departmentAdmin());

        StepVerifier.create(result).expectError(InvalidOperationException.class).verify();

        StepVerifier.create(departmentDataRepository.count()).expectNext(0L).verifyComplete();
    }

    @Test
    void createDepartment_notAuthorized()
    {
        Mono<IdDto> result = createDepartmentUseCase.createDepartment(TestHelper.createDepartmentCommand(),
                TestHelper.member());

        StepVerifier.create(result).expectError(NotAuthorizedException.class).verify();

        StepVerifier.create(departmentDataRepository.count()).expectNext(0L).verifyComplete();
    }
}
