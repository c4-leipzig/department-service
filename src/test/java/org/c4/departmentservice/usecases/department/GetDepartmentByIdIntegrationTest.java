package org.c4.departmentservice.usecases.department;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.exceptions.NotFoundException;
import org.c4.departmentservice.infrastructure.repositories.department.DepartmentDataRepository;
import org.c4.departmentservice.testhelper.TestHelper;
import org.c4.microservice.framework.configuration.IntegrationTest;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

/**
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class GetDepartmentByIdIntegrationTest extends IntegrationTest
{
    @Autowired
    private DepartmentDataRepository dataRepository;
    @Autowired
    private GetDepartmentByIdUseCase getByIdUseCase;

    @BeforeEach
    void setUp()
    {
        dataRepository.deleteAll().block();
    }

    @Test
    void getDepartmentById_success()
    {
        Department department = TestHelper.department();
        dataRepository.save(department).block();

        StepVerifier.create(
                getByIdUseCase.getDepartmentById(department.getId(), TestHelper.departmentAdmin()))
                .expectNextMatches(d -> d.getId().equals(department.getId()))
                .verifyComplete();
    }

    @Test
    void getDepartmentById_notFound()
    {
        StepVerifier.create(
                getByIdUseCase.getDepartmentById(new ObjectId().toHexString(), TestHelper.departmentAdmin()))
                .expectError(NotFoundException.class)
                .verify();
    }

    @Test
    void getDepartmentById_notAuthorized()
    {
        Department department = TestHelper.department();
        dataRepository.save(department).block();

        StepVerifier.create(getByIdUseCase.getDepartmentById(department.getId(), TestHelper.noRoleUser()))
                .expectError(NotAuthorizedException.class)
                .verify();
    }
}
