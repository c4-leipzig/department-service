package org.c4.departmentservice.testhelper;

import org.bson.types.ObjectId;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentChange;
import org.c4.departmentservice.domain.department.events.DepartmentCreatedEvent;
import org.c4.departmentservice.domain.department.events.DepartmentUpdatedEvent;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentTermDto;
import org.c4.departmentservice.domain.departmentassignment.events.DepartmentAssignmentCreatedEvent;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryChange;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.usecases.department.commands.CreateDepartmentCommand;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;
import org.c4.microservice.framework.communication.delta.DeltaProperty;
import org.c4.microservice.framework.communication.delta.ListDeltaProperty;
import org.c4.microservice.framework.domain.user.Role;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domain.user.UserType;

import java.nio.CharBuffer;
import java.time.LocalDate;
import java.util.Collections;

/**
 * HelperClass zur Erzeugung von Testinstanzen.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class TestHelper
{
    public static User departmentAdmin()
    {
        return new User(new ObjectId().toHexString(), Collections.singletonList(Role.DEPARTMENT_ADMIN));
    }

    public static User member()
    {
        return new User(new ObjectId().toHexString(), Collections.singletonList(Role.MEMBER));
    }

    public static User noRoleUser()
    {
        return new User(new ObjectId().toHexString(), Collections.emptyList());
    }

    public static DepartmentCategory departmentCategory()
    {
        return new DepartmentCategory(new ObjectId().toHexString(), new ObjectId().toHexString());
    }

    public static DepartmentCategory departmentCategory(String name)
    {
        DepartmentCategory departmentCategory = DepartmentCategory.create(name);
        departmentCategory.updateVersion(1L);
        return departmentCategory;
    }

    public static DepartmentCategoryChange departmentCategoryChange()
    {
        return new DepartmentCategoryChange("anyName");
    }

    public static Department department()
    {
        return Department.create(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), Collections.singletonList(UserType.MEMBER));
    }

    public static Department department(String name)
    {
        return Department.create(name, new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER));
    }

    public static DepartmentCreatedEvent departmentCreatedEvent()
    {
        return new DepartmentCreatedEvent(department(), departmentAdmin());
    }

    public static String stringOfLength(int n)
    {
        return CharBuffer.allocate(n).toString().replace('\0', 'a');
    }

    public static CreateDepartmentCommand createDepartmentCommand()
    {
        return new CreateDepartmentCommand(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), Collections.singletonList(UserType.MEMBER.toString()));
    }

    public static CreateDepartmentCommand invalidCreateDepartmentCommand()
    {
        return new CreateDepartmentCommand(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), null);
    }

    public static DepartmentAssignment departmentAssignment(String departmentId)
    {
        DepartmentAssignment departmentAssignment = DepartmentAssignment.create(new ObjectId().toHexString(),
                departmentId, new ObjectId().toHexString());
        departmentAssignment.updateVersion(1L);
        return departmentAssignment;
    }

    public static DepartmentAssignment departmentAssignment()
    {
        return departmentAssignment(new ObjectId().toHexString());
    }

    public static DepartmentAssignmentCreatedEvent departmentAssignmentCreatedEvent()
    {
        return new DepartmentAssignmentCreatedEvent(departmentAssignment(), departmentAdmin());
    }

    public static boolean invalidOperationContaining(Throwable e, String error)
    {
        return e instanceof InvalidOperationException && ((InvalidOperationException) e).getErrors()
                .contains(error);
    }

    public static CreateDepartmentAssignmentCommand createDepartmentAssignmentCommand()
    {
        return new CreateDepartmentAssignmentCommand(new ObjectId().toHexString(),
                new ObjectId().toHexString(), new ObjectId().toHexString());
    }

    public static AssignmentTerm assignmentTerm()
    {
        return new AssignmentTerm(LocalDate.of(2020, 8, 7), LocalDate.of(2020, 8, 8),
                new ObjectId().toHexString());
    }

    public static UpdateDepartmentCommand updateDepartmentCommand()
    {
        return new UpdateDepartmentCommand(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), Collections.singletonList(UserType.MEMBER.toString()));
    }

    public static UpdateDepartmentCommand updateDepartmentCommand(String name)
    {
        return new UpdateDepartmentCommand(name, new ObjectId().toHexString(), new ObjectId().toHexString(),
                Collections.singletonList(UserType.MEMBER.toString()));
    }

    public static UpdateDepartmentCommand invalidUpdateDepartmentCommand()
    {
        return new UpdateDepartmentCommand(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), null);
    }

    public static DepartmentChange departmentChange()
    {
        return new DepartmentChange(DeltaProperty.of(new ObjectId().toHexString()),
                DeltaProperty.of(new ObjectId().toHexString()),
                DeltaProperty.of(new ObjectId().toHexString()),
                ListDeltaProperty.of(Collections.singletonList(UserType.MEMBER)));
    }

    public static boolean isEmptyDepartmentChange(DepartmentChange departmentChange)
    {
        return departmentChange.getName().isDoNotChange() && departmentChange.getDescription().isDoNotChange()
                && departmentChange.getDepartmentCategoryId().isDoNotChange()
                && departmentChange.getPossibleUserTypes().isDoNotChange();
    }

    public static DepartmentUpdatedEvent departmentUpdatedEvent()
    {
        return new DepartmentUpdatedEvent(new ObjectId().toHexString(), 1L, member(), departmentChange());

    }

    public static DepartmentAssignmentTermDto departmentAssignmentTermDto()
    {
        return new DepartmentAssignmentTermDto(new ObjectId().toHexString(), new ObjectId().toHexString(),
                new ObjectId().toHexString(), assignmentTerm());
    }
}
