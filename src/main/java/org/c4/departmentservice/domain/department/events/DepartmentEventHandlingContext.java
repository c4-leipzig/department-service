package org.c4.departmentservice.domain.department.events;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * HandlingContext zur Verarbeitung von {@link Department}-Events.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class DepartmentEventHandlingContext
{
    private final DepartmentRepository departmentRepository;

    /**
     * Überprüft, ob das Department in der Persistenz vorhanden ist.
     */
    public Mono<Boolean> departmentExists(Department department)
    {
        return departmentRepository.existsById(department.getId());
    }

    /**
     * Speichert das Department in der lokalen Persistenz.
     */
    public Mono<Void> saveDepartment(Department department)
    {
        return departmentRepository.save(department).then();
    }

    public Mono<Department> getDepartment(String id)
    {
        return departmentRepository.getById(id);
    }
}
