package org.c4.departmentservice.domain.department;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.c4.microservice.framework.communication.delta.DeltaProperty;
import org.c4.microservice.framework.communication.delta.ListDeltaProperty;
import org.c4.microservice.framework.domain.user.UserType;

import java.util.List;

/**
 * Value Object, über das ein getUpdated an einem {@link Department} persistiert wird.
 * <br/>
 * Copyright: Copyright (c) 08.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@AllArgsConstructor
public class DepartmentChange
{
    final DeltaProperty<String>       name;
    final DeltaProperty<String>       description;
    final DeltaProperty<String>       departmentCategoryId;
    final ListDeltaProperty<UserType> possibleUserTypes;

    public String getUpdatedName(String oldName)
    {
        return name.apply(oldName);
    }

    public String getUpdatedDescription(String oldDescription)
    {
        return description.apply(oldDescription);
    }

    public List<UserType> getUpdatedPossibleUserTypes(List<UserType> oldPossibleUserTypes)
    {
        return possibleUserTypes.apply(oldPossibleUserTypes);
    }

    public String getUpdatedDepartmentCategoryId(String oldDepartmentCategoryId)
    {
        return departmentCategoryId.apply(oldDepartmentCategoryId);
    }
}
