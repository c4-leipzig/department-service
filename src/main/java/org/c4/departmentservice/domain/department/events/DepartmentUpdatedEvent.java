package org.c4.departmentservice.domain.department.events;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentChange;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.UpdateDomainEvent;
import reactor.core.publisher.Mono;

/**
 * Event, über das ein Update an einem {@link Department} kommuniziert wird.
 * <br/>
 * Copyright: Copyright (c) 09.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DepartmentUpdatedEvent extends UpdateDomainEvent<Department>
{
    private DepartmentChange change;

    public DepartmentUpdatedEvent(String aggregateId, Long version, User updater, DepartmentChange change)
    {
        super(aggregateId, version, updater);
        this.change = change;
    }

    @Override
    protected Mono<Void> apply(DomainEventHandlingContext handlingContext, Department aggregate)
    {
        log.debug("Handling DepartmentUpdatedEvent for Department '{}'", this::getAggregateId);
        aggregate.handleChange(change);
        aggregate.updateVersion(getVersion());
        return handlingContext.saveDepartment(aggregate);
    }

    @Override
    protected Mono<Department> getAggregate(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getDepartment(getAggregateId());
    }
}
