package org.c4.departmentservice.domain.department.validation;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.usecases.department.commands.CreateDepartmentCommand;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.infrastructure.ListHelper;
import org.c4.departmentservice.infrastructure.StringHelper;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.microservice.framework.domain.user.UserType;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * Statische Validierung von {@link CreateDepartmentCommand} und {@link UpdateDepartmentCommand}.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DepartmentCommandValidator
{
    static Mono<Void> validate(CreateDepartmentCommand command)
    {
        return validate(command.getName(), command.getDescription(), command.getDepartmentCategoryId(),
                command.getPossibleUserTypes());
    }

    static Mono<Void> validate(UpdateDepartmentCommand command)
    {
        return validate(command.getName(), command.getDescription(), command.getDepartmentCategoryId(),
                command.getPossibleUserTypes());
    }

    private static Mono<Void> validate(String name, String description, String departmentCategoryId,
            List<String> possibleUserTypes)
    {
        VoidProcessingResult result = new VoidProcessingResult();

        result.addErrorIf(ErrorKey.NAME_EMPTY, name == null || name.isBlank());
        result.addErrorIf(ErrorKey.NAME_LENGTH_INVALID, StringHelper.exceedsMaxLength(name, 100));

        result.addErrorIf(ErrorKey.DESCRIPTION_LENGTH_INVALID,
                StringHelper.exceedsMaxLength(description, 4000));

        result.addErrorIf(ErrorKey.DEPARTMENT_CATEGORY_REQUIRED,
                departmentCategoryId == null || departmentCategoryId.isBlank());

        result.addErrorIf(ErrorKey.USER_TYPE_REQUIRED,
                possibleUserTypes == null || possibleUserTypes.isEmpty());
        result.addErrorIf(ErrorKey.USER_TYPE_INVALID, !UserType.allExist(possibleUserTypes));
        result.addErrorIf(ErrorKey.USER_TYPE_DUPLICATES, ListHelper.containsDuplicates(possibleUserTypes));

        return result.monoThrowIfErroneous();
    }
}
