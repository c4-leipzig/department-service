package org.c4.departmentservice.domain.department;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentTermDto;

/**
 * Dto zur Übermtittlung eines {@link Department}s zusammen mit dem gerade aktiven {@link AssignmentTerm}.
 * <br/>
 * Copyright: Copyright (c) 19.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@AllArgsConstructor
public class DepartmentWithActiveAssignmentDto
{
    private final Department department;
    private final DepartmentAssignmentTermDto assignmentTerm;
}
