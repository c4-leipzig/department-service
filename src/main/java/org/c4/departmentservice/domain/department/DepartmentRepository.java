package org.c4.departmentservice.domain.department;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository zur Persistierung von {@link Department}s.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DepartmentRepository
{
    /**
     * Überprüft, ob es ein Department mit der entsprechenden Id gibt.
     */
    Mono<Boolean> existsById(String id);

    /**
     * Überprüft, ob ein Name für ein Department vergeben ist.
     */
    Mono<Boolean> existsByName(String name);

    /**
     * Überprüft, ob es bereits ein *anderes* {@link Department} mit dem angegebenen Namen gibt.
     */
    Mono<Boolean> existsByNameExcludingSelf(String id, String name);

    /**
     * Speichert das Department ab.
     */
    Mono<Department> save(Department department);

    /**
     * Gibt einen Stream mit allen vorhandenen Departments zurück.
     */
    Flux<Department> getAll();

    /**
     * Liefert ein spezifisches {@link Department} anhand seiner Id zurück.
     */
    Mono<Department> getById(String id);
}
