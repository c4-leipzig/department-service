package org.c4.departmentservice.domain.department.events;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import reactor.core.publisher.Mono;

/**
 * Event über das die Erzeugung eines {@link Department}s publiziert wird.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DepartmentCreatedEvent extends CreateDomainEvent
{
    private Department department;

    public DepartmentCreatedEvent(Department department, User creator)
    {
        super(department.getId(), creator);
        this.department = department;
    }

    @Override
    protected Mono<Void> apply(DomainEventHandlingContext handlingContext)
    {
        log.debug("Received DepartmentCreatedEvent for aggregate '{}'", this::getAggregateId);
        department.updateVersion(getVersion());
        return handlingContext.saveDepartment(department);
    }

    @Override
    protected Mono<Boolean> aggregatePresent(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.departmentExists(department);
    }
}
