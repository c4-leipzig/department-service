package org.c4.departmentservice.domain.department;

import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.infrastructure.IdGenerator;
import org.c4.microservice.framework.communication.delta.DeltaProperty;
import org.c4.microservice.framework.communication.delta.ListDeltaProperty;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domain.user.UserType;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Entity, welche ein Department repräsentiert.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Document(collection = "departments")
public class Department extends AbstractEntity
{
    private String         name;
    private String         description;
    private String         departmentCategoryId;
    private List<UserType> possibleUserTypes;

    public Department(String id, String name, String description, String departmentCategoryId,
            List<UserType> possibleUserTypes)
    {
        super(id);
        this.name = name;
        this.description = description;
        this.departmentCategoryId = departmentCategoryId;
        this.possibleUserTypes = possibleUserTypes;
    }

    public static Department create(String name, String description, String departmentCategoryId,
            List<UserType> possibleUserTypes)
    {
        return new Department(IdGenerator.generateNew(), name, description, departmentCategoryId,
                possibleUserTypes);
    }

    /**
     * Liefert {@link true} zurück, falls der gegebenen {@link UserType} für dieses Department zulässig ist.
     */
    public boolean canBeAssignedToType(UserType userType)
    {
        return possibleUserTypes.contains(userType);
    }

    /**
     * Ärbeitet die Updates aus dem {@link DepartmentChange} in dieses Department ein.
     */
    public void handleChange(DepartmentChange departmentChange)
    {
        name = departmentChange.getUpdatedName(name);
        description = departmentChange.getUpdatedDescription(description);
        departmentCategoryId = departmentChange.getUpdatedDepartmentCategoryId(departmentCategoryId);
        possibleUserTypes = departmentChange.getUpdatedPossibleUserTypes(possibleUserTypes);
    }

    public DepartmentChange generateChange(UpdateDepartmentCommand command)
    {
        DeltaProperty<String> nameChange = DeltaProperty.of(name, command.getName());
        DeltaProperty<String> descriptionChange = DeltaProperty.of(description, command.getDescription());
        DeltaProperty<String> departmentCategoryIdChange = DeltaProperty.of(departmentCategoryId,
                command.getDepartmentCategoryId());
        ListDeltaProperty<UserType> possibleUserTypesChange = ListDeltaProperty.ofNoDuplicatesNoOrder(
                possibleUserTypes, UserType.ofSave(command.getPossibleUserTypes()));

        return new DepartmentChange(nameChange, descriptionChange, departmentCategoryIdChange,
                possibleUserTypesChange);
    }
}
