package org.c4.departmentservice.domain.department.validation;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.reactor.ReactorBooleanHelper;
import org.c4.departmentservice.usecases.department.commands.CreateDepartmentCommand;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Service zur Validierung von {@link Department}-Commands.
 * <br/>
 * Copyright: Copyright (c) 09.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class DepartmentValidator
{
    private final DepartmentRepository         departmentRepository;
    private final DepartmentCategoryRepository departmentCategoryRepository;

    /**
     * Validiert den übergebenen Command und stellt sicher,
     * dass der User über ausreichend Berechtigung zur Verarbeitung dieses Commands verfügt.
     */
    public Mono<Void> validate(CreateDepartmentCommand command, User creator)
    {
        return creator.validateIsDepartmentAdmin()
                .then(DepartmentCommandValidator.validate(command))
                .then(validateNameIsNotTaken(command.getName()))
                .then(validateDepartmentCategoryIdExists(command.getDepartmentCategoryId()));
    }

    /**
     * Validiert den übergebenen Command und stellt sicher,
     * dass der User über ausreichend Berechtigung zur Verarbeitung dieses Commands verfügt.
     */
    public Mono<Department> validate(String departmentId, UpdateDepartmentCommand command, User updater)
    {
        return updater.validateIsDepartmentAdmin()
                .then(DepartmentCommandValidator.validate(command))
                .then(validateNameIsNotTakenExcludingSelf(departmentId, command.getName()))
                .then(validateDepartmentCategoryIdExists(command.getDepartmentCategoryId()))
                .then(validateDepartmentExists(departmentId));
    }

    private Mono<Void> validateNameIsNotTaken(String name)
    {
        return departmentRepository.existsByName(name)
                .map(b -> ReactorBooleanHelper.throwIfTrue(b, ErrorKey.NAME_TAKEN))
                .then();
    }

    private Mono<Void> validateNameIsNotTakenExcludingSelf(String departmentId, String name)
    {
        return departmentRepository.existsByNameExcludingSelf(departmentId, name)
                .map(b -> ReactorBooleanHelper.throwIfTrue(b, ErrorKey.NAME_TAKEN))
                .then();
    }

    private Mono<Void> validateDepartmentCategoryIdExists(String departmentCategoryId)
    {
        return departmentCategoryRepository.existsById(departmentCategoryId)
                .map(b -> ReactorBooleanHelper.throwIfFalse(b, ErrorKey.DEPARTMENT_CATEGORY_NOT_FOUND))
                .then();
    }

    private Mono<Department> validateDepartmentExists(String id)
    {
        return departmentRepository.getById(id)
                .switchIfEmpty(Mono.error(new InvalidOperationException(ErrorKey.DEPARTMENT_NOT_FOUND)));
    }
}
