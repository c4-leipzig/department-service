package org.c4.departmentservice.domain.general.sort;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Arrays;
import java.util.Optional;

/**
 * <br/>
 * Copyright: Copyright (c) 14.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum SortOrder
{
    ASC(1),
    DESC(-1);

    private final int sortKey;

    SortOrder(int sortKey)
    {
        this.sortKey = sortKey;
    }

    @JsonCreator
    public static Optional<SortOrder> of(String key)
    {
        return Arrays.stream(values())
                .filter(sortOrder -> sortOrder.name().equalsIgnoreCase(key))
                .findFirst();
    }

    public int getKey()
    {
        return sortKey;
    }
}
