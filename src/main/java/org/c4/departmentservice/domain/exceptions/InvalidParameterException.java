package org.c4.departmentservice.domain.exceptions;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Exception über die kommuniziert wird, dass ein ungültiger Parameter angegeben wurde.
 * <br/>
 * Copyright: Copyright (c) 14.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
public class InvalidParameterException extends RuntimeException
{
    private final List<String> errors = new ArrayList<>();

    public InvalidParameterException(List<String> errors)
    {
        this.errors.addAll(errors);
    }

    public InvalidParameterException(String error)
    {
        this.errors.add(error);
    }
}
