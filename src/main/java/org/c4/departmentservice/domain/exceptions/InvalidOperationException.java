package org.c4.departmentservice.domain.exceptions;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Allgemeine Exception, die geworfen wird, wenn eine Operation nicht durchgeführt werden kann.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
public class InvalidOperationException extends RuntimeException
{
    private final List<String> errors = new ArrayList<>();

    public InvalidOperationException(String error)
    {
        addError(error);
    }

    public InvalidOperationException(List<String> errors)
    {
        addAllErrors(errors);
    }

    public void addError(String error)
    {
        errors.add(error);
    }

    public void addAllErrors(List<String> errors)
    {
        this.errors.addAll(errors);
    }
}
