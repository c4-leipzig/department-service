package org.c4.departmentservice.domain.exceptions;

/**
 * Exception, über die mitgeteilt wird, dass eine Anfrage kein Ergebnis liefern konnte.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class NotFoundException extends RuntimeException
{
}
