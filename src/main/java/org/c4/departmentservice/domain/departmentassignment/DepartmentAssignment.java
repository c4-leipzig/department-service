package org.c4.departmentservice.domain.departmentassignment;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.infrastructure.IdGenerator;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Entity, welche ein DepartmentAssignment
 * (d.h die Ausübung eines {@link Department}s durch einen {@link User}) abbildet.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Document(collection = "departmentAssignments")
public class DepartmentAssignment extends AbstractEntity
{
    private final List<AssignmentTerm> terms = new ArrayList<>();

    private String description;
    private String departmentId;
    private String userId;

    @JsonCreator
    @PersistenceConstructor
    public DepartmentAssignment(String id, String description, String departmentId, String userId,
            List<AssignmentTerm> terms)
    {
        super(id);
        this.description = description;
        this.departmentId = departmentId;
        this.userId = userId;
        addTerms(terms);
    }

    public DepartmentAssignment(String id, String description, String departmentId, String userId,
            AssignmentTerm term)
    {
        super(id);
        this.description = description;
        this.departmentId = departmentId;
        this.userId = userId;
        addTerm(term);
    }

    public DepartmentAssignment(String id, String description, String departmentId, String userId)
    {
        super(id);
        this.description = description;
        this.departmentId = departmentId;
        this.userId = userId;
    }

    public static DepartmentAssignment create(String description, String departmentId, String userId)
    {
        return new DepartmentAssignment(IdGenerator.generateNew(), description, departmentId, userId);
    }

    /**
     * Fügt eine Liste von Terms zum Assignment hinzu.
     */
    public void addTerms(List<AssignmentTerm> terms)
    {
        this.terms.addAll(terms);
    }

    /**
     * Fügt einen einzelnen Term zum Assignment hinzu.
     */
    public void addTerm(AssignmentTerm term)
    {
        terms.add(term);
    }

    /**
     * Überprüft, ob das übergebene DepartmentAssignment der gleichen Department-User Kombination entspricht.
     * (Nur für Tests)
     */
    public boolean equalsRegardingDepartmentAndUser(DepartmentAssignment other)
    {
        return other != null && Objects.equals(other.departmentId, departmentId) && Objects.equals(
                other.userId, userId);
    }

    public String getDepartmentId()
    {
        return departmentId;
    }

    public List<AssignmentTerm> getTerms()
    {
        return terms;
    }
}
