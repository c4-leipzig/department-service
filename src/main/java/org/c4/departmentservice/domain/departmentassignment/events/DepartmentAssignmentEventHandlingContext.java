package org.c4.departmentservice.domain.departmentassignment.events;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * HandlingContext für die Verarbeitung von {@link DepartmentAssignment}-Events.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class DepartmentAssignmentEventHandlingContext
{
    private final DepartmentAssignmentRepository departmentAssignmentRepository;

    /**
     * Überprüft, ob das angegebene {@link DepartmentAssignment} existiert.
     */
    public Mono<Boolean> departmentAssignmentExists(DepartmentAssignment departmentAssignment)
    {
        return departmentAssignmentRepository.existsById(departmentAssignment.getId());
    }

    /**
     * Speichert das {@link DepartmentAssignment} mit der angegebenen Version.
     */
    public Mono<Void> saveDepartmentAssignment(DepartmentAssignment departmentAssignment)
    {
        return departmentAssignmentRepository.save(departmentAssignment).then();
    }

    public Mono<DepartmentAssignment> getById(String id)
    {
        return departmentAssignmentRepository.getById(id);
    }
}
