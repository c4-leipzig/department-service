package org.c4.departmentservice.domain.departmentassignment.validation;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.reactor.ReactorBooleanHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domain.user.UserRepository;
import org.c4.microservice.framework.domain.user.UserType;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

/**
 * Validator für {@link DepartmentAssignment}-Commands.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class DepartmentAssignmentValidator
{
    private final DepartmentAssignmentCommandValidator commandValidator;
    private final DepartmentAssignmentRepository       departmentAssignmentRepository;
    private final DepartmentRepository                 departmentRepository;
    private final UserRepository                       userRepository;

    public Mono<Void> validate(CreateDepartmentAssignmentCommand command, User creator)
    {
        return creator.validateIsDepartmentAdmin()
                .then(commandValidator.validate(command))
                .then(validateDepartmentAndUser(command.getDepartmentId(), command.getUserId()))
                .then(validateUniqueness(command.getDepartmentId(), command.getUserId()));
    }

    private Mono<Void> validateDepartmentAndUser(String departmentId, String userId)
    {
        return departmentRepository.getById(departmentId)
                .switchIfEmpty(Mono.error(new InvalidOperationException(ErrorKey.DEPARTMENT_NOT_FOUND)))
                .zipWith(userRepository.getTypeForUser(userId)
                        .onErrorMap(e -> new InvalidOperationException(ErrorKey.USER_NOT_FOUND)))
                .flatMap(this::validateUserAllowedForDepartment);
    }

    private Mono<Void> validateUserAllowedForDepartment(Tuple2<Department, UserType> tuple2)
    {
        return Mono.just(tuple2)
                .map(tuple -> tuple.getT1().canBeAssignedToType(tuple.getT2()))
                .map(b -> ReactorBooleanHelper.throwIfFalse(b, ErrorKey.USER_TYPE_INVALID))
                .then();
    }

    private Mono<Void> validateUniqueness(String departmentId, String userId)
    {
        return departmentAssignmentRepository.existsByDepartmentAndUser(departmentId, userId)
                .map(b -> ReactorBooleanHelper.throwIfTrue(b, ErrorKey.DEPARTMENT_ASSIGNMENT_ALREADY_EXISTS))
                .then();
    }
}
