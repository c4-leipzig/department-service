package org.c4.departmentservice.domain.departmentassignment.events;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import reactor.core.publisher.Mono;

/**
 * Event, über das die Erstellung eines {@link DepartmentAssignment} publiziert wird.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor
public class DepartmentAssignmentCreatedEvent extends CreateDomainEvent
{
    private DepartmentAssignment departmentAssignment;

    public DepartmentAssignmentCreatedEvent(DepartmentAssignment departmentAssignment, User creator)
    {
        super(departmentAssignment.getId(), creator);
        this.departmentAssignment = departmentAssignment;
    }

    @Override
    protected Mono<Void> apply(DomainEventHandlingContext handlingContext)
    {
        log.debug("Received DepartmentAssignmentCreatedEvent for aggregate '{}'", this::getAggregateId);
        departmentAssignment.updateVersion(getVersion());
        return handlingContext.saveDepartmentAssignment(departmentAssignment);
    }

    @Override
    protected Mono<Boolean> aggregatePresent(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.departmentAssignmentExists(departmentAssignment);
    }
}
