package org.c4.departmentservice.domain.departmentassignment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Dto über das ein {@link AssignmentTerm} um zusätzliche Informationen angereichert übergeben werden kann.
 * <br/>
 * Copyright: Copyright (c) 14.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentAssignmentTermDto
{
    private String userId;
    private String departmentId;
    private String departmentAssignmentId;
    private AssignmentTerm assignmentTerm;
}
