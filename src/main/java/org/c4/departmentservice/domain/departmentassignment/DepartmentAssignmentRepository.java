package org.c4.departmentservice.domain.departmentassignment;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.c4.microservice.framework.domain.user.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository zur Verwaltung von {@link DepartmentAssignment}s.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DepartmentAssignmentRepository
{
    /**
     * Speichert ein {@link DepartmentAssignment}.
     */
    Mono<DepartmentAssignment> save(DepartmentAssignment departmentAssignment);

    /**
     * Überprüft, ob es ein {@link DepartmentAssignment} mit angegebener Id gibt.
     */
    Mono<Boolean> existsById(String id);

    /**
     * Überprüft, ob es ein {@link DepartmentAssignment} für ein spezifisches {@link Department} und einen
     * spezifischen {@link User} gibt.
     */
    Mono<Boolean> existsByDepartmentAndUser(String departmentId, String userId);

    /**
     * Überprüft, ob es ein {@link AssignmentTerm} für das angegebene {@link Department} gibt, welches
     * zeitlich überlappend zu dem neuen {@link AssignmentTerm} ist.
     */
    Mono<Boolean> existsOverlappingTermForDepartment(String departmentId, AssignmentTerm newTerm);

    /**
     * Liefert ein {@link DepartmentAssignment} anhand seiner Id zurück.
     */
    Mono<DepartmentAssignment> getById(String id);

    /**
     * Liefert eine Liste aller {@link AssignmentTerm} (in Form von {@link DepartmentAssignmentTermDto}s) sortiert nach
     * Datum für ein {@link Department} zurück.
     */
    Flux<DepartmentAssignmentTermDto> getAllAssignmentTermsForDepartmentSortedByDate(String departmentId, SortOrder sort);

    /**
     * Liefert (falls vorhanden) das aktive {@link AssignmentTerm} für das {@link Department} zurück.
     */
    Mono<DepartmentAssignmentTermDto> getActiveAssignmentTermForDepartment(String departmentId);
}
