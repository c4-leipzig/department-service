package org.c4.departmentservice.domain.departmentassignment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.c4.departmentservice.domain.department.Department;
import org.c4.microservice.framework.domain.user.User;

import java.time.LocalDate;

/**
 * Ein Value Object, welches eine Amtszeit eines {@link User}s in einem {@link Department}
 * innerhalb eines {@link DepartmentAssignment}s abbildet.
 * <br/>
 * Copyright: Copyright (c) 05.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AssignmentTerm
{
    private LocalDate start;
    private LocalDate end;
    private String    description;
}
