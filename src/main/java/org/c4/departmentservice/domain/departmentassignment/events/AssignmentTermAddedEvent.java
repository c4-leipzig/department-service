package org.c4.departmentservice.domain.departmentassignment.events;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.UpdateDomainEvent;
import reactor.core.publisher.Mono;

@Log4j2
@NoArgsConstructor
public class AssignmentTermAddedEvent extends UpdateDomainEvent<DepartmentAssignment>
{
    private AssignmentTerm assignmentTerm;

    public AssignmentTermAddedEvent(String aggregateId, Long version, User updater,
            AssignmentTerm assignmentTerm)
    {
        super(aggregateId, version, updater);
        this.assignmentTerm = assignmentTerm;
    }

    @Override
    protected Mono<Void> apply(DomainEventHandlingContext handlingContext, DepartmentAssignment aggregate)
    {
        log.debug("Handling AssignmentTermAddedEvent for aggregate '{}'", this::getAggregateId);
        aggregate.addTerm(assignmentTerm);
        aggregate.updateVersion(getVersion());
        return handlingContext.saveDepartmentAssignment(aggregate);
    }

    @Override
    protected Mono<DepartmentAssignment> getAggregate(
            DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getDepartmentAssignment(getAggregateId());
    }
}
