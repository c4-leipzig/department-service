package org.c4.departmentservice.domain.departmentassignment.validation;

import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.infrastructure.StringHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Statische Validierung der Eingaben zur Erzeugung eines neuen {@link DepartmentAssignment}s.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
public class DepartmentAssignmentCommandValidator
{
    private final int MAX_DESCRIPTION_LENGTH;

    public DepartmentAssignmentCommandValidator(
            @Value("${c4.departmentservice.validation.descriptionLength}") int MAX_DESCRIPTION_LENGTH)
    {
        this.MAX_DESCRIPTION_LENGTH = MAX_DESCRIPTION_LENGTH;
    }

    Mono<Void> validate(CreateDepartmentAssignmentCommand command)
    {
        VoidProcessingResult result = new VoidProcessingResult();

        result.addErrorIf(ErrorKey.DESCRIPTION_LENGTH_INVALID,
                StringHelper.exceedsMaxLength(command.getDescription(), MAX_DESCRIPTION_LENGTH));

        result.addErrorIf(ErrorKey.DEPARTMENT_ID_REQUIRED,
                StringHelper.nullOrEmpty(command.getDepartmentId()));

        result.addErrorIf(ErrorKey.USER_ID_REQUIRED, StringHelper.nullOrEmpty(command.getUserId()));

        return result.monoThrowIfErroneous();
    }
}
