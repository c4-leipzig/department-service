package org.c4.departmentservice.domain.departmentcategory.events;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryChange;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.UpdateDomainEvent;
import reactor.core.publisher.Mono;

/**
 * Event, über das ein Update einer DepartmentCategory abgebildet wird.
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DepartmentCategoryUpdatedEvent extends UpdateDomainEvent<DepartmentCategory>
{
    private DepartmentCategoryChange change;

    public DepartmentCategoryUpdatedEvent(String id, Long version, User updater,
            DepartmentCategoryChange change)
    {
        super(id, version, updater);
        this.change = change;
    }

    @Override
    protected Mono<Void> apply(DomainEventHandlingContext handlingContext, DepartmentCategory aggregate)
    {
        log.debug("Received UpdatedEvent for DepartmentCategory '{}'", this::getAggregateId);
        aggregate = change.apply(aggregate);
        aggregate.updateVersion(getVersion());
        return handlingContext.saveDepartmentCategory(aggregate);
    }

    @Override
    protected Mono<DepartmentCategory> getAggregate(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getDepartmentCategory(getAggregateId());
    }
}
