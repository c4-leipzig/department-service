package org.c4.departmentservice.domain.departmentcategory.events;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import reactor.core.publisher.Mono;

/**
 * Event, über das die Erzeugung einer {@link DepartmentCategory} publiziert wird.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor
public class DepartmentCategoryCreatedEvent extends CreateDomainEvent
{
    private DepartmentCategory departmentCategory;

    public DepartmentCategoryCreatedEvent(DepartmentCategory departmentCategory, User creator)
    {
        super(departmentCategory.getId(), creator);
        this.departmentCategory = departmentCategory;
    }

    @Override
    protected Mono<Void> apply(DomainEventHandlingContext handlingContext)
    {
        log.debug("Received DepartmentCategoryCreatedEvent for aggregate '{}'", this::getAggregateId);
        departmentCategory.updateVersion(getVersion());
        return handlingContext.saveDepartmentCategory(departmentCategory);
    }

    @Override
    protected Mono<Boolean> aggregatePresent(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.departmentCategoryExists(departmentCategory);
    }
}
