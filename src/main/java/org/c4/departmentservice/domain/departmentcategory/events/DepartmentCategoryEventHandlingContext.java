package org.c4.departmentservice.domain.departmentcategory.events;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Ein HandlingContext, der DepartmentCategory-spezifische Funktionalität bereitstellt.
 * <br/>
 * Copyright: Copyright (c) 25.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class DepartmentCategoryEventHandlingContext
{
    private final DepartmentCategoryRepository departmentCategoryRepository;

    /**
     * Persistiert ein {@link DepartmentCategory} in der lokalen Persistenz.
     */
    public Mono<Void> saveDepartmentCategory(DepartmentCategory departmentCategory)
    {
        return departmentCategoryRepository.save(departmentCategory).then();
    }

    /**
     * Überprüft, ob das DepartmentCategory in der lokalen Persistenz verfügbar ist.
     */
    public Mono<Boolean> departmentCategoryExists(DepartmentCategory departmentCategory)
    {
        return departmentCategoryRepository.existsById(departmentCategory.getId());
    }

    /**
     * Liefert die {@link DepartmentCategory} anhand ihrer id zurück.
     */
    public Mono<DepartmentCategory> getById(String id)
    {
        return departmentCategoryRepository.getById(id);
    }
}
