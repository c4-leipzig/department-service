package org.c4.departmentservice.domain.departmentcategory;

import lombok.Getter;
import org.c4.departmentservice.infrastructure.IdGenerator;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Entity zur Repräsentation einer DepartmentCateogry.
 * <br/>
 * Copyright: Copyright (c) 21.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Document(collection = "departmentCategories")
public class DepartmentCategory extends AbstractEntity
{
    @Getter
    private String name;

    public DepartmentCategory(String id, String name)
    {
        super(id);
        this.name = name;
    }

    public static DepartmentCategory create(String name)
    {
        return new DepartmentCategory(IdGenerator.generateNew(), name);
    }

    public void updateName(String updatedName)
    {
        name = updatedName;
    }
}
