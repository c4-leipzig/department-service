package org.c4.departmentservice.domain.departmentcategory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.communication.delta.Delta;
import org.c4.microservice.framework.communication.delta.NonNullableDeltaProperty;

/**
 * Change-Objekt, über das ein Update einer {@link DepartmentCategory} abgebildet wird.
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentCategoryChange implements Delta<DepartmentCategory>
{
    private NonNullableDeltaProperty<String> name;

    public DepartmentCategoryChange(String updatedName)
    {
        name = new NonNullableDeltaProperty<>(updatedName);
    }

    @Override
    public DepartmentCategory apply(DepartmentCategory baseObject)
    {
        baseObject.updateName(name.apply());
        return baseObject;
    }

    @Override
    public boolean isEmpty()
    {
        return name == null;
    }
}
