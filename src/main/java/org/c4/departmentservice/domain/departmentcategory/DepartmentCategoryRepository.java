package org.c4.departmentservice.domain.departmentcategory;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository für den Zugriff auf {@link DepartmentCategory}s.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DepartmentCategoryRepository
{
    /**
     * Speichert eine bestimmte Version einer DepartmentCategory ab.
     */
    Mono<DepartmentCategory> save(DepartmentCategory departmentCategory);

    /**
     * Sucht ein DepartmentCategory anhand deren Id.
     */
    Mono<DepartmentCategory> getById(String id);

    /**
     * Überprüft, ob es eine Category mit dem angegebenen Namen gibt.
     */
    Mono<Boolean> existsByName(String name);

    /**
     * Überprüft, ob es eine Category mit der angegebenen Id gibt.
     */
    Mono<Boolean> existsById(String id);

    /**
     * Gibt eine Liste aller Categories zurück.
     */
    Flux<DepartmentCategory> getAll();
}
