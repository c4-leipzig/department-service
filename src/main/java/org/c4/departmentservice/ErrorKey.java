package org.c4.departmentservice;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Liste möglicher Fehler, die auftreten und an den User geschickt werden können.
 * Um die Werte statisch in Annotations verwenden zu können, wird auf die Verwendung einer Enum verzichtet.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorKey
{
    public static final String USER_NOT_FOUND                                = "USER_NOT_FOUND";
    public static final String UPDATE_IDENTICAL                              = "UPDATE_IDENTICAL";
    public static final String NAME_EMPTY                                    = "NAME_EMPTY";
    public static final String ID_INVALID                                    = "ID_INVALID";
    public static final String NAME_TAKEN                                    = "NAME_TAKEN";
    public static final String DEPARTMENT_CATEGORY_NOT_FOUND                 = "DEPARTMENT_CATEGORY_NOT_FOUND";
    public static final String DEPARTMENT_NOT_FOUND                          = "DEPARTMENT_NOT_FOUND";
    public static final String DEPARTMENT_CATEGORY_REQUIRED                  = "DEPARTMENT_CATEGORY_REQUIRED";
    public static final String DESCRIPTION_LENGTH_INVALID                    = "DESCRIPTION_LENGTH_INVALID";
    public static final String NAME_LENGTH_INVALID                           = "NAME_LENGTH_INVALID";
    public static final String USER_TYPE_INVALID                             = "USER_TYPE_INVALID";
    public static final String DATE_INVALID                                  = "DATE_INVALID";
    public static final String USER_NOT_PERMITTED                            = "USER_NOT_PERMITTED";
    public static final String DATE_REQUIRED                                 = "DATE_REQUIRED";
    public static final String DATE_ORDER_ERROR                              = "DATE_ORDER_ERROR";
    public static final String OVERLAPPING_ASSIGNMENT_TERM_EXISTS            = "OVERLAPPING_ASSIGNMENT_TERM_EXISTS";
    public static final String DATE_OVERLAPPING                              = "DATE_OVERLAPPING";
    public static final String ASSIGNMENT_NOT_FOUND                          = "ASSIGNMENT_NOT_FOUND";
    public static final String FORBIDDEN                                     = "FORBIDDEN";
    public static final String DEPARTMENT_CATEGORY_ID_INVALID                = "DEPARTMENT_CATEGORY_ID_INVALID";
    public static final String USER_TYPE_REQUIRED                            = "USER_TYPE_REQUIRED";
    public static final String DEPARTMENT_ID_REQUIRED                        = "DEPARTMENT_ID_REQUIRED";
    public static final String USER_ID_REQUIRED                              = "USER_ID_REQUIRED";
    public static final String DEPARTMENT_ASSIGNMENT_ALREADY_EXISTS          = "DEPARTMENT_ASSIGNMENT_ALREADY_EXISTS";
    public static final String START_DATE_REQUIRED                           = "START_DATE_REQUIRED";
    public static final String END_DATE_REQUIRED                             = "END_DATE_REQUIRED";
    public static final String START_AFTER_END                               = "START_AFTER_END";
    public static final String DEPARTMENT_ASSIGNMENT_NOT_FOUND               = "DEPARTMENT_ASSIGNMENT_NOT_FOUND";
    public static final String USER_TYPE_DUPLICATES                          = "USER_TYPE_DUPLICATES";
    public static final String SORT_INVALID                                  = "SORT_INVALID";
    public static final String NOT_CATEGORIZED_METHOD_ARGUMENT_TYPE_MISMATCH = "NOT_CATEGORIZED_METHOD_ARGUMENT_TYPE_MISMATCH";

    /**
     * Testet, ob es sich bei dem gegebenen Error um einen NOT_FOUND-Fehler handelt.
     */
    public static boolean isNotFoundError(String error)
    {
        return DEPARTMENT_NOT_FOUND.equals(error) || DEPARTMENT_CATEGORY_NOT_FOUND.equals(error)
                || USER_NOT_FOUND.equals(error) || ASSIGNMENT_NOT_FOUND.equals(error);
    }
}
