package org.c4.departmentservice.domainevent.initialization;

import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.events.DepartmentEventHandlingContext;
import org.c4.departmentservice.domain.departmentassignment.events.DepartmentAssignmentEventHandlingContext;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryEventHandlingContext;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.springframework.stereotype.Service;

/**
 * Implementation zur Verarbeitung von Events, die im Rahmen der initialen Aufdatung anfallen.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@Service
public class InitialDomainEventHandlingContextImpl extends DomainEventHandlingContext
{
    public InitialDomainEventHandlingContextImpl(DomainEventRepository domainEventRepository,
            DepartmentCategoryEventHandlingContext departmentCategoryEventHandlingContext,
            DepartmentEventHandlingContext departmentEventHandlingContext,
            DepartmentAssignmentEventHandlingContext departmentAssignmentEventHandlingContext)
    {
        super(domainEventRepository, departmentCategoryEventHandlingContext, departmentEventHandlingContext,
                departmentAssignmentEventHandlingContext);
    }

    @Override
    public boolean isInitializing()
    {
        return true;
    }
}
