package org.c4.departmentservice.domainevent;

import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.events.DepartmentEventHandlingContext;
import org.c4.departmentservice.domain.departmentassignment.events.DepartmentAssignmentEventHandlingContext;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryEventHandlingContext;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Implementation zur Verarbeitung von Events, die zur Laufzeit eingehen.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@Service
@Qualifier("OngoingDomainEventHandlingContextImpl")
public class OngoingDomainEventHandlingContextImpl extends DomainEventHandlingContext
{
    public OngoingDomainEventHandlingContextImpl(DomainEventRepository domainEventRepository,
            DepartmentCategoryEventHandlingContext departmentCategoryEventHandlingContext,
            DepartmentEventHandlingContext departmentEventHandlingContext,
            DepartmentAssignmentEventHandlingContext departmentAssignmentEventHandlingContext)
    {
        super(domainEventRepository, departmentCategoryEventHandlingContext, departmentEventHandlingContext,
                departmentAssignmentEventHandlingContext);
    }

    @Override
    public boolean isInitializing()
    {
        return false;
    }
}
