package org.c4.departmentservice.domainevent;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.events.DepartmentEventHandlingContext;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.events.DepartmentAssignmentEventHandlingContext;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryEventHandlingContext;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import reactor.core.publisher.Mono;

/**
 * Context zur Verarbeitung eingehender Events.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@AllArgsConstructor
public abstract class DomainEventHandlingContext
{
    protected final DomainEventRepository                    domainEventRepository;
    protected final DepartmentCategoryEventHandlingContext   departmentCategoryEventHandlingContext;
    protected final DepartmentEventHandlingContext           departmentEventHandlingContext;
    protected final DepartmentAssignmentEventHandlingContext departmentAssignmentEventHandlingContext;

    /**
     * Gibt an, ob es sich um den regulären oder den Initialisierungskontext handelt.
     */
    public abstract boolean isInitializing();

    /**
     * Lädt das Aggregat neu vom Eventstore.
     */
    public void reloadAggregate(String aggregateId, Long minVersion)
    {
        domainEventRepository.getEventStream(aggregateId, minVersion)
                .forEach(domainEvent -> domainEvent.handleEvent(this).block());
    }

    public Mono<Void> saveDepartmentCategory(DepartmentCategory departmentCategory)
    {
        return departmentCategoryEventHandlingContext.saveDepartmentCategory(departmentCategory);
    }

    public Mono<Boolean> departmentCategoryExists(DepartmentCategory departmentCategory)
    {
        return departmentCategoryEventHandlingContext.departmentCategoryExists(departmentCategory);
    }

    public Mono<DepartmentCategory> getDepartmentCategory(String id)
    {
        return departmentCategoryEventHandlingContext.getById(id);
    }

    public Mono<Void> saveDepartment(Department department)
    {
        return departmentEventHandlingContext.saveDepartment(department);
    }

    public Mono<Boolean> departmentExists(Department department)
    {
        return departmentEventHandlingContext.departmentExists(department);
    }

    public Mono<Void> saveDepartmentAssignment(DepartmentAssignment departmentAssignment)
    {
        return departmentAssignmentEventHandlingContext.saveDepartmentAssignment(departmentAssignment);
    }

    public Mono<Boolean> departmentAssignmentExists(DepartmentAssignment departmentAssignment)
    {
        return departmentAssignmentEventHandlingContext.departmentAssignmentExists(departmentAssignment);
    }

    public Mono<DepartmentAssignment> getDepartmentAssignment(String id)
    {
        return departmentAssignmentEventHandlingContext.getById(id);
    }

    public Mono<Department> getDepartment(String id)
    {
        return departmentEventHandlingContext.getDepartment(id);
    }
}
