package org.c4.departmentservice.adapter.rest.departmentcategory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;

/**
 * RestDto für das Anlegen einer {@link DepartmentCategory}.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateDepartmentCategoryRestDto
{
    private String name;
}
