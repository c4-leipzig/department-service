package org.c4.departmentservice.adapter.rest.departmentassignment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;

/**
 * RestDto über den ein {@link CreateDepartmentAssignmentCommand} empfangen wird.
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateDepartmentAssignmentRestDto
{
    private String description;
    private String departmentId;
    private String userId;

    public CreateDepartmentAssignmentCommand toCommand()
    {
        return new CreateDepartmentAssignmentCommand(description, departmentId, userId);
    }
}
