package org.c4.departmentservice.adapter.rest.departmentcategory;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.usecases.departmentcategory.CreateDepartmentCategoryUseCase;
import org.c4.departmentservice.usecases.departmentcategory.GetAllDepartmentCategoriesUseCase;
import org.c4.departmentservice.usecases.departmentcategory.UpdateDepartmentCategoryUseCase;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/**
 * Rest-Schnittstelle zum Triggern von DepartmentCategory-UseCases.
 * <br/>
 * Copyright: Copyright (c) 21.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@RestController
@AllArgsConstructor
public class DepartmentCategoryRestController
{
    private final CreateDepartmentCategoryUseCase   createDepartmentCategoryUseCase;
    private final UpdateDepartmentCategoryUseCase   updateDepartmentCategoryUseCase;
    private final GetAllDepartmentCategoriesUseCase getAllDepartmentCategoriesUseCase;

    @PostMapping("/categories")
    @PreAuthorize("hasRole('DEPARTMENT_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<IdDto> createDepartmentCategory(@RequestBody CreateDepartmentCategoryRestDto restDto,
            Authentication authentication)
    {
        return createDepartmentCategoryUseCase.createDepartmentCategory(restDto.getName(),
                new User(authentication));
    }

    @PutMapping("/categories/{id}")
    @PreAuthorize("hasRole('DEPARTMENT_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> updateDepartmentCategory(@Valid IdDto id,
            @RequestBody UpdateDepartmentCategoryRestDto restDto, Authentication authentication)
    {
        log.debug("Received request to update DepartmentCategory '{}'", id::getId);
        return updateDepartmentCategoryUseCase.updateDepartmentCategory(id.getId(), restDto.getName(),
                new User(authentication));
    }

    @GetMapping("/categories")
    @PreAuthorize("hasAnyRole('CANDIDATE', 'MEMBER', 'HONORARY_MEMBER', 'DEPARTMENT_ADMIN')")
    public Flux<DepartmentCategory> getAllDepartmentCategories(Authentication authentication)
    {
        log.debug("Received request to get all DepartmentCategories");
        return getAllDepartmentCategoriesUseCase.getAllDepartmentCategories(new User(authentication));
    }

}
