package org.c4.departmentservice.adapter.rest.department;

import lombok.Getter;
import lombok.Setter;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.usecases.department.commands.CreateDepartmentCommand;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;

import java.util.List;

/**
 * Dto über das via Rest der Command zum Erstellen oder Updaten eines {@link Department}s empfangen wird.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class DepartmentRestDto
{
    private String       name;
    private String       description;
    private String       departmentCategoryId;
    private List<String> possibleUserTypes;

    public CreateDepartmentCommand toCreateDepartmentCommand()
    {
        return new CreateDepartmentCommand(name, description, departmentCategoryId, possibleUserTypes);
    }

    public UpdateDepartmentCommand toUpdateDepartmentCommand()
    {
        return new UpdateDepartmentCommand(name, description, departmentCategoryId, possibleUserTypes);
    }
}
