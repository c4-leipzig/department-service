package org.c4.departmentservice.adapter.rest.department;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentWithActiveAssignmentDto;
import org.c4.departmentservice.usecases.department.*;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * RestController als Adapter für {@link Department}-UseCases.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@RestController
@AllArgsConstructor
public class DepartmentRestController
{
    private final CreateDepartmentUseCase  createUseCase;
    private final UpdateDepartmentUseCase  updateUseCase;
    private final GetAllDepartmentsUseCase getAllUseCase;
    private final GetAllDepartmentsWithActiveAssignmentUseCase getAllWithActiveAssignmentUseCase;
    private final GetDepartmentByIdUseCase getByIdUseCase;

    @PostMapping("/departments")
    @PreAuthorize("hasRole('DEPARTMENT_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<IdDto> createDepartment(@RequestBody DepartmentRestDto restDto, Authentication authentication)
    {
        log.debug("Received rest request to add department");
        return createUseCase.createDepartment(restDto.toCreateDepartmentCommand(), new User(authentication));
    }

    @PutMapping("/departments/department/{id}")
    @PreAuthorize("hasRole('DEPARTMENT_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> updateDepartment(IdDto id, @RequestBody DepartmentRestDto restDto,
            Authentication authentication)
    {
        log.debug("Received rest request to update department '{}'", id::getId);
        return updateUseCase.updateDepartment(id.getId(), restDto.toUpdateDepartmentCommand(),
                new User(authentication));
    }

    @GetMapping("/departments")
    @PreAuthorize("hasAnyRole('CANDIDATE', 'MEMBER', 'HONORARY_MEMBER', 'DEPARTMENT_ADMIN')")
    public Flux<Department> getAllDepartments(Authentication authentication)
    {
        log.debug("Received rest request to get all departments");
        return getAllUseCase.getAllDepartments(new User(authentication));
    }

    @GetMapping("/departments/department/{id}")
    @PreAuthorize("hasAnyRole('CANDIDATE', 'MEMBER', 'HONORARY_MEMBER', 'DEPARTMENT_ADMIN')")
    public Mono<Department> getDepartment(IdDto id, Authentication authentication)
    {
        log.debug("Received request to get Department '{}'", id::getId);
        return getByIdUseCase.getDepartmentById(id.getId(), new User(authentication));
    }

    @GetMapping("/departments/withActiveAssignment")
    @PreAuthorize("hasAnyRole('CANDIDATE', 'MEMBER', 'HONORARY_MEMBER', 'DEPARTMENT_ADMIN')")
    public Flux<DepartmentWithActiveAssignmentDto> getAllDepartmentsWithActiveAssignment(Authentication authentication)
    {
        log.debug("Received request to get all Departments with active assignment.");
        return getAllWithActiveAssignmentUseCase.getAllDepartmentsWithActiveAssignment(new User(authentication));
    }
}
