package org.c4.departmentservice.adapter.rest.departmentassignment;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentTermDto;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.c4.departmentservice.usecases.department.GetAssignmentTermsForDepartmentUseCase;
import org.c4.departmentservice.usecases.departmentassignment.AddAssignmentTermUseCase;
import org.c4.departmentservice.usecases.departmentassignment.CreateDepartmentAssignmentUseCase;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/**
 * RestController als Adapter für {@link DepartmentAssignment}-UseCases.
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@RestController
@AllArgsConstructor
public class DepartmentAssignmentRestController
{
    private final CreateDepartmentAssignmentUseCase      createUseCase;
    private final AddAssignmentTermUseCase               addAssignmentTermUseCase;
    private final GetAssignmentTermsForDepartmentUseCase getAssignmentTermsForDepartmentUseCase;

    @PostMapping("/assignments")
    @PreAuthorize("hasRole('DEPARTMENT_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<IdDto> createDepartmentAssignment(@RequestBody CreateDepartmentAssignmentRestDto restDto,
            Authentication authentication)
    {
        return createUseCase.createDepartmentAssignment(restDto.toCommand(), new User(authentication));
    }

    @PutMapping("/assignments/{id}/terms")
    @PreAuthorize("hasRole('DEPARTMENT_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> addAssignmentTerm(@Valid IdDto id, @RequestBody AddAssignmentTermRestDto restDto,
            Authentication authentication)
    {
        return addAssignmentTermUseCase.addAssignmentTerm(restDto.toCommand(id.getId()),
                new User(authentication));
    }

    @GetMapping("/assignments/department/{id}")
    @PreAuthorize("hasAnyRole('CANDIDATE', 'MEMBER', 'HONORARY_MEMBER', 'DEPARTMENT_ADMIN')")
    public Flux<DepartmentAssignmentTermDto> getAssignmentTermsForDepartment(@Valid IdDto id,
            @RequestParam(value = "sort", defaultValue = "desc") SortOrder sortOrder,
            Authentication authentication)
    {
        return getAssignmentTermsForDepartmentUseCase.getTermsForDepartment(id.getId(), sortOrder,
                new User(authentication));
    }
}
