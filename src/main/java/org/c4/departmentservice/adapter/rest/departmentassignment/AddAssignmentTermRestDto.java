package org.c4.departmentservice.adapter.rest.departmentassignment;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.usecases.departmentassignment.commands.AddAssignmentTermCommand;
import org.c4.departmentservice.usecases.departmentassignment.AddAssignmentTermUseCase;

import java.time.LocalDate;

/**
 * RestDto, über den Daten für das Antriggern des {@link AddAssignmentTermUseCase} übertragen werden.
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@AllArgsConstructor
public class AddAssignmentTermRestDto
{
    private final String    description;
    private final LocalDate start;
    private final LocalDate end;

    public AddAssignmentTermCommand toCommand(String departmentAssignmentId)
    {
        return new AddAssignmentTermCommand(departmentAssignmentId, start, end, description);
    }
}
