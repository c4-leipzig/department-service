package org.c4.departmentservice.infrastructure;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * HelperClass zur Verarbeitung von Listen.
 * <br/>
 * Copyright: Copyright (c) 10.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ListHelper
{
    /**
     * Überprüft, ob die übergebene Liste Duplikate enthält. Zur Prüfung auf Gleichheit wird {@link Object#equals(Object)}
     * verwendet; T muss daher eine adäquate {@link #equals} Methode bereitstellen.
     */
    public static <T> boolean containsDuplicates(List<T> list)
    {
        return list != null && list.stream().distinct().count() != list.size();
    }
}
