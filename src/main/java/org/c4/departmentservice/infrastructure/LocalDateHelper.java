package org.c4.departmentservice.infrastructure;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * HelperClass für die Verarbeitung von {@link LocalDate}s.
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LocalDateHelper
{
    /**
     * Überprüft, ob zwei {@link LocalDate}s in zeitlich korrekter Reihenfolge (d.h. nacheinander) sind.
     * Die Prüfung ist in jedem Fall erfolgreich, falls eines der beiden Daten {@link null} ist.
     */
    public static boolean inOrder(LocalDate first, LocalDate second)
    {
        return first == null || second == null || first.isBefore(second);
    }
}
