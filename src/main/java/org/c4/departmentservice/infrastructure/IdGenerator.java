package org.c4.departmentservice.infrastructure;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IdGenerator
{
    public static String generateNew()
    {
        return new ObjectId().toHexString();
    }
}
