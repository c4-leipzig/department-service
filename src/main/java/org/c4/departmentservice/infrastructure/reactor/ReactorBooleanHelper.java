package org.c4.departmentservice.infrastructure.reactor;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;

import java.util.Collections;
import java.util.List;

/**
 * Helper-Class zur vereinfachten Verarbeitung von Booleans in Streams.
 * <br/>
 * Copyright: Copyright (c) 21.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReactorBooleanHelper
{
    public static boolean throwIfFalse(boolean b, String error)
    {
        if (!b)
        {
            throw new InvalidOperationException(Collections.singletonList(error));
        }
        return true;
    }

    public static boolean throwIfFalse(boolean b, List<String> errors)
    {
        if (!b)
        {
            throw new InvalidOperationException(errors);
        }
        return true;
    }

    public static boolean throwIfTrue(boolean b, String error)
    {
        if (b)
        {
            throw new InvalidOperationException(Collections.singletonList(error));
        }
        return true;
    }

    public static boolean throwIfTrue(boolean b, List<String> errors)
    {
        if (b)
        {
            throw new InvalidOperationException(errors);
        }
        return false;
    }
}
