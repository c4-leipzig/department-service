package org.c4.departmentservice.infrastructure.converter;

import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.exceptions.InvalidParameterException;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Converter zur automatischen Umwandlung von Strings in {@link SortOrder}-Elemente.
 * <br/>
 * Copyright: Copyright (c) 14.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Component
public class SortOrderConverter implements Converter<String, SortOrder>
{
    @Override
    public SortOrder convert(String sortString)
    {
        return SortOrder.of(sortString)
                .orElseThrow(() -> new InvalidParameterException(ErrorKey.SORT_INVALID));
    }
}
