package org.c4.departmentservice.infrastructure.repositories.departmentcategory;

import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

/**
 * SpringData Repository für den Zugriff auf DepartmentCategories.
 * <br/>
 * Copyright: Copyright (c) 21.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DepartmentCategoryDataRepository
        extends ReactiveMongoRepository<DepartmentCategory, String>
{
    /**
     * Üerprüft, ob es eine {@link DepartmentCategory} mit angegebenem Namen gibt.
     */
    Mono<Boolean> existsByName(String name);
}
