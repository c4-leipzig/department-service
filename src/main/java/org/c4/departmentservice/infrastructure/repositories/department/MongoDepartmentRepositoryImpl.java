package org.c4.departmentservice.infrastructure.repositories.department;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Implementierung des {@link DepartmentRepository}s, welche auf MongoDB als Persistenz zurückgreift.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class MongoDepartmentRepositoryImpl implements DepartmentRepository
{
    private final DepartmentDataRepository departmentDataRepository;

    @Override
    public Mono<Boolean> existsById(String id)
    {
        return departmentDataRepository.existsById(id);
    }

    @Override
    public Mono<Boolean> existsByName(String name)
    {
        return departmentDataRepository.existsByName(name);
    }

    @Override
    public Mono<Boolean> existsByNameExcludingSelf(String id, String name)
    {
        return departmentDataRepository.existsByIdIsNotAndName(id, name);
    }

    @Override
    public Mono<Department> save(Department department)
    {
        return departmentDataRepository.save(department);
    }

    @Override
    public Flux<Department> getAll()
    {
        return departmentDataRepository.findAll();
    }

    @Override
    public Mono<Department> getById(String id)
    {
        return departmentDataRepository.findById(id);
    }
}
