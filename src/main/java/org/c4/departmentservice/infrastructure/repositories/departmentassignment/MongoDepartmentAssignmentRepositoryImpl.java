package org.c4.departmentservice.infrastructure.repositories.departmentassignment;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentTermDto;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

/**
 * Mongo-Implementierung des {@link DepartmentAssignmentRepository}s.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
@AllArgsConstructor
public class MongoDepartmentAssignmentRepositoryImpl implements DepartmentAssignmentRepository
{
    private final DepartmentAssignmentDataRepository dataRepository;

    @Override
    public Mono<DepartmentAssignment> save(DepartmentAssignment departmentAssignment)
    {
        return dataRepository.save(departmentAssignment);
    }

    @Override
    public Mono<Boolean> existsById(String id)
    {
        return dataRepository.existsById(id);
    }

    @Override
    public Mono<Boolean> existsByDepartmentAndUser(String departmentId, String userId)
    {
        return dataRepository.existsByDepartmentIdAndUserId(departmentId, userId);
    }

    @Override
    public Mono<Boolean> existsOverlappingTermForDepartment(String departmentId, AssignmentTerm newTerm)
    {
        return dataRepository.existsOverlappingAssignmentTermForDepartment(departmentId, newTerm.getStart(),
                newTerm.getEnd());
    }

    @Override
    public Mono<DepartmentAssignment> getById(String id)
    {
        return dataRepository.findById(id);
    }

    @Override
    public Flux<DepartmentAssignmentTermDto> getAllAssignmentTermsForDepartmentSortedByDate(String departmentId, SortOrder sort)
    {
        return dataRepository.getAllAssignmentTermsForDepartment(departmentId, sort.getKey());
    }

    @Override
    public Mono<DepartmentAssignmentTermDto> getActiveAssignmentTermForDepartment(String departmentId)
    {
        return dataRepository.getActiveAssignmentTermForDepartmentAtDate(departmentId, LocalDate.now());
    }
}
