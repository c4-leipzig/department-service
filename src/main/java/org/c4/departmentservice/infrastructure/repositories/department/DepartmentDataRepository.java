package org.c4.departmentservice.infrastructure.repositories.department;

import org.c4.departmentservice.domain.department.Department;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

/**
 * Spring-Mongo-Repository für die Persistierung von {@link Department}s.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
public interface DepartmentDataRepository extends ReactiveMongoRepository<Department, String>
{
    /**
     * Überprüft, ob es ein Department mit dem angegebenen Namen gibt.
     */
    Mono<Boolean> existsByName(String name);

    /**
     * Überprüft, ob es ein *anderes* {@link Department} mit dem angegebenen Namen gibt.
     */
    Mono<Boolean> existsByIdIsNotAndName(String id, String name);
}
