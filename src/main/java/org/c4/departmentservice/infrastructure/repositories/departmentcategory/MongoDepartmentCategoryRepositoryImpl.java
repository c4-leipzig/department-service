package org.c4.departmentservice.infrastructure.repositories.departmentcategory;

import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository-Implementierung für das Speichern/Lesen von {@link DepartmentCategory}s in Mongo.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Service
public class MongoDepartmentCategoryRepositoryImpl implements DepartmentCategoryRepository
{
    private final DepartmentCategoryDataRepository dataRepository;

    public MongoDepartmentCategoryRepositoryImpl(DepartmentCategoryDataRepository dataRepository)
    {
        this.dataRepository = dataRepository;
    }

    @Override
    public Mono<DepartmentCategory> save(DepartmentCategory departmentCategory)
    {
        return dataRepository.save(departmentCategory);
    }

    @Override
    public Mono<DepartmentCategory> getById(String id)
    {
        return dataRepository.findById(id);
    }

    @Override
    public Mono<Boolean> existsByName(String name)
    {
        return dataRepository.existsByName(name);
    }

    @Override
    public Mono<Boolean> existsById(String id)
    {
        return dataRepository.existsById(id);
    }

    @Override
    public Flux<DepartmentCategory> getAll()
    {
        return dataRepository.findAll();
    }
}
