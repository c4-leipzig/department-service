package org.c4.departmentservice.infrastructure.repositories.departmentassignment;

import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentTermDto;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

/**
 * Spring-Mongo-Repository für {@link DepartmentAssignment}s.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DepartmentAssignmentDataRepository
        extends ReactiveMongoRepository<DepartmentAssignment, String>
{
    /**
     * Überprüft, ob es ein {@link DepartmentAssignment} für ein spezifisches {@link Department} und einen
     * spezifischen {@link User} gibt.
     */
    Mono<Boolean> existsByDepartmentIdAndUserId(String departmentId, String userId);

    /**
     * Überprüft, ob es ein {@link DepartmentAssignment} mit einem {@link AssignmentTerm} gibt, dessen
     * Start-End-Intervall das eines anderen {@link AssignmentTerm}s überlappt.
     */
    @ExistsQuery("{'departmentId': ?0, 'terms': {$elemMatch: {'start': {$lte: ?2}, 'end': {$gte: ?1}}}}")
    Mono<Boolean> existsOverlappingAssignmentTermForDepartment(String departmentId, LocalDate start,
            LocalDate end);

    /**
     * Gibt eine Liste aller {@link AssignmentTerm}s zurück, die {@link DepartmentAssignment}s eines bestimmten {@link Department}s zugeordnet sind.
     * Die Terms werden um zusätzliche Informationen aus dem Assignment angereichert.
     */
    @Aggregation({ "{$match: {'departmentId': '?0'}}", "{$unwind: {path: '$terms'}}",
            "{$sort: {'terms.start': ?1}}",
            "{$project: {'departmentAssignmentId': '$_id', 'assignmentTerm': '$terms', 'departmentId': '$departmentId', 'userId': '$userId', '_id': false}}" })
    Flux<DepartmentAssignmentTermDto> getAllAssignmentTermsForDepartment(String departmentId, int sortOrder);

    /**
     * Gibt den einzigen - zum übergebenen Zeitpunkt - aktiven {@link AssignmentTerm} für ein {@link Department} zurück.
     */
    @Aggregation({ "{$match: {'departmentId': '?0'}}", "{$unwind: {path: '$terms'}}",
            "{$match: {'terms.start': {$lte: ?1}, 'terms.end': {$gte: ?1}}}",
            "{$project: {'departmentAssignmentId': '$_id', 'assignmentTerm': '$terms', 'departmentId': '$departmentId', 'userId': '$userId', '_id': false}}" })
    Mono<DepartmentAssignmentTermDto> getActiveAssignmentTermForDepartmentAtDate(String departmentId, LocalDate currentDate);
}
