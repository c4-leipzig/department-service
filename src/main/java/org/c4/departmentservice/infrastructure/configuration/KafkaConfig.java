package org.c4.departmentservice.infrastructure.configuration;

import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domainevent.DomainEventSink;
import org.c4.microservice.framework.domainevent.KafkaAsyncDomainEventSinkImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class KafkaConfig
{
    // Die asynchrone DomainEventSink darf erst erstellt werden, wenn die Initialisierung abgeschlossen ist.
    @Bean
    @DependsOn(value = { "initializationRunner" })
    public DomainEventSink kafkaAsyncDomainEventSinkImpl(
            DomainEventHandlingContext ongoingDomainEventHandlingContextImpl)
    {
        return new KafkaAsyncDomainEventSinkImpl(ongoingDomainEventHandlingContextImpl);
    }
}
