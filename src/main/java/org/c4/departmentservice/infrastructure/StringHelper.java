package org.c4.departmentservice.infrastructure;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Helper-Funktionen für das Verarbeiten von String.
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringHelper
{
    /**
     * Liefert {@link true} zurück, falls der übergebene String leer oder {@link null} ist.
     */
    public static boolean nullOrEmpty(String test)
    {
        return test == null || test.isBlank();
    }

    /**
     * Überprüft, ob der übergebene String eine erlaubte Maximallänge überschreitet.
     */
    public static boolean exceedsMaxLength(String test, int maxLength)
    {
        return test != null && test.length() > maxLength;
    }
}
