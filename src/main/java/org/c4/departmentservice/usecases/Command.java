package org.c4.departmentservice.usecases;

import reactor.core.publisher.Mono;

/**
 * Abstraktes Command-Objekt. Dieses kann sich selbst validieren und wirft im Fehlerfall eine entsprechende Exception.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface Command<AGG>
{
    /**
     * Validiert das implementierende Command-Objekt. Im Fehlerfall wird eine Exception mit den entsprechenden
     * Fehlercodes geworfen.
     */
    Mono<Void> validate();

    /**
     * Erzeugt aus dem Command ein entsprechendes Aggregate.
     * Es wird vorausgesetzt, dass der Command valide ist!
     */
    AGG toAggregate();
}
