package org.c4.departmentservice.usecases.departmentassignment;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.events.DepartmentAssignmentCreatedEvent;
import org.c4.departmentservice.domain.departmentassignment.validation.DepartmentAssignmentValidator;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.departmentservice.usecases.departmentassignment.commands.CreateDepartmentAssignmentCommand;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import reactor.core.publisher.Mono;

/**
 * UseCase über den ein {@link DepartmentAssignment} angelegt wird.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class CreateDepartmentAssignmentUseCase
{
    private final EventPublisher                eventPublisher;
    private final DepartmentAssignmentValidator departmentAssignmentValidator;

    /**
     * Validiert die übergebenen Daten und legt im Erfolgsfall das {@link DepartmentAssignment} im Namen des {@link User} an.
     */
    public Mono<IdDto> createDepartmentAssignment(CreateDepartmentAssignmentCommand command, User creator)
    {
        return departmentAssignmentValidator.validate(command, creator)
                .then(Mono.defer(() -> publishCreatedEvent(command, creator)));
    }

    private Mono<IdDto> publishCreatedEvent(CreateDepartmentAssignmentCommand command, User creator)
    {
        log.debug("Sending DepartmentAssignmentCreatedEvent");
        DepartmentAssignment departmentAssignment = command.toDepartmentAssignment();
        eventPublisher.send(new DepartmentAssignmentCreatedEvent(departmentAssignment, creator));

        return Mono.just(departmentAssignment.getIdDto());
    }
}
