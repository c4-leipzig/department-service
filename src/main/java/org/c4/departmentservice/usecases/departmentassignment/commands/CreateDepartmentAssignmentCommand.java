package org.c4.departmentservice.usecases.departmentassignment.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.usecases.departmentassignment.CreateDepartmentAssignmentUseCase;

/**
 * Command, über den der {@link CreateDepartmentAssignmentUseCase} angestoßen wird.
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@AllArgsConstructor
public class CreateDepartmentAssignmentCommand
{
    private final String description;
    private final String departmentId;
    private final String userId;

    public DepartmentAssignment toDepartmentAssignment()
    {
        return DepartmentAssignment.create(description, departmentId, userId);
    }
}
