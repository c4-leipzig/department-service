package org.c4.departmentservice.usecases.departmentassignment.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.infrastructure.LocalDateHelper;
import org.c4.departmentservice.infrastructure.StringHelper;
import org.c4.departmentservice.usecases.Command;
import org.c4.departmentservice.usecases.departmentassignment.AddAssignmentTermUseCase;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

/**
 * Command zur Ausführung des {@link AddAssignmentTermUseCase}s.
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@AllArgsConstructor
public class AddAssignmentTermCommand implements Command<AssignmentTerm>
{
    private final String    departmentAssignmentId;
    private final LocalDate start;
    private final LocalDate end;
    private final String    description;

    @Override
    public Mono<Void> validate()
    {
        VoidProcessingResult result = new VoidProcessingResult();

        result.addErrorIf(ErrorKey.DESCRIPTION_LENGTH_INVALID,
                StringHelper.exceedsMaxLength(description, 4000));

        result.addErrorIf(ErrorKey.START_DATE_REQUIRED, start == null);
        result.addErrorIf(ErrorKey.END_DATE_REQUIRED, end == null);
        result.addErrorIf(ErrorKey.START_AFTER_END, !LocalDateHelper.inOrder(start, end));

        return result.monoThrowIfErroneous();
    }

    @Override
    public AssignmentTerm toAggregate()
    {
        return new AssignmentTerm(start, end, description);
    }
}
