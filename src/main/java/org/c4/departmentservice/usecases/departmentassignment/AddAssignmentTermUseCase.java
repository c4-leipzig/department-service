package org.c4.departmentservice.usecases.departmentassignment;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignment;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.domain.departmentassignment.events.AssignmentTermAddedEvent;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.departmentservice.infrastructure.reactor.ReactorBooleanHelper;
import org.c4.departmentservice.usecases.departmentassignment.commands.AddAssignmentTermCommand;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import reactor.core.publisher.Mono;

/**
 * UseCase für das Hinzufügen eines {@link AssignmentTerm}s zu einem {@link DepartmentAssignment}.
 * <br/>
 * Copyright: Copyright (c) 07.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class AddAssignmentTermUseCase
{
    private final EventPublisher                 eventPublisher;
    private final DepartmentAssignmentRepository departmentAssignmentRepository;

    public Mono<Void> addAssignmentTerm(AddAssignmentTermCommand command, User adder)
    {
        log.info("AddAssignmentTerm started");
        return validate(command, adder).doOnNext(
                departmentAssignment -> publishAssignmentTermAddedEvent(departmentAssignment, command, adder))
                .then();
    }

    private Mono<DepartmentAssignment> validate(AddAssignmentTermCommand command, User adder)
    {
        return validateAdder(adder).then(command.validate())
                .then(validateAssignmentExists(command))
                .flatMap(departmentAssignment -> validateNoOverlappingTermExistsForDepartment(
                        departmentAssignment, command));
    }

    private Mono<Void> validateAdder(User adder)
    {
        if (!adder.isDepartmentAdmin())
        {
            return Mono.error(new NotAuthorizedException());
        }

        return Mono.empty();
    }

    private Mono<DepartmentAssignment> validateAssignmentExists(AddAssignmentTermCommand command)
    {
        return departmentAssignmentRepository.getById(command.getDepartmentAssignmentId())
                .switchIfEmpty(
                        Mono.error(new InvalidOperationException(ErrorKey.DEPARTMENT_ASSIGNMENT_NOT_FOUND)));
    }

    private Mono<DepartmentAssignment> validateNoOverlappingTermExistsForDepartment(
            DepartmentAssignment departmentAssignment, AddAssignmentTermCommand command)
    {
        return departmentAssignmentRepository.existsOverlappingTermForDepartment(
                departmentAssignment.getDepartmentId(), command.toAggregate())
                .map(b -> ReactorBooleanHelper.throwIfTrue(b, ErrorKey.OVERLAPPING_ASSIGNMENT_TERM_EXISTS))
                .thenReturn(departmentAssignment);
    }

    private void publishAssignmentTermAddedEvent(DepartmentAssignment departmentAssignment,
            AddAssignmentTermCommand command, User adder)
    {
        log.debug("Sending AssignmentTermAddedEvent");
        AssignmentTermAddedEvent addedEvent = new AssignmentTermAddedEvent(departmentAssignment.getId(),
                departmentAssignment.getVersion(), adder, command.toAggregate());
        eventPublisher.send(addedEvent);
    }
}
