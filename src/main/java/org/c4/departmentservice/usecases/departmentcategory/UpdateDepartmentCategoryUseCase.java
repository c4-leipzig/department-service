package org.c4.departmentservice.usecases.departmentcategory;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryChange;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryUpdatedEvent;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.departmentservice.infrastructure.reactor.ReactorBooleanHelper;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import reactor.core.publisher.Mono;

/**
 * UseCase bildet das Umbenennen einer DepartmentCategory ab.
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class UpdateDepartmentCategoryUseCase
{
    private final DepartmentCategoryRepository departmentCategoryRepository;
    private final EventPublisher               eventPublisher;

    public Mono<Void> updateDepartmentCategory(String id, String updatedName, User updater)
    {
        return new UpdateDepartmentCategoryUseCaseExecutor(departmentCategoryRepository, eventPublisher, id,
                updatedName, updater).createDepartment();
    }

    @AllArgsConstructor
    private static class UpdateDepartmentCategoryUseCaseExecutor
    {
        private final DepartmentCategoryRepository departmentCategoryRepository;
        private final EventPublisher               eventPublisher;

        private final String id;
        private final String updatedName;
        private final User   updater;

        private Mono<Void> createDepartment()
        {
            log.info("UpdateDepartmentCategory started");
            if (!updater.isDepartmentAdmin())
            {
                return Mono.error(new NotAuthorizedException());
            }

            return departmentCategoryRepository.existsByName(updatedName)
                    .map(b -> ReactorBooleanHelper.throwIfTrue(b, ErrorKey.NAME_TAKEN))
                    .flatMap(b -> getDepartmentCategory())
                    .map(this::buildUpdatedEvent)
                    .doOnNext(
                            updatedEvent -> log.debug("Updating DepartmentCategory '{}' to new name '{}'", id,
                                    updatedName))
                    .doOnNext(eventPublisher::send)
                    .then();
        }

        private Mono<DepartmentCategory> getDepartmentCategory()
        {
            return departmentCategoryRepository.getById(id)
                    .switchIfEmpty(Mono.error(
                            new InvalidOperationException(ErrorKey.DEPARTMENT_CATEGORY_NOT_FOUND)));
        }

        private DepartmentCategoryUpdatedEvent buildUpdatedEvent(DepartmentCategory departmentCategory)
        {
            return new DepartmentCategoryUpdatedEvent(departmentCategory.getId(),
                    departmentCategory.getVersion(), updater, new DepartmentCategoryChange(updatedName));
        }
    }
}
