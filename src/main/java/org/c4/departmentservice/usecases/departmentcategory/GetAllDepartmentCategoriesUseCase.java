package org.c4.departmentservice.usecases.departmentcategory;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.c4.microservice.framework.domain.user.User;
import reactor.core.publisher.Flux;

/**
 * Bildet den UseCase ab, dass ein User eine Liste der DepartmentCategories erhaten möchte.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class GetAllDepartmentCategoriesUseCase
{
    private final DepartmentCategoryRepository repository;

    /**
     * Gibt eine Liste aller {@link DepartmentCategory}s zurück.
     */
    public Flux<DepartmentCategory> getAllDepartmentCategories(User requestingUser)
    {
        log.info("GetAllDepartmentCategories started");
        if (!requestingUser.isAnyMember() && !requestingUser.isDepartmentAdmin())
        {
            return Flux.error(new NotAuthorizedException());
        }

        return repository.getAll();
    }
}
