package org.c4.departmentservice.usecases.departmentcategory;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategory;
import org.c4.departmentservice.domain.departmentcategory.DepartmentCategoryRepository;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryCreatedEvent;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.departmentservice.infrastructure.reactor.ReactorBooleanHelper;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import reactor.core.publisher.Mono;

/**
 * ApplicationService, der den UseCase des Kategorie-Anlegens abbildet.
 * <br/>
 * Copyright: Copyright (c) 21.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class CreateDepartmentCategoryUseCase
{
    private final DepartmentCategoryRepository departmentCategoryRepository;
    private final EventPublisher               eventPublisher;

    /**
     * Erzeugt ein {@link DepartmentCategory} im Namen des Users.
     */
    public Mono<IdDto> createDepartmentCategory(String departmentCategoryName, User creator)
    {
        log.info("CreateDepartmentCategory started");

        if (!creator.isDepartmentAdmin())
        {
            return Mono.error(new NotAuthorizedException());
        }

        return departmentCategoryRepository.existsByName(departmentCategoryName)
                .doOnNext(b -> ReactorBooleanHelper.throwIfTrue(b, ErrorKey.NAME_TAKEN))
                .doOnNext(b -> log.debug("Creating DepartmentCategory '{}'", departmentCategoryName))
                .map(b -> buildCreatedEvent(departmentCategoryName, creator))
                .doOnNext(eventPublisher::send)
                .map(DomainEvent::getIdDto);
    }

    private DepartmentCategoryCreatedEvent buildCreatedEvent(String departmentCategoryName, User creator)
    {
        DepartmentCategory departmentCategory = DepartmentCategory.create(departmentCategoryName);
        return new DepartmentCategoryCreatedEvent(departmentCategory, creator);
    }
}
