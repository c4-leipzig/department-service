package org.c4.departmentservice.usecases.department;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.c4.microservice.framework.domain.user.User;
import reactor.core.publisher.Flux;

/**
 * UseCase über den alle {@link Department}s abgerufen werden können.
 * <br/>
 * Copyright: Copyright (c) 04.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class GetAllDepartmentsUseCase
{
    private final DepartmentRepository departmentRepository;

    /**
     * Liefert einen Stream aller Departments zurück.
     */
    public Flux<Department> getAllDepartments(User requester)
    {
        log.info("GetAllDepartments started");
        if (!requester.isDepartmentAdmin() && !requester.isAnyMember())
        {
            return Flux.error(new NotAuthorizedException());
        }

        return departmentRepository.getAll();
    }
}
