package org.c4.departmentservice.usecases.department;

import lombok.AllArgsConstructor;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentTermDto;
import org.c4.departmentservice.domain.general.sort.SortOrder;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.microservice.framework.domain.user.User;
import reactor.core.publisher.Flux;

/**
 * UseCase über den eine Liste aller {@link AssignmentTerm}s für ein Department abgerufen werden können.
 * <br/>
 * Copyright: Copyright (c) 13.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@UseCase
@AllArgsConstructor
public class GetAssignmentTermsForDepartmentUseCase
{
    private DepartmentAssignmentRepository departmentAssignmentRepository;

    public Flux<DepartmentAssignmentTermDto> getTermsForDepartment(String departmentId, SortOrder sort, User requester)
    {
        return requester.validateIsDepartmentAdmin().onErrorResume(ex -> requester.validateIsAnyMember())
                .thenMany(departmentAssignmentRepository.getAllAssignmentTermsForDepartmentSortedByDate(departmentId, sort));
    }
}
