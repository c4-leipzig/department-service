package org.c4.departmentservice.usecases.department.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.usecases.Command;
import org.c4.departmentservice.usecases.department.CreateDepartmentUseCase;
import org.c4.microservice.framework.domain.user.UserType;

import java.util.List;

/**
 * {@link Command} für das Bearbeiten des {@link CreateDepartmentUseCase}.
 * <br/>
 * Copyright: Copyright (c) 03.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@AllArgsConstructor
public class CreateDepartmentCommand
{
    private final String       name;
    private final String       description;
    private final String       departmentCategoryId;
    private final List<String> possibleUserTypes;

    public Department toDepartment()
    {
        return Department.create(name, description, departmentCategoryId, UserType.ofSave(possibleUserTypes));
    }

}
