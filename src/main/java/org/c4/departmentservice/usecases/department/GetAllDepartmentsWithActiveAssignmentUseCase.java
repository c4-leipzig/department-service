package org.c4.departmentservice.usecases.department;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.DepartmentRepository;
import org.c4.departmentservice.domain.department.DepartmentWithActiveAssignmentDto;
import org.c4.departmentservice.domain.departmentassignment.AssignmentTerm;
import org.c4.departmentservice.domain.departmentassignment.DepartmentAssignmentRepository;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.microservice.framework.domain.user.User;
import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * UseCase, über den eine Liste aller {@link Department}s mit ihrem jeweils aktuellen {@link AssignmentTerm}
 * angefordert werden kann.
 * <br/>
 * Copyright: Copyright (c) 20.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class GetAllDepartmentsWithActiveAssignmentUseCase
{
    private final DepartmentRepository           departmentRepository;
    private final DepartmentAssignmentRepository departmentAssignmentRepository;

    @GetMapping("/departments/withActiveAssignment")
    public Flux<DepartmentWithActiveAssignmentDto> getAllDepartmentsWithActiveAssignment(User requester)
    {
        log.info("GetAllDepartmentsWithActiveAssignment started.");
        return requester.validateIsDepartmentAdmin()
                .onErrorResume(ex -> requester.validateIsAnyMember())
                .thenMany(departmentRepository.getAll())
                .flatMap(this::getWithActiveAssignmentDto);
    }

    private Mono<DepartmentWithActiveAssignmentDto> getWithActiveAssignmentDto(Department department)
    {
        log.info(department.getId());

        return departmentAssignmentRepository.getActiveAssignmentTermForDepartment(department.getId())
                .map(activeAssignment -> new DepartmentWithActiveAssignmentDto(department, activeAssignment))
                .switchIfEmpty(
                        Mono.defer(() -> Mono.just(new DepartmentWithActiveAssignmentDto(department, null))));
    }
}
