package org.c4.departmentservice.usecases.department;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.validation.DepartmentValidator;
import org.c4.departmentservice.usecases.department.commands.UpdateDepartmentCommand;
import org.c4.departmentservice.domain.department.events.DepartmentUpdatedEvent;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import reactor.core.publisher.Mono;

/**
 * <br/>
 * Copyright: Copyright (c) 08.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class UpdateDepartmentUseCase
{
    private final EventPublisher      eventPublisher;
    private final DepartmentValidator departmentValidator;

    public Mono<Void> updateDepartment(String id, UpdateDepartmentCommand command, User updater)
    {
        return departmentValidator.validate(id, command, updater)
                .doOnNext(department -> sendDepartmentUpdatedEvent(department, command, updater))
                .then();
    }

    private void sendDepartmentUpdatedEvent(Department department, UpdateDepartmentCommand command,
            User updater)
    {
        log.debug("Sending DepartmentUpdatedEvent");
        DepartmentUpdatedEvent updatedEvent = new DepartmentUpdatedEvent(department.getId(),
                department.getVersion(), updater, department.generateChange(command));
        eventPublisher.send(updatedEvent);
    }
}
