package org.c4.departmentservice.usecases.department;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.Department;
import org.c4.departmentservice.domain.department.validation.DepartmentValidator;
import org.c4.departmentservice.usecases.department.commands.CreateDepartmentCommand;
import org.c4.departmentservice.domain.department.events.DepartmentCreatedEvent;
import org.c4.departmentservice.infrastructure.configuration.UseCase;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import reactor.core.publisher.Mono;

/**
 * UseCase, über den das Anlegen eines {@link Department}s abgebildet wird.
 * <br/>
 * Copyright: Copyright (c) 31.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@UseCase
@AllArgsConstructor
public class CreateDepartmentUseCase
{
    private final EventPublisher      eventPublisher;
    private final DepartmentValidator departmentValidator;

    /**
     * Validiert den {@link CreateDepartmentCommand} und legt im Erfolgsfall ein {@link Department} im Namen des {@link User}s an.
     */
    public Mono<IdDto> createDepartment(CreateDepartmentCommand createDepartmentCommand, User creator)
    {
        log.info("CreateDepartmentUseCase started");
        return departmentValidator.validate(createDepartmentCommand, creator)
                .then(Mono.defer(() -> publishCreatedEvent(createDepartmentCommand, creator)));
    }

    private Mono<IdDto> publishCreatedEvent(CreateDepartmentCommand createDepartmentCommand, User creator)
    {
        log.debug("Publishing DepartmentCreatedEvent");
        Department department = createDepartmentCommand.toDepartment();
        eventPublisher.send(new DepartmentCreatedEvent(department, creator));

        return Mono.just(department.getIdDto());
    }
}
