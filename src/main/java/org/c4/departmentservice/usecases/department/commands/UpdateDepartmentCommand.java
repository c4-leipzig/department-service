package org.c4.departmentservice.usecases.department.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * <br/>
 * Copyright: Copyright (c) 08.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@AllArgsConstructor
public class UpdateDepartmentCommand
{
    private final String       name;
    private final String       description;
    private final String       departmentCategoryId;
    private final List<String> possibleUserTypes;
}
