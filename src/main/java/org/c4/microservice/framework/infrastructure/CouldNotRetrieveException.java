package org.c4.microservice.framework.infrastructure;

/**
 * Allgemeine Exception, die geworfen wird, wenn ein Web-Request kein Ergebnis liefern kann.
 * <br/>
 * Copyright: Copyright (c) 06.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class CouldNotRetrieveException extends RuntimeException
{
}
