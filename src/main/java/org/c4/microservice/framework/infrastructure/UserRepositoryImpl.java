package org.c4.microservice.framework.infrastructure;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.configuration.UserServiceProperties;
import org.c4.microservice.framework.domain.user.UserRepository;
import org.c4.microservice.framework.domain.user.UserType;
import org.c4.microservice.framework.domain.user.UserTypeResult;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Implementierung des {@link UserRepository}s, die über die Rest-Schnittstelle des User-Services
 * Informationen eines Users abfragt.
 * <br/>
 * Copyright: Copyright (c) 06.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository
{
    private final UserServiceProperties userServiceProperties;

    private final WebClient webClient;

    @Override
    public Mono<UserType> getTypeForUser(String userId)
    {
        return webClient.get()
                .uri(userServiceProperties.getUserTypeForUser(), userId)
                .retrieve()
                .bodyToMono(UserTypeResult.class)
                .onErrorMap(e -> {
                    log.info(e.getMessage());
                    return new CouldNotRetrieveException();
                })
                .map(UserTypeResult::getResult)
                .map(UserType::ofSave);
    }

    @Override
    public Mono<Boolean> existsById(String userId)
    {
        return getTypeForUser(userId).map(u -> true);
    }
}
