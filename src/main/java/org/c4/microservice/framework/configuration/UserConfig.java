package org.c4.microservice.framework.configuration;

import org.c4.microservice.framework.domain.user.UserRepository;
import org.c4.microservice.framework.infrastructure.UserRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * <br/>
 * Copyright: Copyright (c) 06.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class UserConfig
{
    @Bean
    public UserRepository userRepository(UserServiceProperties userServiceProperties,
            WebClient webClient)
    {
        return new UserRepositoryImpl(userServiceProperties, webClient);
    }
}
