package org.c4.microservice.framework.configuration;

import org.c4.microservice.framework.mongodb.DateToLocalDateConverter;
import org.c4.microservice.framework.mongodb.LocalDateToDateConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.convert.*;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Konfiguriert die MongoDB.
 * <br/>
 * Copyright: Copyright (c) 13.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class MongoConfig
{
    /**
     * Überschreiben des Standardmappers, um unnötiges _class-Feld zu umgehen.
     */
    @Bean
    public MappingMongoConverter mappingMongoConverter(MongoMappingContext context,
            MongoCustomConversions conversions) {
        MappingMongoConverter mappingConverter = new MappingMongoConverter(NoOpDbRefResolver.INSTANCE, context);
        mappingConverter.setTypeMapper(new DefaultMongoTypeMapper(null));

        mappingConverter.setCustomConversions(conversions);
        return mappingConverter;
    }

    /**
     * Custom-Converter zur Persistierung von {@link LocalDate}s.
     */
    @Bean
    public MongoCustomConversions mongoCustomConversions()
    {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(new DateToLocalDateConverter());
        converters.add(new LocalDateToDateConverter());
        return new MongoCustomConversions(converters);
    }
}
