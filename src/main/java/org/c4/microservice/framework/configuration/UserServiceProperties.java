package org.c4.microservice.framework.configuration;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * User-Service Properties. Beinhaltet die konkreten Endpunkte, über die mit dem User-Service
 * kommuniziert wird.
 * <br/>
 * Copyright: Copyright (c) 06.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
@ConfigurationProperties(prefix = "c4.userservice")
@Setter
public class UserServiceProperties
{
    private String baseUrl;
    private String userTypeForUser;

    public String getUserTypeForUser()
    {
        return baseUrl + userTypeForUser;
    }
}
