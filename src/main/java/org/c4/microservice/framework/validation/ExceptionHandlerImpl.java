package org.c4.microservice.framework.validation;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.ErrorKey;
import org.c4.departmentservice.domain.exceptions.InvalidOperationException;
import org.c4.departmentservice.domain.exceptions.InvalidParameterException;
import org.c4.departmentservice.domain.exceptions.NotFoundException;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.microservice.framework.domain.user.NotAuthorizedException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ErrorHandling für Validation-Errors.
 * Gescheiterte Validierungen werden als Liste von ErrorKeys ausgegeben.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@ControllerAdvice
public class ExceptionHandlerImpl extends ResponseEntityExceptionHandler
{
    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    protected ResponseEntity<Object> handleInvalidParameterException(MethodArgumentTypeMismatchException ex)
    {
        try
        {
            Throwable cause = ex.getCause().getCause();
            if (cause instanceof InvalidParameterException)
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(VoidProcessingResult.of(((InvalidParameterException) cause).getErrors()));
            }
        }
        catch (Exception ignore)
        {
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(VoidProcessingResult.ofError(ErrorKey.NOT_CATEGORIZED_METHOD_ARGUMENT_TYPE_MISMATCH));
    }

    @ExceptionHandler({ NotFoundException.class })
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex)
    {
        log.info("NotFoundException occurred");
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler({ InvalidOperationException.class })
    protected ResponseEntity<Object> handleInvalidOperationException(InvalidOperationException ex)
    {
        log.info("InvalidOperation occurred with errors '{}'", ex::getErrors);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(VoidProcessingResult.ofErrors(ex.getErrors()));
    }

    @ExceptionHandler({ NotAuthorizedException.class })
    protected ResponseEntity<Object> handleNotAuthorizedException(NotAuthorizedException ex)
    {
        log.info("Operation could not be executed due to insufficient authorization.");
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors())
        {
            errors.add(error.getDefaultMessage());
        }

        return ResponseEntity.status(status).headers(headers).body(VoidProcessingResult.ofErrors(errors));
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request)
    {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getFieldErrors())
        {
            errors.add(error.getDefaultMessage());
        }
        return ResponseEntity.status(status).headers(headers).body(VoidProcessingResult.ofErrors(errors));
    }

    /**
     * Hier werden {@link InvalidFormatException}s gesondert behandelt.
     * Diese treten in Verbindung mit {@link DateTimeFormat} auf, wenn das eingegebene Datum ungültig ist.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        if (ex.getCause() instanceof InvalidFormatException)
        {
            return ResponseEntity.status(status)
                    .headers(headers)
                    .body(VoidProcessingResult.ofErrors(Collections.singletonList(ErrorKey.DATE_INVALID)));
        }

        return super.handleHttpMessageNotReadable(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
            HttpStatus status, WebRequest request)
    {
        return super.handleExceptionInternal(ex, body, headers, status, request);
    }
}
