package org.c4.microservice.framework.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * Validator stellt sicher, dass ein String eine valide ObjectId repräsentiert.
 * <br/>
 * Copyright: Copyright (c) 19.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class IdValidator implements ConstraintValidator<IdValid, String>
{
    private static final Pattern OBJECT_ID_PATTERN = Pattern.compile("^[0-9a-fA-F]{24}$");

    private boolean nullable;

    @Override
    public void initialize(IdValid constraintAnnotation)
    {
        this.nullable = constraintAnnotation.nullable();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context)
    {
        if (nullable)
        {
            return value == null || OBJECT_ID_PATTERN.matcher(value).matches();
        }
        return value != null && OBJECT_ID_PATTERN.matcher(value).matches();
    }
}
