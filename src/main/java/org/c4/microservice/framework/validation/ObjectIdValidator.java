package org.c4.microservice.framework.validation;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Pattern zur Validierung von ObjectIds.
 * <br/>
 * Copyright: Copyright (c) 17.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjectIdValidator
{
    public static final String OBJECT_ID_PATTERN = "^[0-9a-zA-Z]{24}$";
}
