package org.c4.microservice.framework.domain.user;

import java.util.Arrays;
import java.util.Optional;

/**
 * Liste der Rollen, die ein User einnehmen kann.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum Role
{
    CANDIDATE,
    MEMBER,
    HONORARY_MEMBER,
    DEPARTMENT_ADMIN;

    private final String key;

    Role()
    {
        this.key = "ROLE_" + name();
    }

    public static Optional<Role> getByName(String name)
    {
        return Arrays.stream(values())
                .filter(role -> role.name().equals(name) || role.key.equals(name))
                .findFirst();
    }
}
