package org.c4.microservice.framework.domain.user;

import reactor.core.publisher.Mono;

/**
 * Repository, über das User vom User-Service abgefragt werden können.
 * <br/>
 * Copyright: Copyright (c) 06.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface UserRepository
{
    /**
     * Falls der User existiert, wird sein UserType zurückgegeben.
     * @return
     */
    Mono<UserType> getTypeForUser(String userId);

    /**
     * Checkt, ob ein User existiert.
     * @return
     */
    Mono<Boolean> existsById(String userId);
}
