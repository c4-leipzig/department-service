package org.c4.microservice.framework.domain.user;

import lombok.Getter;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Repräsentation eines Users.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class User
{
    @Getter
    private final String     id;
    private final List<Role> roles;

    public User(String id, List<Role> roles)
    {
        this.id = id;
        this.roles = roles;
    }

    public User(Authentication authentication)
    {
        this.id = authentication.getName();
        roles = authentication.getAuthorities()
                .stream()
                .map(authority -> Role.getByName(authority.getAuthority()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    /**
     * Überprüft, ob der User ein DepartmentAdmin ist.
     */
    public boolean isDepartmentAdmin()
    {
        return roles.contains(Role.DEPARTMENT_ADMIN);
    }

    /**
     * Überprüft, ob der User ein Mitglied (d.h. Candidate, Member oder Honorary_Member) ist.
     */
    public boolean isAnyMember()
    {
        return roles.contains(Role.MEMBER) || roles.contains(Role.HONORARY_MEMBER) || roles.contains(
                Role.CANDIDATE);
    }

    public Mono<Void> validateIsDepartmentAdmin()
    {
        if (isDepartmentAdmin())
        {
            return Mono.empty();
        }

        return Mono.error(new NotAuthorizedException());
    }

    public Mono<Void> validateIsAnyMember()
    {
        if (isAnyMember())
        {
            return Mono.empty();
        }

        return Mono.error(new NotAuthorizedException());
    }
}
