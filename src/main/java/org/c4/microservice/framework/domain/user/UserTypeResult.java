package org.c4.microservice.framework.domain.user;

import org.c4.microservice.framework.communication.ProcessingResult;

/**
 * Explizites {@link ProcessingResult} zum Empfangen von UserTypes vom User-Service.
 * <br/>
 * Copyright: Copyright (c) 06.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class UserTypeResult extends ProcessingResult<String>
{
}
