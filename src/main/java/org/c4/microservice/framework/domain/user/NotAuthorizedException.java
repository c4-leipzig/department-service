package org.c4.microservice.framework.domain.user;

/**
 * Exception, die immer dann geworfen wird, wenn ein UseCase von einem User aufgerufen wird,
 * der dazu nicht berechtigt ist.
 * <br/>
 * Copyright: Copyright (c) 29.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class NotAuthorizedException extends RuntimeException
{
}
