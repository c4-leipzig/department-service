package org.c4.microservice.framework.domain.user;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Klassifizierung eines Users in einen bestimmten Typus (Anwärter, Mitglied, ...).
 * <br/>
 * Copyright: Copyright (c) 09.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum UserType
{
    /**
     * Ein nicht weiter mit dem Club assoziierter Gast.
     */
    GUEST,
    /**
     * Ein Anwärter.
     */
    CANDIDATE,
    /**
     * Ein normales Mitglied des Vereins.
     */
    MEMBER,
    /**
     * Ein Ehrenmitglied des Vereins.
     */
    HONORARY_MEMBER,
    /**
     * Ein Mitglied der F11.
     */
    F11;

    private static final List<String> enumValues = Arrays.stream(values())
            .map(Enum::toString)
            .collect(Collectors.toList());

    public static UserType ofSave(String key)
    {
        return valueOf(key);
    }

    public static Optional<List<UserType>> of(List<String> keys)
    {
        if (!enumValues.containsAll(keys))
        {
            return Optional.empty();
        }
        return Optional.of(keys.stream().map(UserType::ofSave).collect(Collectors.toList()));
    }

    public static List<UserType> ofSave(List<String> keys)
    {
        return keys.stream().map(UserType::ofSave).collect(Collectors.toList());
    }

    public static boolean exists(String maybeType)
    {
        return enumValues.contains(maybeType);
    }

    public static boolean allExist(List<String> maybeTypes)
    {
        return maybeTypes == null || enumValues.containsAll(maybeTypes);
    }
}
