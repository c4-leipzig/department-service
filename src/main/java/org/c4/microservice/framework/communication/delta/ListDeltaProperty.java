package org.c4.microservice.framework.communication.delta;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * {@link DeltaProperty} für eine {@link List} von Objekten.
 * <br/>
 * Copyright: Copyright (c) 09.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ListDeltaProperty<T>
{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<T> changeList;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean doNotChange;

    public static <U> ListDeltaProperty<U> of(List<U> value)
    {
        if (value == null)
        {
            ListDeltaProperty<U> listDeltaProperty = new ListDeltaProperty<>();
            listDeltaProperty.doNotChange = true;
            return listDeltaProperty;
        }

        ListDeltaProperty<U> result = new ListDeltaProperty<>();
        result.changeList = value;

        return result;
    }

    /**
     * Vergleicht zwei duplikatfreie Listen und erzeugt daraus ein entsprechendes {@link ListDeltaProperty}.
     * Die Ordnung innerhalb der Listen spielt dabei keine Rolle.
     * T muss eine geeignete {@link #equals}-Methode zur Verfügung stellen.
     * Parameter dürfen nicht {@link null} sein.
     */
    public static <T> ListDeltaProperty<T> ofNoDuplicatesNoOrder(List<T> oldValues, List<T> newValues)
    {
        if (oldValues.size() == newValues.size() && oldValues.containsAll(newValues))
        {
            ListDeltaProperty<T> deltaProperty = new ListDeltaProperty<>();
            deltaProperty.doNotChange = true;
            return deltaProperty;
        }

        return of(newValues);
    }

    public List<T> apply(List<T> oldValue)
    {
        if (doNotChange)
        {
            return oldValue;
        }

        return changeList;
    }
}
