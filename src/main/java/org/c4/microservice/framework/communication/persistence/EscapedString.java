package org.c4.microservice.framework.communication.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;

import java.util.Base64;

/**
 * Ein Wrapper für einen String, der bei der Persistierung in die Datenbank automatisch base64-codiert wird.
 * <br/>
 * Copyright: Copyright (c) 17.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@NoArgsConstructor
public class EscapedString
{
    @JsonIgnore
    @Transient
    private String value;
    private String base64Value;

    @PersistenceConstructor
    public EscapedString(String base64Value)
    {
        setBase64Value(base64Value);
    }

    public static EscapedString of(String value)
    {
        if (value == null)
        {
            return null;
        }

        EscapedString escapedString = new EscapedString();
        escapedString.setValue(value);

        return escapedString;
    }

    public void setValue(String value)
    {
        this.value = value;
        base64Value = Base64.getEncoder().encodeToString(value.getBytes());
    }

    public void setBase64Value(String base64Value)
    {
        this.base64Value = base64Value;
        value = new String(Base64.getDecoder().decode(base64Value));
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof EscapedString)
        {
            return value.equals(((EscapedString) obj).value);
        }

        return false;
    }
}
