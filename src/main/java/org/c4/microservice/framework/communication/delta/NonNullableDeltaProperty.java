package org.c4.microservice.framework.communication.delta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Property eines {@link Delta}s. Dieses kann nicht auf null gesetzt werden
 * und ist somit simpler als das standardmäßige {@link DeltaProperty}.
 * <br/>
 * Copyright: Copyright (c) 30.07.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NonNullableDeltaProperty<T>
{
    private T changeValue;

    public T apply()
    {
        return changeValue;
    }
}
