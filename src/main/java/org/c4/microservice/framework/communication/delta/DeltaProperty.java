package org.c4.microservice.framework.communication.delta;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * Property eines Deltas. Es wird der neue Wert angegeben, oder das Attribut auf null gesetzt, falls
 * das Flag gesetzt ist.
 * <br/>
 * Copyright: Copyright (c) 21.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DeltaProperty<T>
{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T       changeValue;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean doNotChange;

    public static <T> DeltaProperty<T> of(T value)
    {
        DeltaProperty<T> result = new DeltaProperty<>();
        result.changeValue = value;

        return result;
    }

    public static <T> DeltaProperty<T> of(T oldValue, T newValue)
    {
        if (Objects.equals(oldValue, newValue))
        {
            DeltaProperty<T> deltaProperty = new DeltaProperty<>();
            deltaProperty.doNotChange = true;
            return deltaProperty;
        }

        return of(newValue);
    }

    public T apply(T oldValue)
    {
        if (doNotChange)
        {
            return oldValue;
        }

        return changeValue;
    }
}
