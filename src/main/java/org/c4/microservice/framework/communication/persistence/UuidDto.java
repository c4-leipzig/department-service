package org.c4.microservice.framework.communication.persistence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.departmentservice.ErrorKey;
import org.c4.microservice.framework.validation.UUIDValidator;

import javax.validation.constraints.Pattern;

/**
 * Repräsentation einer UUID.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UuidDto
{
    @Pattern(regexp = UUIDValidator.UUID_PATTERN, message = ErrorKey.ID_INVALID)
    private String id;

    public static UuidDto of(String userId)
    {
        return new UuidDto(userId);
    }
}
