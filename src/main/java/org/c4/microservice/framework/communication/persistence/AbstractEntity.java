package org.c4.microservice.framework.communication.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import org.springframework.data.annotation.Id;

/**
 * Wrapper für alle Objekte, die in der Datenbank persistiert werden sollen.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
public abstract class AbstractEntity
{
    @Id
    private final String id;
    private Long version;

    public AbstractEntity(String id)
    {
        this.id = id;
    }

    @JsonIgnore
    /**
     * Liefert die Id als Transfer-Objekt zurück.
     */
    public IdDto getIdDto()
    {
        return IdDto.of(id);
    }

    /**
     * Setzt die Version.
     */
    public void updateVersion(Long newVersion)
    {
        version = newVersion;
    }
}
