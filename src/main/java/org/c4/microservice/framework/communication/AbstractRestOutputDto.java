package org.c4.microservice.framework.communication;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

/**
 * Oberklasse für alle RestOutputDtos.
 * <br/>
 * Copyright: Copyright (c) 09.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractRestOutputDto
{
    String id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String  createdBy;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Instant createdAt;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String  lastModifiedBy;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Instant lastModifiedAt;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Long    version;
}
