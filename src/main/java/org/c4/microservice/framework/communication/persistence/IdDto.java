package org.c4.microservice.framework.communication.persistence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.c4.departmentservice.ErrorKey;
import org.c4.microservice.framework.validation.ObjectIdValidator;

import javax.validation.constraints.Pattern;

/**
 * Repräsentation einer ObjectId.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class IdDto
{
    @Pattern(regexp = ObjectIdValidator.OBJECT_ID_PATTERN, message = ErrorKey.ID_INVALID)
    private String id;

    public static IdDto of(String id)
    {
        return new IdDto(id);
    }

    public static IdDto random()
    {
        return new IdDto(new ObjectId().toHexString());
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof IdDto)
        {
            return id.equals(((IdDto) other).getId());
        }

        return false;
    }
}
