package org.c4.microservice.framework.mongodb;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * Converter zur Umwandlung von {@link Date}s in {@link LocalDate}s.
 * <br/>
 * Copyright: Copyright (c) 08.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class DateToLocalDateConverter implements Converter<Date, LocalDate>
{
    @Override
    public LocalDate convert(Date source)
    {
        return LocalDate.ofInstant(source.toInstant(), ZoneId.ofOffset("UTC", ZoneOffset.UTC));
    }
}
