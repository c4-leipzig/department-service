package org.c4.microservice.framework.mongodb;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * Converter zur Umwandlung von {@link LocalDate}s in {@link Date}s.
 * Dieser werden immer in UTC gespeichert. Bei der Rückkonvertierung wird die Timezone wieder abgeschnitten.
 * <br/>
 * Copyright: Copyright (c) 08.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class LocalDateToDateConverter implements Converter<LocalDate, Date>
{
    @Override
    public Date convert(LocalDate localDate)
    {
        return Date.from(localDate.atStartOfDay().toInstant(ZoneOffset.UTC));
    }
}
