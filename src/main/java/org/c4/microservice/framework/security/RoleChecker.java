package org.c4.microservice.framework.security;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;

/**
 * HelperClass, die überprüft, ob ein Nutzer über entsprechende Rechte verfügt.
 * <br/>
 * Copyright: Copyright (c) 11.03.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public class RoleChecker
{
    /**
     * Überprüft, ob die übergebene {@link Authentication} über die angegebene Rolle verfügt.
     */
    public static boolean authenticationHasRole(Authentication authentication, String role)
    {
        String extendedRole = "ROLE_" + role;
        try
        {
            return authentication.getAuthorities()
                    .stream()
                    .anyMatch(authority -> authority.getAuthority().equals(extendedRole));
        }
        catch (Exception e)
        {
            log.warn("Error checking roles for user '{}'", authentication::getName);
            return false;
        }
    }
}
