package org.c4.microservice.framework.domainevent.repository;

import org.c4.microservice.framework.communication.StringListDto;
import org.c4.microservice.framework.configuration.EventstoreProperties;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * WebClient zur Abfrage von DomainEvents im Zuge der initialen Aufdatung.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class DomainEventRepositoryImpl implements DomainEventRepository
{
    private final WebClient webClient;

    private final EventstoreProperties eventstoreProperties;

    public DomainEventRepositoryImpl(WebClient webClient, EventstoreProperties eventstoreProperties)
    {
        this.webClient = webClient;
        this.eventstoreProperties = eventstoreProperties;
    }

    @Override
    public List<List<DomainEvent>> getRelevantEvents(List<String> eventTypes)
    {
        List<String> aggregateIds = webClient.get()
                .uri(uriBuilder -> uriBuilder.path(
                        eventstoreProperties.getAggregateIdsByEventTypes())
                        .queryParam("eventTypes", String.join(",", eventTypes))
                        .build())
                .retrieve()
                .onStatus(httpStatus -> httpStatus.value() == HttpStatus.NOT_FOUND.value(),
                        clientResponse -> Mono.empty())
                .bodyToMono(StringListDto.class)
                .map(StringListDto::getStringList)
                .block();

        if (aggregateIds == null || aggregateIds.isEmpty())
        {
            return Collections.emptyList();
        }

        List<List<DomainEvent>> result = new ArrayList<>();

        aggregateIds.forEach(aggregateId -> result.add(getEventStream(aggregateId)));

        return result;
    }

    @Override
    public List<DomainEvent> getEventStream(String aggregateId)
    {
        return getEventStream(aggregateId, 0L);
    }

    @Override
    public List<DomainEvent> getEventStream(String aggregateId, long minVersion)
    {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder.path(
                        eventstoreProperties.getEventStreamForAggregate())
                        .queryParam("minVersion", minVersion)
                        .build(aggregateId))
                .retrieve()
                .bodyToFlux(DomainEvent.class)
                .collectList()
                .block();
    }
}
