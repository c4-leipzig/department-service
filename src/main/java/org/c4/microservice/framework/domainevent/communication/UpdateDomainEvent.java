package org.c4.microservice.framework.domainevent.communication;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import reactor.core.publisher.Mono;

import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.*;

/**
 * Oberklasse für UPDATE-Events. Updates werden immer auf ein Aggregate (= eine Subklasse von {@link AbstractEntity})
 * angewendet. Die entsprechende Subklasse wird über das Generic identifiziert.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor
public abstract class UpdateDomainEvent<T extends AbstractEntity> extends DomainEvent
{
    public UpdateDomainEvent(String aggregateId, Long version, User updater)
    {
        super();
        setAggregateId(aggregateId);
        setVersion(version);
        setCreatedBy(updater.getId());
    }

    @Override
    public Mono<DomainEventProcessingState> handleEvent(DomainEventHandlingContext handlingContext)
    {
        return getAggregate(handlingContext).flatMap(
                persistedAggregate -> handleUpdate(handlingContext, persistedAggregate))
                .switchIfEmpty(Mono.error(new Exception()))
                .onErrorResume((e -> handleAggregateNotFound(handlingContext)));
    }

    private Mono<DomainEventProcessingState> handleUpdate(DomainEventHandlingContext handlingContext,
            T persistedAggregate)
    {
        Long currentVersion = persistedAggregate.getVersion();

        if (getVersion() <= currentVersion)
        {
            log.info("Update-Event {} is obsolete. Event version: {}, Current version: {}",
                    getClass().getSimpleName(), getVersion(), currentVersion);
            return Mono.just(OBSOLETE);
        }

        if (getVersion() == currentVersion + 1)
        {
            return apply(handlingContext, persistedAggregate).then(Mono.just(PROCESSED));
        }

        return handleVersionMismatch(handlingContext, currentVersion).then(Mono.just(VERSION_MISMATCH));
    }

    private Mono<DomainEventProcessingState> handleAggregateNotFound(
            DomainEventHandlingContext handlingContext)
    {
        return handleVersionMismatch(handlingContext, null).then(Mono.just(VERSION_MISMATCH));
    }

    protected abstract Mono<Void> apply(DomainEventHandlingContext handlingContext, T aggregate);

    protected abstract Mono<T> getAggregate(DomainEventHandlingContext handlingContext);

    private Mono<Void> handleVersionMismatch(DomainEventHandlingContext handlingContext, Long currentVersion)
    {
        log.warn("Version mismatch for aggregate '{}'. Event version: {}, Current version: {}",
                getAggregateId(), getVersion(), currentVersion);
        handlingContext.reloadAggregate(getAggregateId(), currentVersion == null ? 0L : currentVersion + 1);
        return Mono.empty();
    }

}
