package org.c4.microservice.framework.domainevent.communication;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domain.user.User;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import reactor.core.publisher.Mono;

import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.OBSOLETE;
import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.PROCESSED;

/**
 * Oberklasse für CREATE-Events.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor
public abstract class CreateDomainEvent extends DomainEvent
{
    public CreateDomainEvent(String aggregateId, User createdBy)
    {
        super();
        setAggregateId(aggregateId);
        setCreatedBy(createdBy.getId());
    }

    @Override
    public Mono<DomainEventProcessingState> handleEvent(DomainEventHandlingContext handlingContext)
    {
        if (handlingContext.isInitializing())
        {
            return aggregatePresent(handlingContext).flatMap(b -> b ? obsolete() : process(handlingContext));
        }

        return process(handlingContext);
    }

    private Mono<DomainEventProcessingState> obsolete()
    {
        log.info("CreateEvent {} is obsolete.", getClass().getSimpleName());
        return Mono.just(OBSOLETE);
    }

    private Mono<DomainEventProcessingState> process(DomainEventHandlingContext handlingContext)
    {
        return apply(handlingContext).then(Mono.just(PROCESSED));
    }

    protected abstract Mono<Void> apply(DomainEventHandlingContext handlingContext);

    protected abstract Mono<Boolean> aggregatePresent(DomainEventHandlingContext handlingContext);
}
