package org.c4.microservice.framework.domainevent.communication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.departmentservice.domain.department.events.DepartmentCreatedEvent;
import org.c4.departmentservice.domain.department.events.DepartmentUpdatedEvent;
import org.c4.departmentservice.domain.departmentassignment.events.AssignmentTermAddedEvent;
import org.c4.departmentservice.domain.departmentassignment.events.DepartmentAssignmentCreatedEvent;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryCreatedEvent;
import org.c4.departmentservice.domain.departmentcategory.events.DepartmentCategoryUpdatedEvent;
import org.c4.departmentservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.communication.persistence.IdDto;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import reactor.core.publisher.Mono;

import java.time.Instant;

/**
 * Abstrakte Oberklasse für alle Events.
 * <br/>
 * Copyright: Copyright (c) 22.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, defaultImpl = UnknownEvent.class, property = "classIdentifier")
@JsonSubTypes({
        @JsonSubTypes.Type(value = DepartmentCategoryCreatedEvent.class, name = "DepartmentCategoryCreatedEvent"),
        @JsonSubTypes.Type(value = DepartmentCategoryUpdatedEvent.class, name = "DepartmentCategoryUpdatedEvent"),
        @JsonSubTypes.Type(value = DepartmentCreatedEvent.class, name = "DepartmentCreatedEvent"),
        @JsonSubTypes.Type(value = DepartmentAssignmentCreatedEvent.class, name = "DepartmentAssignmentCreatedEvent"),
        @JsonSubTypes.Type(value = AssignmentTermAddedEvent.class, name = "AssignmentTermAddedEvent"),
@JsonSubTypes.Type (value = DepartmentUpdatedEvent.class, name = "DepartmentUpdatedEvent")})

@Getter
@Setter
@Log4j2
public abstract class DomainEvent
{
    // Wird immer vom erstellenden Service gesetzt
    private String aggregateId;
    private String createdBy;

    // Wird vom Eventstore beim tatsächlichen Einfügen gesetzt
    private Instant createdAt;

    // Wird vom Eventstore vorgegeben
    private Long version;

    /**
     * Methode zum Verarbeiten des Events. Diese prüft auf Gültigkeit des Events und ruft anschließend
     * die entsprechende apply-Methode auf.
     */
    public abstract Mono<DomainEventProcessingState> handleEvent(DomainEventHandlingContext handlingContext);

    @JsonIgnore
    public IdDto getIdDto()
    {
        return IdDto.of(aggregateId);
    }
}
